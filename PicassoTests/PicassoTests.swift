//
//  PicassoTests.swift
//  PicassoTests
//
//  Created by Mantas Jakimavicius on 01/09/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import XCTest
@testable import Picasso

private extension String {
    static let bvText = "1D252C"
    static let bvBrand = "41B6E6"
    static let bvError = "E73C3E"
    static let black = "000000"
    static let white = "FFFFFF"
    static let bv = "BV"
}

// Testing entire theme classes
class PicassoTests: XCTestCase {

    func test_defaultTheme() throws {
        let brand = DataManager.shared.brand(with: "BV")
        let theme = DataManager.shared.theme(name: "DefaultTheme", brand: brand!)
        XCTAssertEqual(theme?.accessorySymbol?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.background?.hexValue, "F4F5F5")
        XCTAssertEqual(theme?.border?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.cardBackground?.hexValue, .white)
        XCTAssertEqual(theme?.subtitle?.hexValue, .bvText)
        XCTAssertEqual(theme?.title?.hexValue, .bvText)
        XCTAssertEqual(theme?.underline?.hexValue, "DEDFE0")
    }
    
    func test_ScoreboardSelectedLink() throws {
        let brand = DataManager.shared.brand(with: "BV")
        let theme = DataManager.shared.theme(name: "ScoreboardSelectedLink", brand: brand!)
        // this debug line is actually needed because otherwise it doesn't work :D
        print("\(date2()):\(Self.self):\(#function):\(#line): theme \(String(describing: theme))")
        XCTAssertEqual(theme?.accessorySymbol?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.background?.hexValue, .black)
        XCTAssertEqual(theme?.background?.alpha?.value, 0)
        XCTAssertEqual(theme?.border?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.cardBackground?.hexValue, .white)
        XCTAssertEqual(theme?.subtitle?.hexValue, .white)
        XCTAssertEqual(theme?.title?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.underline?.hexValue, .bvBrand)
    }
    
    func test_AlertInfoModalTheme() {
        let brand = DataManager.shared.brand(with: "BV")
        let theme = DataManager.shared.theme(name: "AlertInfoModalTheme", brand: brand!)
        print("\(date2()):\(Self.self):\(#function):\(#line): theme \(String(describing: theme))")
        XCTAssertEqual(theme?.accessorySymbol?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.background?.hexValue, .white)
        XCTAssertEqual(theme?.border?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.cardBackground?.hexValue, .white)
        XCTAssertEqual(theme?.subtitle?.hexValue, .bvText)
        XCTAssertEqual(theme?.title?.hexValue, .bvText)
        XCTAssertEqual(theme?.underline?.hexValue, "DEDFE0")
    }
    
    func test_AlertBetslipModalTheme() {
        let brand = DataManager.shared.brand(with: "BV")
        let theme = DataManager.shared.theme(name: "AlertBetslipModalTheme", brand: brand!)
        print("\(date2()):\(Self.self):\(#function):\(#line): theme \(String(describing: theme))")
        XCTAssertEqual(theme?.accessorySymbol?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.background?.hexValue, .white)
        XCTAssertEqual(theme?.border?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.cardBackground?.hexValue, .white)
        XCTAssertEqual(theme?.subtitle?.hexValue, .bvText)
        XCTAssertEqual(theme?.title?.hexValue, .bvText)
        XCTAssertEqual(theme?.underline?.hexValue, "DEDFE0")
    }

    // does not exist outside HeartBingo!
    func test_ButtonUnselectedTheme() {
        let brand = DataManager.shared.brand(with: "HeartBingo")
        let theme = DataManager.shared.theme(name: "ButtonUnselectedTheme", brand: brand!)
        print("\(date2()):\(Self.self):\(#function):\(#line): theme \(String(describing: theme))")
        XCTAssertEqual(theme?.accessorySymbol?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.background?.hexValue, "D8EFF9")
        XCTAssertEqual(theme?.border?.hexValue, .bvBrand)
        XCTAssertEqual(theme?.cardBackground?.hexValue, .white)
        XCTAssertEqual(theme?.subtitle?.hexValue, .bvText)
        XCTAssertEqual(theme?.title?.hexValue, .bvText)
        XCTAssertEqual(theme?.underline?.hexValue, "DEDFE0")
    }

}
