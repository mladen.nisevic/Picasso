//
//  PicassoColourParser.swift
//  PicassoTests
//
//  Created by Mladen Nisevic on 24/02/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import XCTest
@testable import Picasso

private extension String {
    static let bvText = "1D252C"
    static let bvBrand = "41B6E6"
    static let black = "000000"
    static let white = "FFFFFF"
    static let bv = "BV"
}

final class PicassoColourParserTests: XCTestCase {
    
    var parser: ColourParser?
    var bvBrand: Brand!

    override func setUpWithError() throws {
        parser = ColourParser()
        bvBrand = DataManager.shared.brand(with: .bv)
    }

    func test_fromHex() {
        let input = "fromHex(0xFAFAFA"
        let parser = FromHexParser(inputLine: input, brand: bvBrand)
        let colourInfo = parser.parseColour()
        let expected = "FAFAFA"
        XCTAssertEqual(colourInfo?.hex, expected)
    }
    
    func test_fromHex_alpha() {
        let input = ".fromHex(0x1D252D, alpha: 0.5)"
        let parser = FromHexParser(inputLine: input, brand: bvBrand)
        let colourInfo = parser.parseColour()
        let expected = "1D252D"
        XCTAssertEqual(colourInfo?.hex, expected)
        XCTAssertEqual(colourInfo?.alpha, 0.5)
    }

    func test_fromHex_UIColor() {
        let input = "UIColor.fromHex(0xCED4DA)"
        let parser = FromHexParser(inputLine: input, brand: bvBrand)
        let colourInfo = parser.parseColour()
        let expected = "CED4DA"
        XCTAssertEqual(colourInfo?.hex, expected)
    }
}

// MARK: - Other Colour
extension PicassoColourParserTests {
    
    func test_brandColour_boost() {
        let input = "UIColor.boost.withAlphaComponent(0.2"
        let name = "OutcomeBoostTheme_background"
        
        let colour = DataManager.shared.fetchColour(for: name, brand: bvBrand)
        let parser = BrandColourParser(inputLine: input, brand: bvBrand, miscData: colour!)
        let colourInfo = parser.parseColour()
        
        let expected = "FBDE40"
        XCTAssertEqual(colourInfo?.hex, expected)
        XCTAssertEqual(colourInfo?.alpha, 0.2)
    }
    
    func test_themeColour_background() {
        let name = "BetslipEachWaySuspendedTheme_background"
        let input = "super.background().withAlphaComponent(0.5"
        
        let colour = DataManager.shared.fetchColour(for: name, brand: bvBrand)
        let parser = BrandColourParser(inputLine: input, brand: bvBrand, miscData: colour!)
        let colourInfo = parser.parseColour()
        
        let expected = "D8EFF9"
        XCTAssertEqual(colourInfo?.hex, expected)
        XCTAssertEqual(colourInfo?.alpha, 0.5)
    }
    
    func test_themeColour_title() {
        let name = "BetslipEachWaySuspendedTheme_title"
        let input = "super.title().withAlphaComponent(0.5"
        let colour = DataManager.shared.fetchColour(for: name, brand: bvBrand)
        let parser = BrandColourParser(inputLine: input, brand: bvBrand, miscData: colour!)
        let colourInfo = parser.parseColour()
        
        XCTAssertEqual(colourInfo?.hex, String.black)
        XCTAssertEqual(colourInfo?.alpha, 0.5)
    }
    
    func test_themeColour_subtitle_withAlpha() {
        let name = "ButtonKeyPadSuspendedTheme_subtitle"
        let input = "super.subtitle().withAlphaComponent(0.3"
        let brand = DataManager.shared.brand(with: "Parimatch")
        let colour = DataManager.shared.fetchColour(for: name, brand: brand!)
        let parser = BrandColourParser(inputLine: input, brand: brand!, miscData: colour!)
        let colourInfo = parser.parseColour()
        XCTAssertEqual(colourInfo?.hex, String.white)
        
        let expectedAlpha: Double = 0.3
        XCTAssertEqual(colourInfo?.alpha, expectedAlpha)
    }
    
    func test_themeColour_subtitle() {
        let name = "LimitsSubtitleTheme_title"
        let input = "super.subtitle"
        let brand = DataManager.shared.brand(with: "HeartBingo")
        let colour = DataManager.shared.fetchColour(for: name, brand: brand!)
        let parser = BrandColourParser(inputLine: input, brand: brand!, miscData: colour!)
        let colourInfo = parser.parseColour()

        XCTAssertEqual(colourInfo?.hex, String.black)
    }
    
    func test_themeColour_subtitle_2() {
        let name = "LimitsSubtitleTheme_title"
        let input = "super.subtitle()"
        let brand = DataManager.shared.brand(with: "HeartBingo")
        let colour = DataManager.shared.fetchColour(for: name, brand: brand!)
        let parser = BrandColourParser(inputLine: input, brand: brand!, miscData: colour!)
        let colourInfo = parser.parseColour()
        
        XCTAssertEqual(colourInfo?.hex, String.black)
    }
}

// MARK: - Existing colours
extension PicassoColourParserTests {

    func test_themeColour__text_() {
        let input = "( .text )"
        let brand = DataManager.shared.brand(with: .bv)
        let colourName = DataManager.shared.colourName(with: "text")
        let parser = ColourNameParser(inputLine: input, brand: brand!, miscData: colourName)
        let colourInfo = parser.parseColour()
        XCTAssertEqual(colourInfo?.hex, String.bvText)
    }
    
    func test_themeColour__warning() {
        let input = ".warning"
        let brand = DataManager.shared.brand(with: .bv)
        let colourName = DataManager.shared.colourName(with: "warning")
        let parser = ColourNameParser(inputLine: input, brand: brand!, miscData: colourName)
        let colourInfo = parser.parseColour()

        let expected = "FFAA4D"
        XCTAssertEqual(colourInfo?.hex, expected)
    }
    
    func test_themeColour__underline() {
        let input = ".underline"
        let brand = DataManager.shared.brand(with: .bv)
        let colourName = DataManager.shared.colourName(with: "underline")
        let parser = ColourNameParser(inputLine: input, brand: brand!, miscData: colourName)
        let colourInfo = parser.parseColour()

        let expected = "DEDFE0"
        XCTAssertEqual(colourInfo?.hex, expected)
    }
    
    func test_themeColour__brand() {
        let input = "( .brand )"
        let brand = DataManager.shared.brand(with: .bv)
        let colourName = DataManager.shared.colourName(with: "brand")
        let parser = ColourNameParser(inputLine: input, brand: brand!, miscData: colourName)
        let colourInfo = parser.parseColour()
        XCTAssertEqual(colourInfo?.hex, String.bvBrand)
    }

    // subtitle: .circularProgressSubtitle -> .primary.withAlphaComponent(0.3)) -> 009A44
    func test_CircularProgressTheme() {
        let brand = DataManager.shared.brand(with: .bv)
        let theme = DataManager.shared.theme(name: "CircularProgressTheme", brand: brand!)

        XCTAssertEqual(theme?.title?.hexValue, "009A44")
        
        XCTAssertEqual(theme?.subtitle?.hexValue, "009A44")
        XCTAssertEqual(theme?.subtitle?.alphaValue, 0.3)
        
        // black / 0.5
        XCTAssertEqual(theme?.underline?.hexValue, .black)
        XCTAssertEqual(theme?.underline?.alphaValue, 0.5)
        
        // clear
        XCTAssertEqual(theme?.background?.hexValue, .black)
        XCTAssertEqual(theme?.background?.alphaValue, 0.0)
    }
    
    func test_RateTheAppPopupTheme() {
        let brand = DataManager.shared.brand(with: .bv)
        let theme = DataManager.shared.theme(name: "RateTheAppPopupTheme", brand: brand!)
        XCTAssertEqual(theme?.title?.hexValue, String.bvBrand)
    }
    
    func test_ButtonTertiaryTheme_accessorySymbol() {
        let brand = DataManager.shared.brand(with: .bv)
        let theme = DataManager.shared.theme(name: "ButtonTertiaryTheme", brand: brand!)
        XCTAssertEqual(theme?.accessorySymbol?.hexValue, String.bvText)
    }
    
    func test_ScoreboardSelectedLink_underline() {
        let brand = DataManager.shared.brand(with: .bv)
        let theme = DataManager.shared.theme(name: "ScoreboardSelectedLink", brand: brand!)
        XCTAssertEqual(theme?.underline?.hexValue, String.bvBrand)
    }
}

// MARK: - Verify Colours Have been correctly extracted and added
extension PicassoColourParserTests {
    
    /// super.title().withAlphaComponent(0.5)
    func test_BetslipEachWaySuspendedTheme_title() {
        let name = "BetslipEachWaySuspendedTheme_title"
        let brand = DataManager.shared.brand(with: .bv)
        let colour = DataManager.shared.colourAlpha(with: name, brand: brand!)
        
        XCTAssertEqual(colour?.hexValue, String.black)
        let expectedAlpha = 0.5
        XCTAssertEqual(colour?.alphaValue, expectedAlpha)
    }
    
    /// super.background().withAlphaComponent(0.5)
    func test_BetslipEachWaySuspendedTheme_background() {
        let name = "BetslipEachWaySuspendedTheme_background"
        let brand = DataManager.shared.brand(with: .bv)
        let colour = DataManager.shared.colourAlpha(with: name, brand: brand!)
        let expectedHex = "D8EFF9"
        XCTAssertEqual(colour?.hexValue, expectedHex)
        let expectedAlpha = 0.5
        XCTAssertEqual(colour?.alphaValue, expectedAlpha)
    }
}
