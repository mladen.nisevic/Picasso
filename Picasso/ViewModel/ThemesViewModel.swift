//
//  ThemesViewModel.swift
//  Picasso
//
//  Created by Mantas Jakimavicius on 01/09/2021.
//

import Foundation
import SwiftUI

final class ThemesViewModel: ObservableObject {

    static let shared = ThemesViewModel()

    let appstoreThemes = ThemesModel().bildBetThemes
    let mixedThemes = ThemesModel().betVictorThemes + ThemesModel().parimatchThemes + ThemesModel().talkSportThemes
    let asianThemes = ThemesModel().bWinThemes + ThemesModel().williamHillThemes
    let casinoThemes = ThemesModel().heartBingoThemes
    var themes: [NSObject] {
        appstoreThemes + mixedThemes + asianThemes + casinoThemes
    }
}
