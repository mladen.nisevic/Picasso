//
//  PaletterHelper.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 15/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import Foundation

private extension String {
    static var palettesFileName = "Palettes.json"
}

/**
 act as a bridge between parsing and reading by saving data into a json file
 */
class PaletteHelper: ObservableObject {
    static let shared = PaletteHelper()

    @Published var palettes: [BrandName: BrandPalette] = [:]

    var encoder: JSONEncoder
    var decoder: JSONDecoder
    init(encoder: JSONEncoder = .init(), decoder: JSONDecoder = .init()) {
        self.encoder = encoder
        self.encoder.outputFormatting = .prettyPrinted
        self.decoder = decoder
        self.decoder.keyDecodingStrategy = .convertFromSnakeCase
        self.initPalettes()
    }
}

// MARK: - Write
extension PaletteHelper {

    func paletteJSON() -> String {
        var jsonPalettes = [JSONPalette]()
        for key in BrandName.allCases {
            if let palette = palettes[key], let jsonPalett = JSONPalette(palette) {
                jsonPalettes.append(jsonPalett)
            }
        }
        return createJSON(jsonPalettes)
    }
    
    func createJSON<T: Codable>(_ inputData: T) -> String {
        var ret: String = .empty
        do {
            let data = try encoder.encode(inputData)
            ret += "\(String(data: data, encoding: .utf8) ?? .empty)"
        } catch {
            print("\(Date()):\(Self.self):\(#function):\(#line): Encoding error:  \(error.localizedDescription)")
        }
        return ret
    }
}

// MARK: - Read
extension PaletteHelper {

    func initPalettes() {
        let jsonPalettes = readJSON()
//        let jsonPalettes = Bundle.main.decode([JSONPalette].self, from: "Palettes.json") // can't find the file for some reason
        for jsonPalette in jsonPalettes {
            if let brandPalette = BrandPalette(jsonPalette) {
                palettes[brandPalette.id] = brandPalette
            }
        }
    }

    func readJSON() -> [JSONPalette] {

        if let fileURL = Bundle.main.url(forResource: "Palettes", withExtension: "json") {
            let jsonString = try! String(contentsOf: fileURL)
            let data = jsonString.data(using: .utf8) ?? Data()
            do {
                let palettes = try decoder.decode([JSONPalette].self, from: data)
                return palettes

            } catch DecodingError.keyNotFound(let key, let context) {
                let msg = "could not find key \(key) in JSON: \(context.debugDescription)"
                print(msg)
            } catch DecodingError.valueNotFound(let type, let context) {
                let msg = "could not find type \(type) in JSON: \(context.debugDescription)"
                print(msg)
            } catch DecodingError.typeMismatch(let type, let context) {
                let msg = "type mismatch for type \(type) in JSON: \(context.debugDescription)"
                print(msg)
            } catch DecodingError.dataCorrupted(let context) {
                let msg = "data found to be corrupted in JSON: \(context.debugDescription)"
                print(msg)
            } catch let error as NSError {
                let msg = "Error in read(from:ofType:) domain= \(error.domain), description= \(error.localizedDescription)"
                print(msg)
            } catch {
                print("error: \(error.localizedDescription)")
            }
        }
        return []
    }
}

struct JSONPaletteElement: Codable {
    let key: String
    let hexColour: String
}
extension JSONPaletteElement {
    init?(_ element: PaletteElement) {
        key = element.key.rawValue
        hexColour = element.colourHex
    }
}

struct JSONPalette: Codable {
    let brand: String
    let palette: [JSONPaletteElement]
}
extension JSONPalette {
    init?(_ brandPalette: BrandPalette) {
        brand = brandPalette.id.rawValue
        self.palette = brandPalette.palette.compactMap { JSONPaletteElement($0) }
    }
}

struct BrandColour: Codable, Hashable {
    let name: String
    let colour: String
}

struct BrandColours: Codable {
    let brand: String
    let brandColours: [BrandColour]
}
