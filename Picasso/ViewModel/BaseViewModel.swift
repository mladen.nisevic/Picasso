//
//  BaseViewModel.swift
//  Picasso
//
//  Created by Mladen Nisevic on 20/03/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import UIKit
import CoreData

private extension String {
    static let colourName = "Colour Name"
    
    static let coloursHexContains = "colours.hexValue CONTAINS[cd] %@"
    static let coloursSourceContains = "colours.source CONTAINS[cd] %@"
    static let descriptiveNameContains = "colours.descriptiveName CONTAINS[cd] %@"
    
    static let searchPrompt = "Search Themes colours for hex-colour (FFFFFF) or name (background), Brand (BV), or full text"
    
    static let noSearchResults = "No colours found 🤔"
    
    static let defaultTheme = "DefaultTheme"
    static let colourSource = "Colour Source:"
    
    static let equals = "="
}

final class BaseViewModel: ObservableObject {
    
    var brands: [Brand]?
    
    var appearanceColors: [AppearanceColor]?
    var appearanceAliaseNames: [AppearanceAliasName]?
    var appearanceAliases: [AppearanceAlias]?
    
    var colourNames: [ColourName]?
    var colourAlphas: [ColourAlpha]?
    var colours: [Colour]?
    var alphas: [Alpha]?
    
    var themeNames: [ThemeName2]?
    var themes: [Theme2]?
    
    var dataManager: DataManager?
    
    init() {
        dataManager = .shared
        load()
    }
    
    private func load() {
        brands = dataManager?.brands()
        
        colourNames = dataManager?.colourNames()
        colourAlphas = dataManager?.colourAlphas()
        colours = dataManager?.colours()
        alphas = dataManager?.allAlphas()
        
        appearanceAliaseNames = dataManager?.aliasNames()
        appearanceColors = dataManager?.appearanceColors()
        appearanceAliases = dataManager?.aliases()
        
        themeNames = dataManager?.themeNames()
        themes = dataManager?.themes()
    }
}

// MARK: - Filter Brands
extension BaseViewModel {
    
    func brands(for brand: BrandName?) -> [Brand]? {
        guard let brand else { return brands }
        return brands?.filter { $0.name == brand.rawValue }
    }
    
    func brand(from searchText: String) -> BrandName? {
        let tokens = searchText.components(separatedBy: String.space)
        let brands = tokens.compactMap { BrandName(rawValue: $0) }
        return brands.first
    }
}

// MARK: - Filter Colour Names
extension BaseViewModel {
    
    func colourNames(for searchString: String?) -> [ColourName]? {
        guard let searchString else { return colourNames }
        if searchString.contains(String.equals) {
            return multiSearch(for: searchString)
        }
        if searchString.hasPrefix(String.hexPrefix) {
            return hexColours(for: searchString)
        }
        let filtered = colourNames?.filter { $0.name?.lowercased().contains(searchString.lowercased()) == true }
        return filtered
    }
    
    private func hexColours(for searchString: String) -> [ColourName]? {
        var searchString = searchString
        if searchString.hasPrefix(String.hexPrefix) {
            searchString = searchString.remove(String.hexPrefix)
        }
        let filtered = colourNames?.filter { ($0.colours as? Set)?.contains(hexValue: searchString) == true }
        return filtered
    }
    
    // e.g. BV=#262D34 BWin=#000000 BildBet=#212529 HeartBingo=#EE1D3A Parimatch=#262626 TalkSport=#0F0F0C WilliamHill=#00ADFF
    private func multiSearch(for searchString: String) -> [ColourName]? {
        let brandColours = searchString.components(separatedBy: .whitespaces)
        var filtered = colourNames
        for brandColourPair in brandColours {
            let brandHex = brandColourPair.components(separatedBy: String.equals)
            guard brandHex.count == 2 else { continue }
            let hexValue = brandHex[1].remove(String.hexPrefix)
            filtered = filtered?.filter({ colourName in
                if let colourSet = colourName.colours as? Set<Colour> {
                    return colourSet.contains(hexValue: hexValue) && colourSet.contains(brandName: brandHex[0])
                }
                return false
            })
        }
        return filtered
    }
}

extension Set where Element == Colour {
    func contains(hexValue: String) -> Bool {
        let filtered = Array(self).filter { $0.hexValue == hexValue }
        return !filtered.isEmpty
    }
    func contains(brandName: String) -> Bool {
        let filtered = Array(self).filter { $0.brand?.name == brandName }
        return !filtered.isEmpty
    }
}
