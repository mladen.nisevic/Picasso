//
//  PicassoApp.swift
//  Picasso
//
//  Created by Mantas Jakimavicius on 01/09/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import SwiftUI

private extension String {
    static let noParseData = "-NoParseData"
}

@main
struct PicassoApp: App, DebugLoggable {
    let persistenceController = PersistenceController.shared
    
    init() {
        if CommandLine.arguments.first(where: { $0 == String.noParseData }) != nil {
            let msg = "\(Self.self):\(#function):\(#line): Not parsing, colour/theme info may be outdated"
            log(message: msg, level: .info)
        } else {
            Populator.shared.depopulate()
            Populator.shared.populate()
        }
        Populator.shared.populateMissingHexValues()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: BaseViewModel())
                .environmentObject(IndividualThemeHelper())
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}

