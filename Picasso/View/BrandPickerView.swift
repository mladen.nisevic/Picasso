//
//  BrandPickerView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 14/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import SwiftUI

private extension String {
    static let any = "Any"
    static let brand = "Brand"
}

struct BrandPickerView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Brand.name, ascending: true)],
        animation: .default)

    private var brandItems: FetchedResults<Brand>
    @ObservedObject var settingsHelper = SettingsHelper.shared

    @ViewBuilder func brandButton(_ brand: Brand, action: @escaping (() -> Void)) -> some View {
        Button(brand.fullName ?? .empty, action: action)
    }

    var body: some View {
        Menu {
            Button(String.any, action: noSelection)
            ForEach(brandItems, id: \.self) { brand in
                brandButton(brand) {
                    settingsHelper.selectedBrand = brand.name
                }
            }
        } label: {
            Text(settingsHelper.selectedBrand ?? .brand)
        }
        .font(.title3)
    }

    func noSelection() {
        settingsHelper.selectedBrand = nil
    }
}

struct BrandPickerView_Previews: PreviewProvider {
    static var previews: some View {
        BrandPickerView()
    }
}
