//
//  AppearanceView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 13/07/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI

let _1_column: [GridItem] = Array(repeating: .init(.flexible()), count: 1)
let _7_column: [GridItem] = Array(repeating: .init(.flexible()), count: 7)

struct AppearanceView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Brand.name, ascending: true)],
        animation: .default)
    private var brandItems: FetchedResults<Brand>

    var body: some View {
        if #available(macCatalyst 16.0, iOS 16, *) {
            NavigationStack {
                VStack {
                    ScrollView {
                        LazyVGrid(columns: _7_column) {
                            ForEach(brandItems, id: \.self) { brand in
                                VStack {
                                    Text("\(brand.name ?? .empty)")
                                        .font(.headline)
                                    AppearanceColorsView(appearanceColors: brand.appearanceColoursArray)
                                }
                            }
                        }
                    }
                }
            }
            .onAppear {
                UserDefaults.standard.set(ViewType.appearanceColor.rawValue, forKey: Key.tabSelection)
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

struct AppearanceColorsView: View {
    var appearanceColors: [AppearanceColor]
    var body: some View {
        VStack {
            ForEach(appearanceColors, id: \.self) { color in
                ColourView2(appearanceColor: color)
            }
        }
    }
}

/// A palette component cell (such as text, background, ...)
struct ColourView2: View {
    var appearanceColor: AppearanceColor
    var body: some View {
        let elementColour = Color(uiColor: UIColor(hex: appearanceColor.hexValue ?? .empty) ?? UIColor())

        return VStack {
            RoundedRectangle(cornerRadius: 1)
                .fill(elementColour)
            Text("\(appearanceColor.name ?? .empty)")
                .foregroundColor(Color.primary)
            Text("\(appearanceColor.hexValue ?? .empty)")
                .foregroundColor(Color.primary)
        }
        .overlay(
            RoundedRectangle(cornerRadius: 1)
                .stroke(Color.black, lineWidth: 0.5)
        )
    }
}

struct AppearanceView_Previews: PreviewProvider {
    static var previews: some View {
        AppearanceView()
    }
}
