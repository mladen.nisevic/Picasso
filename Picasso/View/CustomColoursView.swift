//
//  CustomColoursView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 04/01/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI

extension CGFloat {
    static let dividerSpacing: CGFloat = 20
}

private extension String {
    static let resetColours = "Reset Colours"
}

private extension LocalizedStringKey {
    static let fontSize = LocalizedStringKey(stringLiteral: "Font Size:")
}

extension Int {
    static let defaultFontSize: Int = 16
}

struct CustomColoursView: View {
    
    @State private var keyBridges = KeyBridges()
    @State private var fontSize = Int.defaultFontSize
    
    var body: some View {
        VStack {
            HStack(spacing: .dividerSpacing) {
                Button(String.resetColours) {
                    self.keyBridges = KeyBridges()
                }
                StepperField(title: .fontSize, value: $fontSize)
            }
            List {
                AdaptiveStack(spacing: .dividerSpacing) {
                    CustomColourSelectorView(keyBridges: $keyBridges)
                    Spacer()
                    MatchingThemesView(titleColourKey: keyBridges.title.destination, backgroundColourKey: keyBridges.background.destination)
                }
                BrandComponentsView(keyBridges: keyBridges, fontSize: CGFloat(fontSize))
            }
        }
        .onAppear {
            UserDefaults.standard.set(ViewType.custom.rawValue, forKey: Key.tabSelection)
        }
    }
}

struct CustomColourSelectorView: View {
    @Binding var keyBridges: KeyBridges
    var body: some View {
        VStack {
            HStack {
                Text("Component")
                    .font(.headline)
                    .frame(width: 200, alignment: .trailing)
                Text("Colour")
                    .font(.headline)
                    .frame(width: 200, alignment: .leading)
            }
            IndividualKeyView(name: "Title", keyBridge: $keyBridges.title)
            IndividualKeyView(name: "Subtitle", keyBridge: $keyBridges.subtitle)
            IndividualKeyView(name: "Background", keyBridge: $keyBridges.background)
            IndividualKeyView(name: "Border", keyBridge: $keyBridges.border)
            IndividualKeyView(name: "Accessory Symbol", keyBridge: $keyBridges.accessorySymbol)
            IndividualKeyView(name: "Underline", keyBridge: $keyBridges.underline)
            IndividualKeyView(name: "Card Background", keyBridge: $keyBridges.cardBackground)
        }
        .padding()
    }
}

struct IndividualKeyView: View {
    var name: String
    @Binding var keyBridge: KeyBridge
    var body: some View {
        VStack {
            HStack {
                Text("\(name): ")
                    .font(.headline)
                    .frame(width: 200, alignment: .trailing)
                Picker(String.empty, selection: $keyBridge.destination) {
                    ForEach(PaletteKeyStruct.allCases, id: \.self) { paletteKey in
                        Text(paletteKey.rawValue)
                    }
                }
                .labelsHidden()
                .pickerStyle(.menu)
                .frame(width: 200, alignment: .leading)
            }
            .frame(maxWidth: 450)
        }
    }
}

struct KeyBridges {
    var title: KeyBridge = .init(source: .text, destination: .text)
    var subtitle: KeyBridge = .init(source: .text, destination: .text)
    var background: KeyBridge = .init(source: .background, destination: .background)
    var cardBackground: KeyBridge = .init(source: .card, destination: .card)
    var border: KeyBridge = .init(source: .brand, destination: .brand)
    var accessorySymbol: KeyBridge = .init(source: .brand, destination: .brand)
    var underline: KeyBridge = .init(source: .underline, destination: .underline)
}

// source: the colour to set (title, background, border etc)
// destination: the colour in the palette (any from the brand palette)
struct KeyBridge {
    var source: PaletteKeyStruct
    var destination: PaletteKeyStruct
}

struct BrandComponentsView: View {
    var keyBridges: KeyBridges
    var fontSize: CGFloat
    var body: some View {
        ScrollView {
            LazyVGrid(columns: columns) {
                ForEach(BrandName.allCases) { brand in
                    if let palette = PaletteHelper.shared.palettes[brand] {
                        if let themeColour = ThemeColourSet(with: palette, keyBridges: keyBridges, name: brand.fullName) {
                            AncestorThemeView(themeColour: themeColour, fontSize: fontSize, backgroundColour: UIColor.systemBackground)
                                .padding()
                        }
                    }
                }
            }
        }
        .padding()
    }
}

struct CustomColoursView_Previews: PreviewProvider {
    static var previews: some View {
        CustomColoursView()
    }
}
