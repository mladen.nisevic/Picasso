//
//  AncesterThemeView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 23/12/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import SwiftUI

struct AncestorsView: View {
    var title: String
    var themes: [NSObject]
    var fontSize: CGFloat = DefaultSize.font
    var themesLimited: [NSObject] {
        Array(themes.prefix(5))
    }
    var body: some View {
        VStack {
            Text(title + " (\(themes.count))")
                .font(.headline)
            HStack {
                ForEach(themesLimited, id: \.self) { theme in
                    if let theme = theme as? Theme, let themeColour = ThemeColourSet(theme) {
                        AncestorThemeView(themeColour: themeColour, fontSize: fontSize, backgroundColour: UIColor.secondarySystemBackground)
                            .padding()
                    } else {
                        Text("Invalid object \(theme)")
                    }
                }
            }
        }
    }
}

struct AncestorThemeView: View {
    var themeColour: ThemeColourSet
    var fontSize: CGFloat
    var backgroundColour: UIColor
    @EnvironmentObject var helper: IndividualThemeHelper
    @Environment(\.managedObjectContext) private var viewContext
    var themeViewSize: ThemeViewSize = .small
    var body: some View {
        VStack {
            Text(themeColour.name)
                .paddedAll(backgroundColour: backgroundColour)
                .latoRegular(18)
            VStack(spacing: 2) {
                Text("Title")
                    .paddedAll(backgroundColour: themeColour.background)
                    .latoBold(fontSize)
                    .foregroundColor(Color.init(themeColour.title))
                Text("Subtitle")
                    .paddedAll(backgroundColour: themeColour.background)
                    .latoRegular(fontSize)
                    .foregroundColor(Color.init(themeColour.subtitle))
                Text("Light font")
                    .paddedAll(backgroundColour: themeColour.background)
                    .latoLight(fontSize)
                    .foregroundColor(Color.init(themeColour.title))
                Image(systemName: "applelogo")
                    .paddedAll(backgroundColour: themeColour.background)
                    .foregroundColor(Color.init(themeColour.accessorySymbol))
                    .font(.title)
                Text("Underline")
                    .underline()
                    .paddedAll(backgroundColour: themeColour.background)
                    .latoRegular(fontSize)
                    .foregroundColor(Color.init(themeColour.underline))
                Text("Card Background")
                    .paddedAll(backgroundColour: themeColour.cardBackground)
                    .latoRegular(fontSize)
                    .foregroundColor(Color.init(themeColour.title))
            }
            .overlay(
                RoundedRectangle(cornerRadius: themeViewSize.dimension.cornerRadius)
                    .stroke(Color(uiColor: themeColour.border), lineWidth: themeViewSize.dimension.lineWidth)
            )
        }
        .gesture(
            TapGesture()
                .onEnded { _ in
                    if let theme = themeColour.theme {
                        helper.theTheme = theme
                        addItem(helper.theTheme.nameNoBrand)
                    }
                }
        )
        .contextMenu(
            ContextMenu(menuItems: {
                Button("Copy \(themeColour.theme?.nameNoBrand ?? .empty) to Clipboard", action: {
                    if let themeName = themeColour.theme?.nameNoBrand {
                        print("Copying \(themeName) to clipboard")
                        UIPasteboard.general.string = themeName
                    }
                })
            })
        )
    }
}

// MARK: - Core Data
extension AncestorThemeView {
    func addItem(_ search: String) {
        let newItem = HistoryItem(context: viewContext)
        newItem.timestamp = Date()
        newItem.searchString = search
        newItem.id = UUID()
        newItem.isExact = SettingsHelper.shared.isExactMatch
        newItem.brandSelection = SettingsHelper.shared.selectedBrand

        do {
            try viewContext.save()
        } catch {
// TODO: add proper error handling
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}

extension View {
    func paddedAll(_ padding: CGFloat = 10, backgroundColour: UIColor = .clear) -> some View {
        modifier(Padded(padding: padding, backgroundColour: backgroundColour))
    }
}

struct Padded: ViewModifier {
    var padding: CGFloat = 10
    var backgroundColour: UIColor
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity)
            .padding(.all, padding)
            .background(Color.init(backgroundColour))
    }
}

struct ThemeColourSet {
    var name: String
    var background: UIColor
    var title: UIColor
    var subtitle: UIColor
    var accessorySymbol: UIColor
    var underline: UIColor
    var cardBackground: UIColor
    var border: UIColor
    var theme: Theme?
}

// MARK: - ThemeColour inits
extension ThemeColourSet {
    init?(_ theme: Theme) {
        name = theme.themeName
        background = theme.background()
        title = theme.title()
        subtitle = theme.subtitle()
        accessorySymbol = theme.accessorySymbol()
        underline = theme.underline()
        cardBackground = theme.cardBackground()
        border = theme.border()
        self.theme = theme
    }

    init?(with palette: BrandPalette, keyBridges: KeyBridges, name: String = .empty) {
        self.init(DefaultTheme_BV())
        self.name = name
        title = palette.colour(keyBridges.title.destination)
        subtitle = palette.colour(keyBridges.subtitle.destination)
        background = palette.colour(keyBridges.background.destination)
        border = palette.colour(keyBridges.border.destination)
        accessorySymbol = palette.colour(keyBridges.accessorySymbol.destination)
        cardBackground = palette.colour(keyBridges.cardBackground.destination)
        underline = palette.colour(keyBridges.underline.destination)
    }
}
