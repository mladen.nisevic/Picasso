//
//  ThemeView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 14/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import SwiftUI

extension LocalizedStringKey {
    static let copyNameToClipboard = LocalizedStringKey(stringLiteral: "Copy Theme Name to Clipboard")
}

private extension String {
    static let searchPrompt = "Search Themes"
    static let noSearchResults = "No theme found 🤔"
    static let parsingThemeError = "Not theme? parsing issuse"
}

struct ThemesView: View {

    let themes = ThemesViewModel.shared.themes
    @Environment(\.managedObjectContext) private var viewContext
    @State private var searchText: String = .empty
    @State private var filteredElements = [NSObject]()
    @ObservedObject var settingsHelper = SettingsHelper.shared
    @State var searching = false
    @EnvironmentObject var helper: IndividualThemeHelper
    @State var isExact = false

    var body: some View {
        if #available(macCatalyst 16.0, iOS 16, *) {
            NavigationStack {
                VStack {
                    HStack(spacing: 20) {
                        HistoryPickerView(searchText: $searchText, isExact: $isExact, selectedBrand: $settingsHelper.selectedBrand)
                            .frame(maxWidth: 150, alignment: .leading)
                        BrandPickerView()
                            .frame(maxWidth: 150, alignment: .leading)
                        Toggle(ButtonTitle.exact, isOn: $isExact)
                            .onChange(of: isExact, perform: { value in
                                SettingsHelper.shared.isExactMatch = value
                            })
                            .keyboardShortcut(KeyEquivalent("e"), modifiers: .control)
                            .frame(maxWidth: 170, alignment: .leading)
                        Spacer().frame(maxWidth: .infinity, alignment: .leading)
                    }
                    .padding([.leading, .trailing], 20)
                    .padding([.bottom], 60)
                    ScrollView {
                        LazyVGrid(columns: columns) {
                            ForEach(filteredElements, id: \.self) { theme in
                                if let theme = theme as? Theme {
                                    NavigationLink(destination: IndividualThemeView().onAppear(perform: {
                                        helper.theTheme = theme
                                        addItem(helper.theTheme.nameNoBrand)
                                    })) {
                                        ThemeView(theme: theme)
                                    }
                                } else {
                                    Text(String.parsingThemeError).font(.largeTitle)
                                }
                            }
                        }
                        .background(Color(uiColor: UIColor.secondarySystemBackground))
                        
                        VStack {
                            if filteredElements.isEmpty {
                                Text(String.noSearchResults).font(.largeTitle)
                            }
                        }
                        .padding([.top, .bottom], 200)
                    }
                }
            }
            .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .always), prompt: String.searchPrompt)
            .onSubmit(of: .search) {
                filterElements()
            }
            .onChange(of: searchText) { _ in
                filterElements()
            }
            .onAppear {
                searchText = SettingsHelper.shared.lastSearch
                UserDefaults.standard.set(ViewType.themes.rawValue, forKey: Key.tabSelection)
                isExact = UserDefaults.standard.bool(forKey: Key.isExact)
                filterElements()
            }
            .onDisappear {
                SettingsHelper.shared.lastSearch = searchText
            }
        } else {
            // Fallback on earlier versions
        }
    }

    private func filterElements() {
        let brandFiltered: [NSObject] = {
            if let brand = settingsHelper.selectedBrand {
                return themes.filter { filterBrands($0, brandName: brand) }
            } else {
                return themes
            }
        }()

        let filtered: [NSObject] = {
            if searchText.isEmpty {
                return brandFiltered
            } else {
                return brandFiltered.filter { filterThemes($0, searchText: self.searchText, isExact: settingsHelper.isExactMatch) }
            }
        }()

        filteredElements = filtered
    }

    func filterBrands(_ element: Any, brandName: String) -> Bool {
        guard let theme = element as? Theme else { return true }
        return theme.fullName.hasSuffix(brandName)
    }

    func filterThemes(_ element: Any, searchText: String, isExact: Bool) -> Bool {
        guard let theme = element as? Theme else { return true }
        return self.matchSearch(theme.justName.lowercased(), searchText: self.searchText.lowercased(), isExact: isExact)
    }

    func matchSearch(_ element: String, searchText: String, isExact: Bool) -> Bool {
        guard !searchText.isEmpty else { return true }
        if searchText.isQuoted || isExact {
            var justSearchText = searchText
            if searchText.isQuoted && searchText.count > 2 {
                justSearchText.removeFirst()
                justSearchText.removeLast()
            }
            return element == justSearchText
        }
        return element.contains(searchText)
    }
}

// MARK: - Core Data
extension ThemesView {
    func addItem(_ search: String) {
        let newItem = HistoryItem(context: viewContext)
        newItem.timestamp = Date()
        newItem.searchString = search
        newItem.id = UUID()
        newItem.isExact = SettingsHelper.shared.isExactMatch
        newItem.brandSelection = SettingsHelper.shared.selectedBrand

        do {
            try viewContext.save()
        } catch {
// TODO: add proper error handling
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}

enum ThemeViewSize {
    case small, medium, large
    var dimension: ThemeDimensions {
        switch self {
        case .small: return ThemeDimensions(cornerRadius: 5, lineWidth: 6, rectangleHeight: 20, padding: 2)
        case .medium: return ThemeDimensions(cornerRadius: 5, lineWidth: 3, rectangleHeight: 20, padding: 2)
        case .large: return ThemeDimensions(cornerRadius: 40, lineWidth: 10, rectangleHeight: 220, padding: 20)
        }
    }
    struct ThemeDimensions {
        var cornerRadius: CGFloat
        var lineWidth: CGFloat
        var rectangleHeight: CGFloat
        var padding: CGFloat
    }
}

struct ThemeView: View {

    var theme: Theme
#if targetEnvironment(macCatalyst)
    var themeViewSize: ThemeViewSize = .large
#else
    var themeViewSize: ThemeViewSize = .medium
#endif

    var body: some View {

        Group {
            ZStack {
                RoundedRectangle(cornerRadius: themeViewSize.dimension.cornerRadius, style: .continuous)
                    .fill(Color.init(theme.background()))
                    .frame(minWidth: 0, maxWidth: UIScreen.main.bounds.width)
                VStack(alignment: .leading) {
                    Text(theme.brand)
                        .help(Text("Theme brand colour"))
                        .foregroundColor(Color(theme.title()))
                        .font(.title2)
                        .padding()
                    HStack {
                        #if targetEnvironment(macCatalyst)
                        Spacer()
                        #endif
                        Image(systemName: "applelogo")
                            .help(Text("Accessory symbol colour"))
                            .foregroundColor(Color.init(theme.accessorySymbol()))
                            .padding(.bottom, 3)
                            .font(.title)
                        VStack {
                            Text(String(describing: theme.background().toHexString().uppercased()))
                            StrokeText(text: "\(theme.themeName)", width: 0.5, color: .gray)
                                .help(Text("Title colour"))
                                .foregroundColor(Color.init(theme.title()))
                                .font(.title2)
                                .lineLimit(nil)
                            Divider()
                                .background(Color.init(theme.underline()))
                            StrokeText(text: "Subtitle", width: 0.5, color: .gray)
                                .help(Text("Subtitle colour"))
                                .foregroundColor(Color.init(theme.subtitle()))
                                .lineLimit(nil)
                            Text("Underline")
                                .underline()
                                .foregroundColor(Color.init(theme.underline()))
                        }
                        Rectangle()
                            .fill(Color.init(theme.cardBackground()))
                            .cornerRadius(themeViewSize.dimension.cornerRadius, corners: [.topRight, .bottomRight])
                            .frame(width: 30, height: themeViewSize.dimension.rectangleHeight, alignment: .trailing)
                            .padding(themeViewSize.dimension.padding)
                    }
                    .padding()
                    .frame(minWidth: 0, maxWidth: UIScreen.main.bounds.width)
                }
            }
            .overlay(
                RoundedRectangle(cornerRadius: themeViewSize.dimension.cornerRadius)
                    .stroke(Color(uiColor: theme.border()), lineWidth: themeViewSize.dimension.lineWidth)
            )
        }
        .padding(themeViewSize.dimension.padding)
        .contextMenu(
            ContextMenu(menuItems: {
                Button("Copy \(theme.nameNoBrand) to Clipboard", action: {
                    UIPasteboard.general.string = theme.nameNoBrand
                })
            })
        )
    }
}

struct ThemeView_Previews: PreviewProvider {
    static var previews: some View {
        ThemeView(theme: DefaultTheme_BV())
    }
}
