//
//  MatchingThemes.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 02/06/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI

private extension String {
    static let themesMatchingTitleBackground = "Themes Matching Title & Background Colours"
    static let title = "Title"
    static let background = "Background"
}

struct MatchingThemesView: View {
    
    var titleColourKey: PaletteKeyStruct
    var backgroundColourKey: PaletteKeyStruct
    let themes = ThemesViewModel.shared.themes
    @State var selection: BrandName = .BV
    @State var includeTitle = true
    @State var includeBackground = true
    @EnvironmentObject var helper: IndividualThemeHelper
    @State private var showEditView = false
    @State private var selectedTheme: Theme = DefaultTheme_BV()
    var palette: BrandPalette {
        PaletteHelper.shared.palettes[selection] ?? BrandPalette.test
    }
    var titleColour: UIColor? {
        PaletteHelper.shared.palettes[selection]?.colour(titleColourKey)
    }
    var backgroundColour: UIColor? {
        PaletteHelper.shared.palettes[selection]?.colour(backgroundColourKey)
    }
    
    var matchingThemes: [NSObject] {
        var things = [NSObject]()
        for theme in themes {
            guard let theme2 = theme as? Theme else { continue }
            let titleRule = Rule(include: includeTitle, rule: theme2.title() == titleColour)
            let backgroundRule = Rule(include: includeBackground, rule: theme2.background() == backgroundColour)
            if titleRule.result && backgroundRule.result {
                things.append(theme)
            }
        }
        return things
    }
    var body: some View {
        ZStack {
            VStack(alignment: .leading) {
                Text(String.themesMatchingTitleBackground + " (\(matchingThemes.count))")
                    .font(.headline)
                Text(String.empty)
                HStack {
                    Picker(selection: $selection,
                           label: Text("Picker"),
                           content: {
                        ForEach(BrandName.allCases) { option in
                            Text(option.fullName).tag(option)
                        }
                    })
                        .padding()
                        .pickerStyle(MenuPickerStyle())
                    VStack(alignment: .leading) {
                        HStack {
                            Toggle(String.title, isOn: $includeTitle)
                            Text(titleColour?.toHexString() ?? .empty)
                            Spacer(minLength: 20)
                        }
                        HStack {
                            Toggle(String.background, isOn: $includeBackground)
                            Text(backgroundColour?.toHexString() ?? .empty)
                            Spacer(minLength: 20)
                        }
                    }
                }
                List(matchingThemes, id: \.self) { theme in
                    if let theme = theme as? Theme {
                        Text("\(theme.justName)")
                            .contextMenu(
                                ContextMenu(menuItems: {
                                    Button("Show \(theme.nameNoBrand)", action: {
                                        self.showEditView = true
                                        self.selectedTheme = theme
                                    })
                                    Button("Copy \(theme.nameNoBrand) to Clipboard", action: {
                                        UIPasteboard.general.string = theme.nameNoBrand
                                    })
                                })
                            )
                    }
                }
                .listStyle(PlainListStyle())
                NavigationLink(destination: IndividualThemeView().onAppear(perform: {
                    helper.theTheme = self.selectedTheme
                }), isActive: $showEditView) {
                    EmptyView()
                }

            }
        }
    }
}

public extension UIColor {
    /// As colour values are CGFloat and can have many decimal places, sometimes the same colours may appear different in comparison
    /// this compares RGBA values of two colours up to 6 decimal places
    static func == (l: UIColor, r: UIColor?) -> Bool {
        guard let r = r else { return false }
        var l_red = CGFloat(0); var l_green = CGFloat(0); var l_blue = CGFloat(0); var l_alpha = CGFloat(0)
        guard l.getRed(&l_red, green: &l_green, blue: &l_blue, alpha: &l_alpha) else { return false }
        var r_red = CGFloat(0); var r_green = CGFloat(0); var r_blue = CGFloat(0); var r_alpha = CGFloat(0)
        guard r.getRed(&r_red, green: &r_green, blue: &r_blue, alpha: &r_alpha) else { return false }
        return l_red ==~ r_red && l_green ==~ r_green && l_blue ==~ r_blue && l_alpha ==~ r_alpha
    }
}

infix operator ==~: AdditionPrecedence
private extension CGFloat {
    static func ==~(lhs: CGFloat, rhs: CGFloat) -> Bool {
        roundToDecimalPlaces(lhs) == roundToDecimalPlaces(rhs)
    }
}

// 1_000_000 - 6 decimal places
private func roundToDecimalPlaces(_ value: Double, rounderer: Double = 1_000_000) -> CGFloat {
    round(value * rounderer) / rounderer
}

private struct Rule {
    var include: Bool
    var rule: Bool
    var result: Bool {
        if !include {
            return true
        }
        return rule
    }
}
