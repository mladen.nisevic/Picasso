//
//  ThemePickerView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 14/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import SwiftUI

struct ThemeName: Identifiable {
    var id: String
}

struct ThemePickerView: View {

    @ObservedObject var themeNamesHelper = ThemeNameHelper.shared

    var thingies: [ThemeName] {
        themeNamesHelper.themeList()
    }

    var body: some View {
        Text("Theme")
    }
}

struct ThemePickerView_Previews: PreviewProvider {
    static var previews: some View {
        ThemePickerView()
    }
}
