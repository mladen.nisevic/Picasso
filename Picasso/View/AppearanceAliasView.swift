//
//  AppearanceAliasView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 13/07/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI

private extension String {
    static let noSearchResults = "No aliases found 🤔"
}

private extension String {
    static let colorHexContains = "appearanceAliases.color.hexValue CONTAINS[cd] %@"
    static let descriptiveNameContains = "appearanceAliases.color.descriptiveName CONTAINS[cd] %@"
    
    static let searchPrompt = "Search Appearance Color for hex-colour (FFFFFF) or name (input27), Brand (BV), full text. <Enter/Return>"
    static let appearanceAliasSearchText = "AppearanceAliasSearchText"
}

struct RowColour: Equatable {
    var brandName: String?
    var hexColour: String?
    var description: String {
        if let brand = brandName, let hex = hexColour {
            return "\(brand)=\(hex)"
        }
        return .empty
    }
}
extension Array where Element == RowColour {
    var description: String {
        var str = String.empty
        for el in self {
            str += el.description
            if el != self.last {
                str += String.space
            }
        }
        return str
    }
}

struct AppearanceAliasView: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Brand.name, ascending: true)],
        animation: .default)
    private var brands: FetchedResults<Brand>
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \AppearanceAlias.name, ascending: true)],
        animation: .default)
    private var aliases: FetchedResults<AppearanceAlias>
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \AppearanceColor.name, ascending: true)],
        animation: .default)
    private var appearanceColors: FetchedResults<AppearanceColor>

    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \AppearanceAliasName.name, ascending: true)],
        animation: .default)
    private var aliasNames: FetchedResults<AppearanceAliasName>

    @AppStorage(.appearanceAliasSearchText) private var searchText: String = .empty

    var body: some View {
        if #available(macCatalyst 16.0, iOS 16, *) {
            NavigationStack {
                VStack {
                    AliasHeaderView(brands: brands)
                        .frame(height: .headerHeight)
                        .cornerRadius(.cornerRadius)
                    ScrollView {
                        if !aliasNames.isEmpty {
                            ForEach(aliasNames.indices, id: \.self) { index in
                                AliasRowView(searchText: $searchText, aliases: aliases, appearanceColors: appearanceColors, brands: brands, aliasName: aliasNames[index], rowNo: index)
                                    .frame(height: .rowHeight)
                                    .cornerRadius(.cornerRadius)
                            }
                        } else {
                            Spacer(minLength: .noResultsSpacer)
                            Text(String.noSearchResults).font(.largeTitle)
                        }
                    }
                }
            }
            .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .automatic), prompt: String.searchPrompt)
            .onSubmit(of: .search) {
                let searchResult = getPredicateText(searchText)
                if let hex = searchResult.hexColour {
                    aliasNames.nsPredicate = NSPredicate(format: .colorHexContains, hex)
                } else if let colName = searchResult.colourName {
                    aliasNames.nsPredicate = NSPredicate(format: .nameContains, colName)
                } else if let misc = searchResult.misc {
                    aliasNames.nsPredicate = anythingPredicate(misc)
                } else {
                    aliasNames.nsPredicate = nil
                }
                brands.nsPredicate = searchResult.brandName == nil ? nil : NSPredicate(format: .nameContains, searchResult.brandName!)
            }
            //        .onChange(of: searchText) { newValue in
            // Tried. It's very slow.
            //        }
            .padding()
            .onAppear {
                UserDefaults.standard.set(ViewType.appearanceAlias.rawValue, forKey: Key.tabSelection)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func getPredicateText(_ inputText: String) -> ColourSearch {
        var colourSearch = ColourSearch()
        guard !inputText.isEmpty else { return colourSearch }
        let tokens = inputText.components(separatedBy: String.space)
        tokens.forEach { token in
            if UIColor.isValid(hexCode: token) {
                colourSearch.hexColour = token
            } else if BrandName(rawValue: token) != nil {
                colourSearch.brandName = token
            } else {
                colourSearch.misc = token
            }
        }
        return colourSearch
    }
    
    private func anythingPredicate(_ text: String) -> NSPredicate {
        var predicates = [NSPredicate]()
        predicates.append(NSPredicate(format: .descriptiveNameContains, text))
        predicates.append(NSPredicate(format: .nameContains, text))
        predicates.append(NSPredicate(format: .colorHexContains, text))
        return NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
    }
}

struct AliasHeaderView: View {
    var brands: FetchedResults<Brand>
    var body: some View {
        GeometryReader { geometry in
            HStack {
                CornerHeaderCellView()
                ForEach(brands, id: \.self) { brand in
                    HeaderCellView(brand: brand)
                }
            }
        }
    }
}

struct AliasRowView: View {
    @Binding var searchText: String
    var aliases: FetchedResults<AppearanceAlias>
    var appearanceColors: FetchedResults<AppearanceColor>
    var brands: FetchedResults<Brand>
    var aliasName: AppearanceAliasName
    @State var rowColours: [RowColour] = []
    var rowNo: Int
    var body: some View {
        GeometryReader { geometry in
            HStack {
                AliasFirstColumnView(rowNo: rowNo + 1, name: aliasName.name ?? .empty)
                    .contextMenu(
                        ContextMenu(menuItems: {
                            Button("Copy row colours for \(aliasName.name ?? "")", action: {
                                rowColours.removeAll()
                                populateRowData()
                                UIPasteboard.general.string = rowColours.description
                                print("Alias: \(rowColours.description)")
                            })
                        })
                    )
                ForEach(brands, id: \.self) { brand in
                    AliasColourView(searchText: $searchText, colour: alias(appearanceColors, brand: brand, aliasName: aliasName), rowHeight: .rowHeight)
                }
            }
        }
        .onTapGesture {
            rowColours.removeAll()
            populateRowData()
        }
        .onAppear {
            rowColours.removeAll()
            populateRowData()
        }
    }
    
    private func alias(_ appearanceColors: FetchedResults<AppearanceColor>, brand: Brand, aliasName: AppearanceAliasName) -> AppearanceColor? {
        let colour = aliases.first(where: { $0.aliasName == aliasName && $0.color?.brand == brand } )?.color
        return colour
    }
    
    private func populateRowData() {
        for brand in brands {
            let colour = aliases.first(where: { $0.aliasName == aliasName && $0.color?.brand == brand } )?.color
            rowColours.append(RowColour(brandName: brand.name, hexColour: colour?.hexValue))
        }
    }
}

struct AliasFirstColumnView: View {
    let rowNo: Int
    let name: String
    var body: some View {
        GeometryReader { metrics in
            VStack {
                Text("\(rowNo).")
                    .font(.headline)
                    .padding()
                    .frame(maxWidth: .infinity, alignment: .center)
                    .frame(height: metrics.size.height * 0.4)
                    .background(.quaternary)
                    .cornerRadius(.cornerRadius)
                Text("\(name)")
                    .font(.headline)
                    .padding()
                    .frame(maxWidth: .infinity, alignment: .center)
                    .frame(height: metrics.size.height * 0.4)
                    .background(.quaternary)
                    .cornerRadius(.cornerRadius)
            }
        }
        .padding(.all, .cellPadding)
    }
}

struct AliasColourView: View {
    @Binding var searchText: String
    var colour: AppearanceColor?
    let rowHeight: CGFloat
    var thirdHeight: CGFloat {
        (rowHeight - 4 * .cellPadding)/3
    }
    var body: some View {
        var elementColour: Color = .clear
        if let hex = colour?.hexValue, !hex.isEmpty, let uiColor = UIColor(hex: hex) {
            elementColour = Color(uiColor: uiColor)
        }
        
        return GeometryReader { metrics in
            VStack {
                if elementColour == .clear {
                    ClearColourView(rowHeight: metrics.size.height * 0.4)
                        .cornerRadius(.cornerRadius, corners: [.topLeft, .topRight])
                        .frame(height: metrics.size.height * 0.4)
                } else {
                    RoundedRectangle(cornerRadius: 1)
                        .fill(elementColour)
                        .cornerRadius(.cornerRadius, corners: [.topLeft, .topRight])
                        .frame(height: metrics.size.height * 0.4)
                }
                Text(colour?.hexValue ?? "")
                    .foregroundColor(.primary)
                    .frame(height: metrics.size.height * 0.25)
                    .font(.headline)
                Rectangle()
                    .fill(.clear)
                    .frame(height: metrics.size.height * 0.2)
            }
            .contextMenu(
                ContextMenu(menuItems: {
                    Button("\(colour?.descriptiveName ?? "")", action: {})
                    Button(.searchByColour + "\(colour?.hexValue ?? "")", action: {
                        searchText = colour?.hexValue ?? ""
                    })
                    Button(.searchByColourAndBrand + "\(colour?.hexValue ?? ""), \(colour?.brand?.name ?? "")", action: {
                        searchText = "\(colour?.hexValue ?? "") \(colour?.brand?.name ?? "")"
                    })
                    Button(.copyHexValue + "\(colour?.hexValue ?? "") to clipboard", action: {
                        UIPasteboard.general.string = colour?.hexValue
                    })
                })
            )
        }
        .overlay(
            RoundedRectangle(cornerRadius: .cornerRadius)
                .stroke(.quaternary, lineWidth: 1)
        )
        .padding(.all, .cellPadding)
    }
}

struct AppearanceAliasView_Previews: PreviewProvider {
    static var previews: some View {
        AppearanceAliasView()
    }
}
