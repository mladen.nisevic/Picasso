//
//  HistoryPickerView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 01/06/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI

private extension String {
    static let history = "History"
    static let clearHistory = "Clear History"
}

struct HistoryPickerView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \HistoryItem.timestamp, ascending: false)],
        animation: .default)

    private var historyItems: FetchedResults<HistoryItem>
    @Binding var searchText: String
    @Binding var isExact: Bool
    @Binding var selectedBrand: String?
    @ViewBuilder func historyButton(_ historyItem: String, action: @escaping (() -> Void)) -> some View {
        Button(historyItem, action: action)
    }
    
    var body: some View {
        Menu {
            ForEach(historyItems, id: \.self) { hist in
                historyButton(hist.searchString ?? .empty) {
                    searchText = hist.searchString?.dethemed ?? .empty
                    isExact = hist.isExact
                    if let brandString = hist.brandSelection,
                        let brand = BrandName(rawValue: brandString) {
                        selectedBrand = brand.rawValue
                    } else {
                        selectedBrand = nil
                    }
                }
            }
            Divider()
            Button {
                deleteHistory()
            } label: {
                Text(String.clearHistory)
            }
        } label: {
            Text(String.history)
        }
        .font(.title3)
    }
    
    func deleteHistory() {
        historyItems.forEach(viewContext.delete)
        do {
            try viewContext.save()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}

struct HistoryPickerView_Previews: PreviewProvider {
    static var previews: some View {
        Text("not implemented")
    }
}
