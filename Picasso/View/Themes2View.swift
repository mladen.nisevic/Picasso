//
//  PalettesView2.swift
//  Picasso
//
//  Created by Mladen Nisevic on 03/01/2023.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI

private extension String {
    static let background = "background"
    static let brand = "brand"
    static let text = "text"
    static let colourName = "Colour Name"
    
    static let searchPrompt = "Search Themes by name, hex-colour (FFFFFF) or colour name (yellow), Brand (BV), or sets of colours (i.e. title BV=FFFFFF BildBet=FFFFFF...) <Enter/Return>"
    static let noSearchResults = "No themes found 🤔"
    
    static let title = "Title"
    static let subtitle = "Subtitle"
    static let body = "Body"
    static let underline = "Underline"
    static let appleLogo = "applelogo"
    static let cardBackground = "Card Background"
    
    static let copyRowColoursFor = "Copy row colours for:"
    
    static let colourSource = "ColourSource"
    static let themesViewSearchText = "ThemesViewSearchText"
}

private extension CGFloat {
    static let column0Width: CGFloat = 100
    static let brandBorderWidth: CGFloat = 5
}

struct Themes2View: View {
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \ColourName.name, ascending: true)],
        animation: .default)
    var colourNames: FetchedResults<ColourName>
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Brand.name, ascending: true)],
        animation: .default)
    var brands: FetchedResults<Brand>
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \ThemeName2.name, ascending: true)],
        animation: .default)
    private var themeNames: FetchedResults<ThemeName2>
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Theme2.name, ascending: true)],
        animation: .default)
    var themes: FetchedResults<Theme2>
    
    @State var cellWidth: CGFloat = 0
    @AppStorage(.themesViewSearchText) private var searchText: String = .empty
    
    var body: some View {
        if #available(macCatalyst 16.0, iOS 16, *) {
            NavigationStack {
                VStack {
                    AliasHeaderView(brands: brands)
                        .frame(height: .headerHeight)
                        .cornerRadius(.cornerRadius)
                    ScrollView {
                        if !themeNames.isEmpty {
                            let columns = columns(brands.count)
                            LazyVGrid(columns: columns) {
                                ForEach(themeNames.indices, id: \.self) { themeNameIndex in
                                    ForEach(0..<columns.count, id: \.self) { index in
                                        let themeName = themeNames[themeNameIndex]
                                        if index == .zero {
                                            FirstColumnView(rowNo: themeNameIndex, name: themeName.name ?? .empty)
                                        } else {
                                            Theme2ColonView(searchText: $searchText, theme: theme(themeName: themeName, brand: brands[index - 1]))
                                        }
                                    }
                                }
                            }
                        } else {
                            Spacer(minLength: .noResultsSpacer)
                            Text(String.noSearchResults).font(.largeTitle)
                        }
                    }
                }
            }
            .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .automatic), prompt: String.searchPrompt)
            .disableAutocorrection(true)
            .onSubmit(of: .search) {
                themeNames.nsPredicate = getThemesNamesPredicate(searchText)
                brands.nsPredicate = getBrandPredicate(searchText)
            }
            .padding()
            .onAppear {
                UserDefaults.standard.set(ViewType.themes2.rawValue, forKey: Key.tabSelection)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func columns(_ count: Int) -> [GridItem] {
        Array(repeating: .init(.flexible()), count: count + 1)
    }
    
    private func theme(themeName: ThemeName2, brand: Brand) -> Theme2? {
        let theme = themes.first(where: { $0.themeName == themeName && $0.brand == brand })
        return theme
    }
}

struct ThemesFirstColumnView: View {
    let rowNo: Int
    let name: String
    var themes: FetchedResults<Theme2>
    var brands: FetchedResults<Brand>
    @State var rowColours: [RowColour] = []
    var body: some View {
        GeometryReader { metrics in
            VStack {
                Text("\(rowNo).")
                    .font(.headline)
                    .padding()
                    .frame(maxWidth: .infinity, alignment: .center)
                    .frame(height: metrics.size.height * 0.4)
                    .background(.quaternary)
                    .cornerRadius(.cornerRadius)
                Text("\(name)")
                    .font(.headline)
                    .padding()
                    .frame(maxWidth: .infinity, alignment: .center)
                    .frame(height: metrics.size.height * 0.4)
                    .background(.quaternary)
                    .cornerRadius(.cornerRadius)
            }
        }
        .padding(.all, .cellPadding)
        .frame(height: .rowHeight)
        .contextMenu(
            ContextMenu(menuItems: {
                    Button("Copy '\(name)' to Clipboard", action: {
                        UIPasteboard.general.string = name
                    })
                    Divider()
                Text(String.copyRowColoursFor)
                ForEach(ColourNameEnum.allCases) { colourName in
                    Button("\(colourName.rawValue)", action: {
                        copyRowColoursToClipboard(colourName)
                    })
                }
            })
        )
    }
    
    fileprivate func copyRowColoursToClipboard(_ colourName: ColourNameEnum) {
        rowColours.removeAll()
        populateRowData(colourName)
        UIPasteboard.general.string = rowColours.description
        print("\(name): \(rowColours.description)")
    }
    
    private func populateRowData(_ colourName: ColourNameEnum) {
        rowColours.removeAll()
        for brand in brands {
            let theme = themes.first(where: { $0.themeName?.name == name && $0.brand == brand } )
            rowColours.append(RowColour(brandName: brand.name, hexColour: theme?.colour(for: colourName)?.colour?.hexValue))
        }
    }
}

struct Theme2ColonView: View {
    @Binding var searchText: String
    var theme: Theme2?
    var fontSize: CGFloat = CGFloat(Int.defaultFontSize)
    @EnvironmentObject var helper: IndividualThemeHelper
    @Environment(\.managedObjectContext) private var viewContext
    var themeViewSize: ThemeViewSize = .small
    var body: some View {
        ZStack {
            Color(uiColor: backgroundColor())
            VStack {
                HStack(spacing: 2) {
                    Text(String.title)
                        .paddedAll()
                        .latoBold(fontSize)
                        .foregroundColor(Color.init(theme?.title?.uiColor ?? UIColor.clear))
                    Text(String.underline)
                        .underline()
                        .paddedAll()
                        .latoRegular(fontSize)
                        .foregroundColor(Color.init(theme?.underline?.uiColor ?? UIColor.clear))
                }
                HStack(spacing: 2) {
                    Text(String.subtitle)
                        .paddedAll()
                        .latoRegular(fontSize)
                        .foregroundColor(Color.init(theme?.subtitle?.uiColor ?? UIColor.clear))
                    Image(systemName: String.appleLogo)
                        .paddedAll()
                        .foregroundColor(Color.init(theme?.accessorySymbol?.uiColor ?? UIColor.clear))
                        .font(.title)
                    CardBackgroundView(text: .cardBackground, colour: theme?.cardBackground?.uiColor ?? .clear, fontSize: fontSize)
                }
                HStack(spacing: 2) {
                    Text(String.body)
                        .paddedAll()
                        .latoRegular(fontSize)
                        .foregroundColor(Color.init(theme?.body?.uiColor ?? UIColor.clear))
                    if let parent = theme?.parent {
                        Text("\(parent.name ?? .empty)")
                            .paddedAll()
                            .latoRegular(fontSize)
                            .foregroundColor(Color.init(backgroundColor().contrasty))
                    } else {
                        Text(String.empty)
                    }
                }
            }
        }
        .overlay(
            RoundedRectangle(cornerRadius: themeViewSize.dimension.cornerRadius)
                .stroke(Color(uiColor: theme?.border?.uiColor ?? UIColor.clear), lineWidth: themeViewSize.dimension.lineWidth)
        )
        .padding(.all, .cellPadding)
        .contextMenu(
            ContextMenu(menuItems: {
                if let parent = theme?.parent?.name {
                    Button(.searchForParent + parent, action: {
                        searchText = parent
                    })
                }
                if let name = theme?.name {
                    Button("Copy '\(name)' to Clipboard", action: {
                        UIPasteboard.general.string = name
                    })
                    Divider()
                }
                Text(String.copyHexValue)
                ForEach(ColourNameEnum.allCases) { colourName in
                    if let col = theme?.colour(for: colourName), let hexValue = col.hexValue {
                        Button("\(colourName.rawValue): \(hex(hexValue, alpha: col.alphaValue))", action: {
                            UIPasteboard.general.string = hexValue
                            print("\(colourName.rawValue): \(hexValue)")
                        })
                    }
                }
                Divider()
                Button("Copy all colours hex values to Clipboard", action: {
                    UIPasteboard.general.string = theme?.hexValues ?? .empty
                })
                Button("Copy all colours' definitions to Clipboard", action: {
                    UIPasteboard.general.string = theme?.definitionStrings ?? .empty
                })
                Button("Copy complete genesis to Clipboard", action: {
                    let text = "\(theme?.name ?? .empty)\n" + (theme?.themeGenesis ?? .empty)
                    UIPasteboard.general.string = text
                })
            })
        )
    }
    private func backgroundColor() -> UIColor {
        theme?.background?.uiColor ?? UIColor.systemBlue
    }
}

struct CardBackgroundView: View {
    var text: String
    var colour: UIColor
    var fontSize: CGFloat
    var body: some View {
        Text(String.cardBackground)
            .paddedAll(backgroundColour: colour)
            .latoRegular(fontSize)
            .foregroundColor(Color.init(colour.contrasty))
            .cornerRadius(15, corners: [.topLeft, .bottomLeft])
    }
}

struct Themes2View_Previews: PreviewProvider {
    static var previews: some View {
        Themes2View()
    }
}
