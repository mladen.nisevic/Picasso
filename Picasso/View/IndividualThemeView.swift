//
//  IndividualThemeView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 01/11/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import SwiftUI

/**
 
 func background() -> UIColor
 func cardBackground() -> UIColor
 func title() -> UIColor
 func subtitle() -> UIColor
 func border() -> UIColor
 func underline() -> UIColor
 func accessorySymbol() -> UIColor
 func shape() -> ViewShape
 
 */

enum ThemeComponent: String {
    case background, cardBackground, title, subtitle, border, underline, accessorySymbol
    static func comps(_ names: [String]) -> [ThemeComponent] {
        var components = [ThemeComponent]()
        names.forEach { name in
            if let comp = ThemeComponent(rawValue: name) {
                components.append(comp)
            }
        }
        return components
    }
}

struct IndividualThemeView: View {
    @EnvironmentObject var helper: IndividualThemeHelper
    let allThemes = ThemesViewModel.shared.themes
    var ancestorThemesObjects: [NSObject] {
        let themeAncestorTypes = superClasses(for: type(of: helper.theTheme).self)

        let objects = allThemes.filter { nsObject in
            isType(of: nsObject, in: themeAncestorTypes) && nsObject !== helper.theTheme
        }
        let sorted = objects.sorted { o1, o2 in
            o1.superclass != type(of: o2)
        }
        return sorted
    }

    var childThemeObjects: [NSObject] {
        let objects = allThemes.filter { nsObject in
            nsObject.superclass == type(of: helper.theTheme)
        }
        return objects
    }

    var body: some View {
        ScrollView {
            LazyVGrid(columns: columns) {
                ThemeView(theme: helper.theTheme)
                    .padding()
                ElementaryColoursView(theme: helper.theTheme)
                AdditionalColoursView(theme: helper.theTheme)
                AncestorsView(title: "Ancestor Themes", themes: ancestorThemesObjects)
                AncestorsView(title: "Descendent Themes", themes: childThemeObjects)
                if childThemeObjects.count > 5 {
                    DescendentListView(matchingThemes: childThemeObjects)
                }
            }
                .navigationTitle("\(helper.theTheme.brand): \(helper.theTheme.themeName)")
                .navigationBarTitleDisplayMode(.inline)
        }
        .background(Color(uiColor: UIColor.secondarySystemBackground))
    }
}

struct ElementaryColoursView: View {
    var theme: Theme
    var body: some View {
        VStack(alignment: .leading) {
            ThemeColourView(name: .background, colour: theme.background())
            ThemeColourView(name: .cardBackground, colour: theme.cardBackground())
            ThemeColourView(name: .title, colour: theme.title())
            ThemeColourView(name: .subtitle, colour: theme.subtitle())
            ThemeColourView(name: .border, colour: theme.border())
            ThemeColourView(name: .underline, colour: theme.underline())
            ThemeColourView(name: .accessorySymbol, colour: theme.accessorySymbol())
        }
        .padding()
    }
}

struct AdditionalColoursView: View {
    var theme: Theme
    var body: some View {
        VStack(alignment: .leading) {
            Text("Overriden by the theme")
                .font(.headline)
                .paddedAll()
            if !theme.isDefault {
                ForEach(theme.colourComponentNames(), id: \.self) { methodName in
                    Text(methodName.rawValue)
                        .paddedAll()
                }
            }
        }
        .padding()
    }
}

struct ThemeColourView: View {
    var name: ThemeComponent
    var colour: UIColor
    var emphasise: Bool = false
    var body: some View {
        VStack {
            RoundedRectangle(cornerRadius: 1)
                .fill(Color(uiColor: colour))
            ColourNameValueView(name: name.rawValue, value: colour.accessibilityName)
            ColourNameValueView(name: .empty, value: colour.toHexString())
        }
        .overlay(
                RoundedRectangle(cornerRadius: 5)
                    .stroke(Color.black, lineWidth: emphasise ? 5 : 1)
            )
    }
}

struct ColourNameValueView: View {
    var name: String
    var value: String
    var body: some View {
        HStack(spacing: 8) {
            VStack {
                Text(name)
                    .font(.headline)
            }
            .frame(minWidth: 0, maxWidth: .infinity, alignment: .trailing)
            VStack {
                Text(value)
                    .font(.body)
            }
            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
        }
        .frame(minWidth: 0, maxWidth: .infinity)
    }
}

final class IndividualThemeHelper: ObservableObject {
    @Published var theTheme: Theme = DefaultTheme_BV()
}
