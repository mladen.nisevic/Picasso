//
//  ContentView.swift
//  Picasso
//
//  Created by Mantas Jakimavicius on 26/03/2021.
//

import SwiftUI

#if targetEnvironment(macCatalyst)
var columns: [GridItem] = Array(repeating: .init(.flexible()), count: 3)
#else
var columns: [GridItem] = Array(repeating: .init(.flexible()), count: UIDevice.isIPad ? 3 : 1)
#endif

struct ContentView: View {
    @State private var selection: String = .empty
    @State private var selectedView: ViewType = .brandColours
    @StateObject var viewModel: BaseViewModel
    
    var body: some View {
        VStack {
            Picker("Select View", selection: $selectedView) {
                ForEach(ViewType.allCases, id: \.self) { viewType in
                    Text(viewType.rawValue)
                        .tag(viewType)
                }
            }
            .pickerStyle(.segmented)
            selectedView.view(viewModel)
        }
        .edgesIgnoringSafeArea(.top)
        .padding()
        .onAppear {
            if let savedTab = UserDefaults.standard.string(forKey: Key.tabSelection),
               let tab = ViewType(rawValue: savedTab) {
                selectedView = tab
            } else {
                selectedView = .brandColours
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: BaseViewModel())
    }
}

extension UIDevice {
    static var isIPad: Bool {
        UIDevice.current.userInterfaceIdiom == .pad
    }

    static var isIPhone: Bool {
        UIDevice.current.userInterfaceIdiom == .phone
    }
}
