//
//  BrandColoursView.swift
//  Picasso
//
//  Created by Mladen Nisevic on 21/12/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI

extension String {
    static let nameContains = "name CONTAINS[cd] %@"
    
    static let searchByColour = "Search by Colour: "
    static let searchByColourAndBrand = "Search by Colour & Brand: "
    static let searchForParent = "Search for Parent: "
    static let copyHexValue = "Copy hex value: "
    static let copyThemesHexValues = "Copy theme's hex values: "
    static let definitionStrings = "Copy theme's colours' definitions: "
    static let themesGenesis = "Theme's Genesis:"
}

private extension String {
    static let colourName = "Colour Name"
    
    static let coloursHexContains = "colours.hexValue CONTAINS[cd] %@"
    static let coloursSourceContains = "colours.source CONTAINS[cd] %@"
    static let descriptiveNameContains = "colours.descriptiveName CONTAINS[cd] %@"
    
    static let searchPrompt = "Search brand colours for hex-colour: #FFFFFF or name: background, brand: BV, or set: BV=#ABCDEF BildBet=#000000..."
    
    static let noSearchResults = "No colours found 🤔"
    
    static let defaultTheme = "DefaultTheme"
    static let colourSource = "Colour Source:"
    static let brandColourSearchText = "BrandColourSearchText"
}

extension CGFloat {
    static let cornerRadius: CGFloat = 15
    static let headerHeight: CGFloat = 150
    static let rowHeight: CGFloat = 170
    static let cellPadding: CGFloat = 6
    static let noResultsSpacer: CGFloat = 100
}

private extension CGFloat {
    static let column0Width: CGFloat = 100
    static let brandBorderWidth: CGFloat = 5
}

struct ColourSearch {
    var hexColour: String?
    var brandName: String?
    var colourName: String?
    var themeColourName: ColourNameEnum?
    var themeColours: [String]?
    var misc: String?
}

enum ColourSource: String, CaseIterable {
    case theme
    case colour
    case all
}

struct BrandColoursView: View {
    
    @AppStorage(.colourSource) private var colourSource: ColourSource = .theme
    
    @State var cellWidth: CGFloat = 0
    @AppStorage(.brandColourSearchText) private var searchText: String = .empty
    @State private var isSearching = false
    @State private var colourAlphas: [ColourAlpha] = []
    @State var colourNames: [ColourName] = []
    @State private var brands: [Brand] = []
    @ObservedObject var viewModel: BaseViewModel
    
    var colourNames2: [ColourName] {
        colourSource == .all ? colourNames : colourNames.filter { $0.source == colourSource.rawValue }
    }
    
    var body: some View {
        if #available(macCatalyst 16.0, iOS 16, *) {
            NavigationStack {
                VStack {
                    HStack {
                        Text(String.colourSource)
                            .font(.headline)
                            .frame(width: 200, alignment: .trailing)
                        Picker(String.empty, selection: $colourSource) {
                            ForEach(ColourSource.allCases, id: \.self) {
                                Text($0.rawValue)
                            }
                        }
                        .pickerStyle(.segmented)
                        .frame(width: 200, alignment: .trailing)
                    }
                    HeaderView(brands: brands)
                        .frame(height: .headerHeight)
                        .cornerRadius(.cornerRadius)
                    ScrollView {
                        if !colourNames.isEmpty {
                            let columns = columns(brands.count)
                            LazyVGrid(columns: columns) {
                                ForEach(colourNames2.indices, id: \.self) { colourNameIndex in
                                    ForEach(0..<columns.count, id: \.self) { index in
                                        let colourName = colourNames2[colourNameIndex]
                                        if index == .zero {
                                            FirstColumnView(rowNo: colourNameIndex, name: colourName.name ?? .empty)
                                        } else {
                                            ColourView3(searchText: $searchText, colourA: colour(colourName: colourName, brand: brands[index - 1]), rowHeight: .rowHeight)
                                        }
                                    }
                                }
                            }
                        } else {
                            Spacer(minLength: .noResultsSpacer)
                            Text(String.noSearchResults).font(.largeTitle)
                        }
                    }
                }
            }
            .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .automatic), prompt: String.searchPrompt)
            .disableAutocorrection(true)
            .onChange(of: searchText, perform: { newValue in
                colourNames = viewModel.colourNames(for: searchText) ?? []
                brands = viewModel.brands(for: viewModel.brand(from: searchText)) ?? []
            })
            .padding()
            .onAppear {
                UserDefaults.standard.set(ViewType.brandColours.rawValue, forKey: Key.tabSelection)
                colourAlphas = viewModel.colourAlphas ?? []
                brands = viewModel.brands ?? []
                colourNames = viewModel.colourNames ?? []
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func columns(_ count: Int) -> [GridItem] {
        Array(repeating: .init(.flexible()), count: count + 1)
    }
    
    private func colour(colourName: ColourName, brand: Brand) -> ColourAlpha? {
        var filtered = colourAlphas.filter { $0.brand == brand && $0.colour?.colourName == colourName }
        if colourSource != .all {
            filtered = filtered.filter { $0.source == colourSource.rawValue }
        }
        return filtered.first
    }
}

struct HeaderView: View {
    var brands: [Brand]
    var body: some View {
        GeometryReader { geometry in
            HStack {
                CornerHeaderCellView()
                ForEach(brands, id: \.self) { brand in
                    HeaderCellView(brand: brand)
                }
            }
        }
    }
}

struct HeaderCellView: View {
    var brand: Brand
    var body: some View {
        ZStack {
            colour(for: brand, colourName: .background)
                .cornerRadius(.cornerRadius)
            VStack {
                Text(brand.name ?? .empty)
                    .font(.title)
                    .foregroundColor(colour(for: brand, colourName: .title))
            }
        }
        .overlay(
            RoundedRectangle(cornerRadius: .cornerRadius, style: .continuous)
                .stroke(colour(for: brand, colourName: .accessorySymbol), lineWidth: .brandBorderWidth)
        )
        .padding(.all, .cellPadding)
    }
    
    private func colour(for brand: Brand, colourName: ColourNameEnum) -> Color {
        var retColour = Color.clear
        if let hex = brand.theme(for: .defaultTheme)?.colour(for: colourName)?.hexValue,
            let uiColor = UIColor(hex: hex) {
            retColour = Color(uiColor: uiColor)
        }
        return retColour
    }
}

struct CornerHeaderCellView: View {
    var body: some View {
        ZStack {
            Color.clear
                .cornerRadius(.cornerRadius)
            VStack {
                Text(String.colourName)
                    .font(.title2)
                    .foregroundColor(.primary)
            }
        }
        .overlay(
            RoundedRectangle(cornerRadius: .cornerRadius, style: .continuous)
                .stroke(.clear, lineWidth: 5)
        )
        .padding(.all, .cellPadding)
    }
}

func hex(_ hexString: String?, alpha: Double?) -> String {
    guard let hexString, !hexString.isEmpty else { return .empty }
    if hexString.count == 6 {
        var alphaStr: String = .empty
        if let alpha, alpha != 1 {
            alphaStr = " \(alpha)"
        }
        return hexString + alphaStr
    }
    if hexString.count == 8 {
        let rgb = String(hexString.prefix(6))
        let alpha = String(hexString.suffix(2))
        if alpha == .defaultAlphaHex{
            return rgb
        }
        return rgb + " " + alpha
    }
    return .empty
}

struct ColourView3: View {
    @Binding var searchText: String
    let colourA: ColourAlpha?
    let rowHeight: CGFloat
    var thirdHeight: CGFloat {
        (rowHeight - 4 * .cellPadding)/3
    }
    var body: some View {
        var elementColour: Color = .clear
        if let hex = colourA?.hexValue, !hex.isEmpty, let uiColor = UIColor(hex: hex, alpha: colourA?.alphaValue ?? 1) {
            elementColour = Color(uiColor: uiColor)
        }
        return GeometryReader { metrics in
            VStack {
                if elementColour == .clear {
                    ClearColourView(rowHeight: metrics.size.height * 0.4)
                        .cornerRadius(.cornerRadius, corners: [.topLeft, .topRight])
                        .frame(height: metrics.size.height * 0.4)
                } else {
                    RoundedRectangle(cornerRadius: 1)
                        .fill(elementColour)
                        .cornerRadius(.cornerRadius, corners: [.topLeft, .topRight])
                        .frame(height: metrics.size.height * 0.4)
                }
                Text(hex(colourA?.hexValue, alpha: colourA?.alphaValue))
                    .foregroundColor(.primary)
                    .frame(height: metrics.size.height * 0.25)
                    .font(.headline)
                if let parent = colourA?.colour?.parent {
                    Text(parent)
                        .foregroundColor(.secondary)
                        .frame(height: metrics.size.height * 0.2)
                } else {
                    Rectangle()
                        .fill(.clear)
                        .frame(height: metrics.size.height * 0.2)
                }
            }
            .contextMenu(
                ContextMenu(menuItems: {
                    Button("\(colourA?.colour?.descriptiveName ?? .empty)", action: {})
                    Button(.searchByColour + "\(colourA?.hexValue ?? .empty)", action: {
                        searchText = colourA?.hexValue ?? .empty
                    })
                    Button(.searchByColourAndBrand + "\(colourA?.hexValue ?? .empty), \(colourA?.brand?.name ?? .empty)", action: {
                        searchText = String.hexPrefix + "\(colourA?.hexValue ?? .empty) \(colourA?.brand?.name ?? .empty)"
                    })
                    Button(.copyHexValue + "\(colourA?.hexValue ?? .empty) to clipboard", action: {
                        UIPasteboard.general.string = String.hexPrefix + (colourA?.hexValue ?? .empty)
                    })
                    Button("Copy to clipboard: " + "\(colourA?.name ?? .empty)", action: {
                        UIPasteboard.general.string = colourA?.name ?? .empty
                    })
                })
            )
        }
        .overlay(
            RoundedRectangle(cornerRadius: .cornerRadius)
                .stroke(.quaternary, lineWidth: 1)
        )
        .padding(.all, .cellPadding)
    }
}

struct FirstColumnView: View {
    let rowNo: Int
    let name: String
    var body: some View {
        GeometryReader { metrics in
            VStack {
                Text("\(rowNo).")
                    .font(.headline)
                    .padding()
                    .frame(maxWidth: .infinity, alignment: .center)
                    .frame(height: metrics.size.height * 0.4)
                    .background(.quaternary)
                    .cornerRadius(.cornerRadius)
                HStack {
                    Spacer()
                    Text("\(name)")
                        .font(.system(size: 13))
                        .lineLimit(nil)
                        .multilineTextAlignment(.center)
                        .frame(height: metrics.size.height * 0.4)
                    Spacer()
                }
                .background(.quaternary)
                .cornerRadius(.cornerRadius)
            }
        }
        .padding(.all, .cellPadding)
        .frame(height: .rowHeight)
        .contextMenu(
            ContextMenu(menuItems: {
                Button("Copy '\(name)' to Clipboard", action: {
                    UIPasteboard.general.string = name
                })
            })
        )
    }
}

struct ClearColourView: View {
    var rowHeight: CGFloat
    var body: some View {
        GeometryReader { geometry in
            Path { path in
                let size = geometry.size
                path.move(to: CGPoint(x: 0, y: size.height))
                path.addLine(to: CGPoint(x: size.width, y: 0))
            }
            .stroke(Color.red, style: StrokeStyle(lineWidth: 1, lineCap: .round))
        }
        .frame(height: rowHeight)
        .background(.quaternary)
    }
}

struct PalettesView2_Previews: PreviewProvider {
    static var previews: some View {
        BrandColoursView(viewModel: BaseViewModel())
    }
}
