//
//  Themes2ViewViewModel.swift
//  Picasso
//
//  Created by Mladen Nisevic on 06/01/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import SwiftUI
import CoreData

private extension String {
    static let equals = "="
    
    static let themes = "themes"
    static let themesColoursHexContains = "themes.title.colour.hexValue CONTAINS[cd] %@"
    static let descriptiveNameContains = "themes.title.colour.descriptiveName CONTAINS[cd] %@"
    static let hexValueContains = "colour.hexValue CONTAINS[cd] %@"
    static let themesTitleHexValueContains = "themes.title.colour.hexValue CONTAINS[cd] %@"
    static let themesBrandContains = "themes.brand CONTAINS %@"
    static let selfIn = "SELF IN %@"
}

// MARK: - CoreData Predicate Fun
extension Themes2View {

    func getThemesNamesPredicate(_ searchText: String) -> NSPredicate? {
        guard let searchResult = processSearchText(searchText) else { return nil }
        var predicate: NSPredicate?
        if searchResult.themeColours != nil {
            predicate = themesColoursPredicate(for: searchResult)
        } else if searchResult.themeColourName != nil {
            predicate = themeColourPredicate(for: searchResult)
        } else if let hex = searchResult.hexColour {
            predicate = NSPredicate(format: .themesColoursHexContains, hex)
        } else if let colName = searchResult.colourName {
            predicate = NSPredicate(format: .nameContains, colName)
        } else if let misc = searchResult.misc {
            predicate = anythingPredicate(misc)
        }
        return predicate
    }
    
    func getBrandPredicate(_ searchText: String) -> NSPredicate? {
        guard let tokens = getTokens(from: searchText) else { return nil }
        var predicate: NSPredicate?
        tokens.forEach { token in
            if let brandName = BrandName(rawValue: token) {
                predicate = NSPredicate(format: .nameContains, brandName.rawValue)
                return // continue
            }
        }
        return predicate
    }
    
    private func processSearchText(_ searchText: String) -> ColourSearch? {
        
        guard let tokens = getTokens(from: searchText) else { return nil }
        
        if isMultiColourSearch(tokens) {
            return processMultiColour(tokens)
        }
        var colourSearch = ColourSearch()
        tokens.forEach { token in
            if let col = ColourNameEnum(rawValue: token) {
                colourSearch.themeColourName = col
            }
            if UIColor.isValid(hexCode: token) {
                colourSearch.hexColour = token.replacingOccurrences(of: String.hexPrefix, with: String.empty)
            } else if BrandName(rawValue: token) != nil {
                colourSearch.brandName = token
            } else if colourNamesNotEmpty(Array(colourNames), token: token) {
                colourSearch.colourName = token
            } else {
                colourSearch.misc = token
            }
        }
        return colourSearch
    }
    
    private func getTokens(from inputText: String) -> [String]? {
        guard !inputText.isEmpty else { return nil }
        return inputText.components(separatedBy: String.space)
    }
    
    private func isMultiColourSearch(_ tokens: [String]) -> Bool {
        let matching = tokens.filter { $0.contains(String.equals) }
        return !matching.isEmpty
    }
    
    private func processMultiColour(_ tokens: [String]) -> ColourSearch {
        var tokens = tokens
        var colourSearch = ColourSearch()
        if let themeColourName = ColourNameEnum(rawValue: tokens[0]) {
            colourSearch.themeColourName = themeColourName
        }
        tokens = tokens.filter { !$0.isEmpty }
        if tokens.count > 1 {
            colourSearch.themeColours = Array(tokens[1...])
        }
        return colourSearch
    }
    
    private func themeColourPredicate(for colourSearch: ColourSearch) -> NSPredicate? {
        guard let hex = colourSearch.hexColour, let themeColour = colourSearch.themeColourName else { return nil }
        let predicateString = .themes + ".\(themeColour.rawValue)." + .hexValueContains
        return NSPredicate(format: predicateString, hex)
    }
    
    private func themesColoursPredicate(for colourSearch: ColourSearch) -> NSPredicate? {
        
        guard let searchColours = colourSearch.themeColours, let themeColour = colourSearch.themeColourName else {
            print("\(Date()):\(Self.self):\(#function):\(#line): unable to form predicate from \(colourSearch)")
            return nil
        }

        var candidatesNames: [ThemeName2] = []
        
        for colour in searchColours {
            
            let tokens = colour.components(separatedBy: String.equals)
            guard tokens.count == 2 else { continue }
            
            let brandName = tokens[0]
            let hexValue = tokens[1].noHexPrefix
            
            let brand = brands.first(where: { $0.name == brandName } )
            guard brand != nil else {
                print("\(Date()):\(Self.self):\(#function):\(#line): can't find brand for \(brandName)")
                return nil
            }
            let themes2 = themes.filter { theme2 in
                (theme2.value(forKey: themeColour.rawValue) as? Colour)?.hexValue == hexValue && theme2.brand == brand
            }
            let nameMap = themes2.compactMap { $0.themeName }
            candidatesNames.append(contentsOf: nameMap)
        }
        
        let groupByCategory = Dictionary(grouping: candidatesNames) { $0.name }
        var themeNames = [ThemeName2]()
        for key in groupByCategory.keys {
            let value = groupByCategory[key]
            if let value = value, value.count == searchColours.count {
                themeNames.append(value[0])
            }
        }
        
        return NSPredicate(format: .selfIn, themeNames)
    }

    private func colourNamesNotEmpty(_ allColourNames: [ColourName]?, token: String) -> Bool {
        let cols = allColourNames?.filter { $0.name == token }
        if let cols = cols, !cols.isEmpty {
            return true
        }
        return false
    }
    
    private func anythingPredicate(_ text: String) -> NSPredicate {
        var predicates = [NSPredicate]()
        predicates.append(NSPredicate(format: .descriptiveNameContains, text))
        predicates.append(NSPredicate(format: .nameContains, text))
        predicates.append(NSPredicate(format: .themesTitleHexValueContains, text))
        return NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
    }
}

private extension String {
    var noHexPrefix: String {
        if self.hasPrefix(String.hexPrefix) {
            return String(self.dropFirst())
        }
        return self
    }
}
