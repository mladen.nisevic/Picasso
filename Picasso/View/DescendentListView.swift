//
//  ChildrenThemesView.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 17/06/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI

private extension String {
    static let descendentsOf = "All descendents of"
}

struct DescendentListView: View {
    @EnvironmentObject var helper: IndividualThemeHelper
    var matchingThemes: [NSObject]
    @State private var showEditView = false
    @State private var selectedTheme: Theme = DefaultTheme_BV()
    var body: some View {
        ZStack {
            Color(uiColor: .tertiarySystemBackground)
            VStack(alignment: .leading) {
                Text(String.descendentsOf + " \(helper.theTheme.justName) (\(matchingThemes.count))")
                    .font(.headline)
                    .paddedAll()
                ScrollView {
                    LazyVGrid(columns: columns) {
                        ForEach(matchingThemes, id: \.self) { theme in
                            if let theme = theme as? Theme {
                                DescendentView(theme: theme)
                                    .contextMenu(
                                        ContextMenu(menuItems: {
                                            Button("Show \(theme.nameNoBrand)", action: {
                                                self.showEditView = true
                                                self.selectedTheme = theme
                                            })
                                            Button("Copy \(theme.nameNoBrand) to Clipboard", action: {
                                                UIPasteboard.general.string = theme.nameNoBrand
                                            })
                                        })
                                    )
                            }
                        }
                    }
                }
                .listStyle(PlainListStyle())
                NavigationLink(destination: IndividualThemeView().onAppear(perform: {
                    helper.theTheme = self.selectedTheme
                }), isActive: $showEditView) {
                    EmptyView()
                }
            }
        }
    }
}

struct DescendentView: View {
    var theme: Theme
    var body: some View {
        ZStack {
            Color(uiColor: .secondarySystemBackground)
            VStack {
                Text("\(theme.justName)")
                    .foregroundColor(Color.blue)
            }
        }
    }
}

struct ChildrenThemesView_Previews: PreviewProvider {
    static var previews: some View {
        DescendentListView(matchingThemes: [])
    }
}
