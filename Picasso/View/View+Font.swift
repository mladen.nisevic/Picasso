//
//  View+Font.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 16/01/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI

enum AppFont: String {
    case latoLight = "LatoLatin-Light"
    case latoRegular = "LatoLatin-Regular"
    case latoBold = "LatoLatin-Bold"
}

struct DefaultSize {
    static let font: CGFloat = 16
}

extension View {
    func latoLight(_ size: CGFloat = DefaultSize.font) -> some View { modifier(Light(size: size)) }
    func latoRegular(_ size: CGFloat = DefaultSize.font) -> some View { modifier(Regular(size: size)) }
    func latoBold(_ size: CGFloat = DefaultSize.font) -> some View { modifier(Bold(size: size)) }
}

struct Bold: ViewModifier {
    var size: CGFloat
    func body(content: Content) -> some View {
        content
            .font(Font.custom(AppFont.latoBold.rawValue, size: size))
    }
}

struct Regular: ViewModifier {
    var size: CGFloat
    func body(content: Content) -> some View {
        content
            .font(Font.custom(AppFont.latoRegular.rawValue, size: size))
    }
}

struct Light: ViewModifier {
    var size: CGFloat
    func body(content: Content) -> some View {
        content
            .font(Font.custom(AppFont.latoLight.rawValue, size: size))
    }
}
