//
//  ViewConstants.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 27/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import SwiftUI

struct Key {
    static let lastSearch = "LastSearch"
    static let tabSelection = "TabSelection"
    static let isExact = "IsExact"
    static let selectedBrand = "SelectedBrand"
    static let selectedBrandName = "SelectedBrandName"
    static let noMatches = "NoMatches"
}

enum ViewType: String, CaseIterable {
    case brandColours = "Brand Colours"
    case themes2 = "Themes2"
    case themes = "Themes"
    case appearanceAlias = "Appearance Alias"
    case appearanceColor = "Appearance"
    case custom = "Custom"
    
    func view(_ viewModel: BaseViewModel) -> AnyView {
        switch self {
        case .brandColours: return AnyView(BrandColoursView(viewModel: viewModel))
        case .themes2: return AnyView(Themes2View())
        case .themes: return AnyView(ThemesView())
        case .appearanceAlias: return AnyView(AppearanceAliasView())
        case .appearanceColor: return AnyView(AppearanceView())
        case .custom: return AnyView(CustomColoursView())
        }
    }
    
    var imageName: String {
        switch self {
        case .brandColours: return "paintpalette"
        case .themes: return "paintbrush"
        case .themes2: return "square.stack"
        case .custom: return "pencil"
        case .appearanceColor: return "photo"
        case .appearanceAlias: return "photo.on.rectangle"
        }
    }
}

struct ButtonTitle {
    static let exact = "Exact Match"
}
