//
//  Constants.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 15/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import Foundation

enum ColourNameEnum: String, CaseIterable, Identifiable {
    var id: Self { self }
    case background, title, subtitle, accessorySymbol, underline, cardBackground, border, body
}

struct RegexPattern {
    static let appearanceColour = "(.*Appearance.color.*)"
    static let classes = "(private )?class (.*?): "
    static let coloursVar = "static var (.*?): UIColor"
    static let coloursLet = "static let (.*?): UIColor"
    static let commentOutLine = "// TODO: Replace with Themes: $1" // for the replacement as $1 refers to the match made in the first part of the regex
    static let oneLineComment = "//.*$"
    static let extensionUIColor = #"extension UIColor \{([^}]*)\}"#
    static let extensionUIColorAny = #"(private )*extension UIColor \{([^}]*)\}"#
    static let multiEmptyLines = #"(^\n){2,}"#
    static let outsideBrackets = "\\((.*)\\)"
    static let outsideBracketsPart2 = "$1"
    static let protocols = "protocol (.*?) "
    static let spacesNewLine = #"^( +)\n"#

    static func exactName(_ name: String) -> String {
        "\\b\(name)\\b"
    }
}

public enum ExportFilePath: String {
    
    case generatedClasses = "GeneratedClasses"
    case colourExtension = "UIColor+Brands"
    case classes = "BVClasses"
    case palettes = "Palettes"
    case colourNames = "ColourNames"
    case themes = "Themes"
    case brandColours = "BrandColours"

    var directoryPath: String { "Picasso/ParsedColors/" }
    
    func path(_ brand: String? = nil) -> String {
        var suffix: String = .dotSwift
        let brandString = brand ?? String.empty
        switch self {
        case .palettes, .colourNames, .brandColours, .themes: suffix = .dotJson
        default: break
        }
        return directoryPath + self.rawValue + brandString + suffix
    }
}
