//
//  BrandStuff.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 15/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import Foundation

struct BrandsStuff {
    var content: [BrandContent]
    var colourNames: [String] {
        var all = [String]()
        content.forEach { all += $0.colourNames }
        return Array(Set(all)).sorted()
    }
}

struct BrandContent {
    var brand: BrandName
    var content: String
    var colours: [String: String]
    var colourNames: [String]
}

struct InputTheme: Codable, Hashable {
    
    var name: String
    var parentName: String
    var colours: [InputColour]
    static var zero: InputTheme {
        InputTheme(name: .empty, parentName: .empty, colours: [])
    }
}

struct InputColour: Codable, Hashable {
    var name: String
    var rawInput: String
    var hexValue: String
    static var zero: InputColour {
        InputColour(name: .empty, rawInput: .empty, hexValue: .empty)
    }
}

extension Array where Element == InputColour {
    func first(for name: ColourNameEnum) -> InputColour? {
        first(where: { $0.name == name.rawValue })
    }
}
