//
//  ThemesExtractor.swift
//  Picasso
//
//  Created by Mladen Nisevic on 03/01/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import Foundation

struct InputElement {
    var classDefinitionLine: String
    var colourLines: [String]
    static var zero: InputElement {
        InputElement(classDefinitionLine: .empty, colourLines: [])
    }
}

private extension String {
    static let classWord = "class"
    static let colon = ":"
    static let finalClass = "final class"
    static let nsObject = "NSObject"
    static let openCurly = "{"
    static let overrideFunc = "override func"
    static let overridePublicFunc = "override public func"
    static let parenthesis = "()"
    static let publicClass = "public class"
    static let publicFunc = "public func"
    static let publicOverrideFunc = "public override func"
    static let theme = "Theme"
}

private let themesUnusuallyNamed: [String] = [
    "BetBuilderTabSelector",
    "BetslipActiveSwitch",
    "BetslipInactiveSwitch",
    "BetslipSuspendedSwitch",
    "ScoreboardLink",
    "ScoreboardSelectedLink"
]

private let funcAttributesList: [String] = [.overridePublicFunc, .publicOverrideFunc, .publicFunc, .overrideFunc]
private let classAttributesList: [String] = [.classWord, .finalClass, .publicClass]

final class ThemesExtractor {
    
    enum State {
        case start, open, inside, closed
    }
    
    let fileManager = FileManager.default
    var fileName: String?
    var lines: [String]?
    var inputElements: [InputElement] = []
    
    func readFiles(for brand: BrandName) -> [String] {
        var lines = [String]()
        let pathForThemes = path(for: brand)
        do {
            let items = try fileManager.contentsOfDirectory(atPath: pathForThemes)
            for item in items {
                guard item.hasSuffix(String.dotSwift) else { continue }
                let ret = readFile(pathForThemes + "/" + item)
                lines.append(contentsOf: ret)
            }
        } catch {
            print("\(Date()):\(Self.self):\(#function):\(#line): error \(error.localizedDescription)")
        }
        return lines
    }
    
    func readFile(_ filePath: String) -> [String] {
        var lines = [String]()
        if fileManager.fileExists(atPath: filePath) {
            let parsedFileContent = try! String(contentsOfFile: filePath)
            lines = parsedFileContent.components(separatedBy: .newlines)
        }
        return lines
    }
    
    private func path(for brand: BrandName) -> String {
        "Resources/\(brand.rawValue)/Common/Style"
    }
    
    /// process lines and save raw data into [InputElement]
    func firstPass(_ lines: [String]) -> [InputElement] {
        var state: State = .start
        var inputElement = InputElement.zero
        var inputElements = [InputElement]()
        for line in lines {
            
            if state == .open {
                if isColourFuncLine(line) {
                    inputElement.colourLines.append(line)
                    continue
                }
            }
            
            if isClassDefLine(line) {
                inputElement.classDefinitionLine = line
                state = .open
            }
            
            if state == .open || state == .inside {
                if line.contains(String.closeCurly) {
                    state = .closed
                    inputElements.append(inputElement)
                    inputElement = InputElement.zero
                }
            }
        }
        return inputElements
    }
    
    private func isClassDefLine(_ line: String) -> Bool {
        for funcLine in classAttributesList {
            if line.contains(funcLine) {
                return true
            }
        }
        return false
    }
    
    private func isColourFuncLine(_ line: String) -> Bool {
        for funcLine in funcAttributesList {
            if line.contains(funcLine) {
                return true
            }
        }
        return false
    }

    /// process raw input data
    /// parse class name, ancestor
    /// and array of colours from input
    func process(_ inputElements: [InputElement]) -> [InputTheme] {
        var themes = [InputTheme]()
        var theme = InputTheme.zero
        for element in inputElements {
            let (name, parentName) = parseName(element.classDefinitionLine)
            theme.name = name
            theme.parentName = parentName
            theme.colours = parse(colourLines: element.colourLines)
            
            themes.append(theme)
            theme = InputTheme.zero
        }
        return themes
    }
    
    func parseName(_ element: String) -> (String, String) {

        enum State {
            case start, afterName, afterParent, end
        }
        var state: State = .start
        let chunks = element.components(separatedBy: .whitespaces)
        
        var (name, parent) = (String.empty, String.empty)

        for chunk in chunks {

            let chunk = chunk.trimmingCharacters(in: .punctuationCharacters)
            if chunk.contains(String.openCurly) {
                state = .end
            }
            if state == .afterParent {
                break
            }
            if chunk.contains(String.theme) || themesUnusuallyNamed.contains(chunk) {
                if state == .afterName {
                    if chunk != String.theme && chunk != String.nsObject {
                        parent = chunk
                        state = .afterParent
                    }
                } else {
                    name = chunk.replacingOccurrences(of: String.colon, with: String.empty)
                    state = .afterName
                }
            }
        }
        return (name, parent)
    }
    
    private func parse(colourLines: [String]) -> [InputColour] {
        var colours = [InputColour]()
        var afterName = false
        var inDefinition = false
        var inAppearanceAlphaDefinition = false
        var appearanceAlphaPart1: String = ""
        var colour = InputColour.zero
        for line in colourLines {
            let chunks = line.components(separatedBy: .whitespaces)
            for chunk in chunks {
                if chunk.contains(String.closeCurly) {
                    afterName = false
                    inDefinition = false
                    break
                }
                if !afterName {
                    if chunk.contains(String.parenthesis) {
                        colour.name = chunk.replacingOccurrences(of: String.parenthesis, with: String.empty)
                        afterName = true
                    }
                } else {
                    if inDefinition {
                        if inAppearanceAlphaDefinition {
                            colour.rawInput = appearanceAlphaPart1 + chunk.trimmingCharacters(in: .punctuationCharacters)
                            colours.append(colour)
                            colour = InputColour.zero
                            inDefinition = false
                            inAppearanceAlphaDefinition = false
                            continue
                        }
                        if chunk.hasPrefix("Appearance.color.") && chunk.contains("alpha") {
                            inAppearanceAlphaDefinition = true
                            appearanceAlphaPart1 = chunk
                            continue
                        }
                        colour.rawInput = chunk
                        colours.append(colour)
                        colour = InputColour.zero
                        inDefinition = false
                    } else {
                        if chunk.contains(String.openCurly) {
                            inDefinition = true
                            continue
                        }
                    }
                }
            }
        }
        return colours
    }
    
    func run() {
        BrandName.allCases.forEach { brandName in
            lines = readFiles(for: brandName)
            inputElements = firstPass(lines ?? [])
            let themesData = process(inputElements)
            let json = PaletteHelper.shared.createJSON(themesData)
            FileWriter.write(json, toFile: .themes, brand: brandName)
        }
    }
}
