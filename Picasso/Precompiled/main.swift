#!/usr/bin/env xcrun --sdk macosx swift

import Foundation

class AppearanceParser {

    var paletteHelper = PaletteHelper.shared
    
    func run() {
        writeFunctions()
        ThemesExtractor().run()
    }

    func writeFunctions() {

        var content: String = .importUIKit + .newLine
        var classesContent: String = .importFoundation + .newLine

        let fileManager = FileManager.default
        
        var brandsContents = BrandsStuff(content: [])
        var arrayOfColorNames = [String]()

        BrandName.allCases.forEach { brand in
            let target = brand.rawValue

            classesContent += .newLine + "class \(brand.targetThemesClassName)Themes {"
            var arrayOfColors = [String]()
            var resultsClasses = [String]()
            var resultsProtocols = [String]()

            var classesForUI = [String]()

            content += "//---------------------------- \(target) ---------------------------------- " + .newLine
            var fileContent = String.empty

            TargetFile.allCases.forEach { file in

                let pathForThemes = "Resources/\(target)/Common/Style/\(file).swift"

                if fileManager.fileExists(atPath: pathForThemes) {
                    var parsedFileContent = try! String(contentsOfFile: pathForThemes)

                    parsedFileContent = removeImports(parsedFileContent)
                    fileContent += parsedFileContent

                    arrayOfColors += extractColours(parsedFileContent)
                    arrayOfColorNames.append(contentsOf: arrayOfColors)
                    resultsClasses += extractClasses(parsedFileContent)
                    resultsProtocols += extractProtocols(parsedFileContent)
                }
            }
            let uniqueArrayOfColors = Array(Set(arrayOfColors))
            var uniqueResultsClasses = Array(Set(resultsClasses))
            uniqueResultsClasses.sort()

            var uniqueProtocols = Array(Set(resultsProtocols))
            uniqueProtocols.sort()

            let fileContentArray = fileContent.components(separatedBy: "\n")

            // ----------Replace with target names-----------//
            uniqueArrayOfColors.forEach { color in
                fileContent = fileContent.replacingOccurrences(of: "var \\b\(color)\\b", with: "var \(color)_\(target)", options: .regularExpression)
                fileContent = fileContent.replacingOccurrences(of: "let \\b\(color)\\b", with: "let \(color)_\(target)", options: .regularExpression)
            }

            uniqueArrayOfColors.forEach { color in
                fileContent = fileContent.replacingOccurrences(of: "\\.?\\b\(color)\\b(?!\\()", with: ".\(color)_\(target)", options: .regularExpression)

                // fix .card_Parimatched, should stay .carded
                fileContent = fileContent.replacingOccurrences(of: ".\(color)_\(target)" + .ed, with: ".\(color)" + .ed)

                // should replace .background but not .background(), so restore it here
                fileContent = fileContent.replacingOccurrences(of: ".\(color)_\(target)()", with: ".\(color)()")

                // background() not .background()
                fileContent = fileContent.replacingOccurrences(of: "func.\(color)()", with: "func \(color)()")

                // fix .selected_BVInactive:
                fileContent = fileContent.replacingOccurrences(of: ".\(color)_\(target)Inactive:", with: ".\(color)Inactive:")

                fileContent = fileContent.replacingOccurrences(of: "case .\(color)_\(target)", with: "case .\(color)")
                fileContent = fileContent.replacingOccurrences(of: ", .\(color)_\(target):", with: ", .\(color):")
                fileContent = fileContent.replacingOccurrences(of: "super.\(color)_\(target)", with: "super.\(color)")
                let BetslipGermanTaxInfo = ".*BetslipGermanTaxInfo.*"
                fileContent = fileContent.replacingOccurrences(of: BetslipGermanTaxInfo, with: "", options: .regularExpression)
            }

            var brandColours = [String: String]()
            uniqueArrayOfColors.forEach { color in
                let key = "static var \(color): UIColor = "
                let matches = fileContentArray.filter { $0.contains(key)}
                brandColours[color] = matches.first?.replacingOccurrences(of: "^.* = ", with: "", options: .regularExpression) ?? ""
            }

            uniqueResultsClasses.forEach { className in
                fileContent = fileContent.replacingOccurrences(of: RegexPattern.exactName(className), with: "\(className)_\(target)", options: .regularExpression)
                classesForUI.append("\(className)_\(target)")
            }

            uniqueProtocols.forEach { protocolName in
                fileContent = fileContent.replacingOccurrences(of: RegexPattern.exactName(protocolName), with: "\(protocolName)_\(target)", options: .regularExpression)
            }

            fileContent = fileContent.replacingOccurrences(of: "_\(target)_\(target)", with: "_\(target)")
            fileContent = fileContent.replacingOccurrences(of: "_\(target)Theme_\(target)", with: "Theme_\(target)")

            content += fileContent
            brandsContents.content.append(BrandContent(brand: brand, content: .importUIKit + .newLine + fileContent, colours: brandColours, colourNames: arrayOfColors))

            // ----------Create array of theme objects-----------//
            classesContent += .newLine + .tab + "let \(target)Clases = [\n\t\t\(classesForUI.joined(separator: "(),\n\t\t"))()\n\t]"
            classesContent += .newLine + .closeCurly + .newLine
        }

        content = removeAppearanceColors(content)
        
        let ret = filterUIColorsExtension(content: &content)
        FileWriter.write(ret.content, toFile: .colourExtension)

        FileWriter.write(classesContent, toFile: .classes)
        let themesExtractor = ThemesColourExtractor(brandsContents, coloursRaw: ret.colours)
        themesExtractor.processThemesStuff()

        var brandContentsFinal = brandsContents.content
        brandContentsFinal = removeBrandsAppearances(brandContentsFinal)
        brandContentsFinal = removeUIColorsExtensions(brandContentsFinal)
        brandContentsFinal = removeEmptyLines(brandContentsFinal)
        writeThemesFiles(brandContentsFinal)
    }
}

extension AppearanceParser {
    
    func writeThemesFiles(_ brandsContents: [BrandContent]) {
        for brandContent in brandsContents {
            FileWriter.write(brandContent.content, toFile: .generatedClasses, brand: brandContent.brand)
        }
    }
    
    func removeBrandsAppearances(_ brandsContents: [BrandContent]) -> [BrandContent] {
        var brandsContentsOut = [BrandContent]()
        for brandContent in brandsContents {
            var brandContentOut = brandContent
            brandContentOut.content = removeAppearanceColors(brandContent.content)
            brandsContentsOut.append(brandContentOut)
        }
        return brandsContentsOut
    }
    
    func removeUIColorsExtensions(_ brandsContents: [BrandContent]) -> [BrandContent] {
        var brandsContentsOut = [BrandContent]()
        for brandContent in brandsContents {
            var brandContentOut = brandContent
            brandContentOut.content = removeColourExtensions(from: brandContent.content)
            brandsContentsOut.append(brandContentOut)
        }
        return brandsContentsOut
    }
    
    func removeEmptyLines(_ brandsContents: [BrandContent]) -> [BrandContent] {
        var brandsContentsOut = [BrandContent]()
        for brandContent in brandsContents {
            var brandContentOut = brandContent
            brandContentOut.content = removeEmptyLines(from: brandContent.content)
            brandsContentsOut.append(brandContentOut)
        }
        return brandsContentsOut
    }

    func removeAppearanceColors(_ content: String) -> String {
        // Appearance.color should not be used here, only the new Themes framework
        let nsRange = NSRange(location: 0, length: content.utf16.count)
        let range = Range(nsRange, in: content)
        let content = content.replacingOccurrences(of: RegexPattern.appearanceColour, with: RegexPattern.commentOutLine, options: .regularExpression, range: range)
        return content
    }
    
    func removeEmptyLines(from content: String) -> String {
        
        var regexColors = try? NSRegularExpression(pattern: RegexPattern.spacesNewLine, options: [.anchorsMatchLines])
        var range = NSRange(location: 0, length: content.utf16.count)
        var content = regexColors?.stringByReplacingMatches(in: content, options: [], range: range, withTemplate: String.newLine) ?? .empty

        regexColors = try? NSRegularExpression(pattern: RegexPattern.multiEmptyLines, options: [.anchorsMatchLines])
        range = NSRange(location: 0, length: content.utf16.count)
        content = regexColors?.stringByReplacingMatches(in: content, options: [], range: range, withTemplate: String.newLine) ?? .empty
        
        return content
    }

    func removeImports(_ parsedFileContent: String) -> String {
        var parsedFileContent = parsedFileContent
        let toRemove = parsedFileContent.components(separatedBy: String.importUIFactory)
        if toRemove.count == 2 {
            parsedFileContent = parsedFileContent.replacingOccurrences(of: toRemove[0], with: String.empty)
        }
        if parsedFileContent.contains(String.importUIKit) {
            parsedFileContent = parsedFileContent.remove(String.importUIKit)
        }
        if parsedFileContent.contains(String.importUIFactory) { // it doesn't!
            parsedFileContent = parsedFileContent.remove(String.importUIFactory)
        }
        return parsedFileContent
    }

    // ----------Colors-----------//
    func extractColours(_ parsedFileContent: String) -> [String] {

        var returnColourArray = [String]()

        for regexPattern in [RegexPattern.coloursVar, RegexPattern.coloursLet] {

            var arrayOfColors = [String]()
            let regexColors = try! NSRegularExpression(pattern: regexPattern, options: [])

            regexColors.enumerateMatches(in: parsedFileContent, options: [], range: NSRange(location: 0, length: parsedFileContent.utf16.count)) { result, _, _ in
                if let r = result?.range(at: 1), let range = Range(r, in: parsedFileContent) {
                    arrayOfColors.append(String(parsedFileContent[range]))
                }
            }
            returnColourArray.append(contentsOf: arrayOfColors)
        }

        return returnColourArray
    }

    // ----------Classes-----------//
    func extractClasses(_ parsedFileContent: String) -> [String] {

        var resultsClasses = [String]()

        // public, internal (with or without @objc) yes, but private no
        let regex = try! NSRegularExpression(pattern: RegexPattern.classes, options: [])
        regex.enumerateMatches(in: parsedFileContent, options: [], range: NSRange(location: 0, length: parsedFileContent.utf16.count)) { result, _, _ in

            if let r = result?.range(at: 1), Range(r, in: parsedFileContent) != nil {
                // private - not needed, but the ranges check above is needed. not sure why
            } else if let r = result?.range(at: 2), let range = Range(r, in: parsedFileContent) {
                resultsClasses.append(String(parsedFileContent[range]))
            }
        }

        return resultsClasses
    }

    // ----------Protocols-----------//
    func extractProtocols(_ parsedFileContent: String) -> [String] {
        var resultsProtocols = [String]()
        let regexProtocol = try! NSRegularExpression(pattern: RegexPattern.protocols, options: [])
        regexProtocol.enumerateMatches(in: parsedFileContent, options: [], range: NSRange(location: 0, length: parsedFileContent.utf16.count)) { result, _, _ in

            if let r = result?.range(at: 1), let range = Range(r, in: parsedFileContent) {
                resultsProtocols.append(String(parsedFileContent[range]))
            } else if let r = result?.range(at: 2), let range = Range(r, in: parsedFileContent) {
                resultsProtocols.append(String(parsedFileContent[range]))
            }
        }
        return resultsProtocols
    }

    ///    empty the multiple brand colour definitions and
    ///    save all the brand colours into one unified UIColor extension
    /// - Parameter content: the content that is being processed
    /// - Returns: content for the colours extension
    func filterUIColorsExtension(content: inout String) -> (content: String, colours: [String]) {
        var colors = [String]()
        let regexColors = try? NSRegularExpression(pattern: RegexPattern.extensionUIColor, options: [])

        regexColors?.enumerateMatches(in: content, options: [], range: NSRange(location: 0, length: content.utf16.count)) { result, _, _ in
            if let r = result?.range(at: 1) {
                if let range = Range(r, in: content) {
                    colors.append(String(content[range]))
                }
            }
        }

        let colourSet = Set(colors.reduce([String]()) {
            $0 + $1.components(separatedBy: String.newLine)
        })

        let colorsArray = Array(colourSet)

        var trimmedArray = [String]()
        colorsArray.forEach { colour in
            trimmedArray.append(colour.trimmingCharacters(in: .whitespacesAndNewlines))
        }

        trimmedArray.sort()

        var colourContent = .importUIKit + .newLines() + "extension UIColor {" + .newLine
        _ = trimmedArray.map {
            let val = $0.trimmingCharacters(in: .whitespacesAndNewlines)
            guard !val.isEmpty, !val.hasPrefix(String.comment) else { return } // i.e. continue
            content = content.replacingOccurrences(of: val, with: String.empty)
            colourContent += .tab + "\(val)" + .newLine
        }
        colourContent += .closeCurly
        return (colourContent, trimmedArray)
    }
    
    /// Remove any extension UIColor from the GeneratedClasses content
    /// They go into another file
    /// - Parameter content: content including themes and UIColor extensions
    /// - Returns: content with themes but without UIColor extensions
    func removeColourExtensions(from content: String) -> String {
        let regexColors = try? NSRegularExpression(pattern: RegexPattern.extensionUIColorAny, options: [])
        let range = NSRange(location: 0, length: content.utf16.count)
        let ret = regexColors?.stringByReplacingMatches(in: content, options: [], range: range, withTemplate: String.empty) ?? .empty
        return ret
    }
}

extension String {
    
    static let reference1 = "$1"
    
    func split(_ string: String) -> [String] {
        components(separatedBy: string)
    }
    
    func remove(_ string: String) -> String {
        replacingOccurrences(of: string, with: String.empty)
    }

    func apply(regex: String) -> String {
        replacingOccurrences(of: regex, with: String.reference1, options: .regularExpression).trimmingCharacters(in: .whitespacesAndNewlines)
    }
}

// MARK: - Main Harness
AppearanceParser().run()
