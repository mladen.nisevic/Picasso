// 
//
//  ThemesColourExtractor.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 19/12/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//
import Foundation
/**
    Take the Themes-related parsed colours, palettes and pallete keys for each brand
    write them out in JSON files for later import by the main app
*/

final class ThemesColourExtractor {
//  used extensively - need to be defined
    enum BaseColour: String, CaseIterable {
        case clear,
             black,
             white
        static var textList: [String] {
            allCases.map { $0.rawValue }
        }
    }

    let brandsStuff: BrandsStuff
    let paletteHelper: PaletteHelper
    let coloursRaw: [String]

    init(_ brandsStuff: BrandsStuff, coloursRaw: [String]) {
        self.brandsStuff = brandsStuff
        self.paletteHelper = .shared
        self.coloursRaw = coloursRaw
    }

    func processThemesStuff() {
        writeAllColourKeys()
        writeBrandColours()
        generatePalette_old(coloursRaw)
    }

    private func writeAllColourKeys() {
        let colourNames = brandsStuff.colourNames + BaseColour.textList
        let json = paletteHelper.createJSON(colourNames)
        FileWriter.write(json, toFile: .colourNames)
    }

    private func writeBrandColours() {
        var jsonString: String = .squareOpen
        var first = true
        brandsStuff.content.forEach { content in
            if first {
               first = false
            } else {
                jsonString += .comma
            }
            let json = coloursJSON(content, allColourNames: brandsStuff.colourNames)
            jsonString += json
            jsonString += .newLine
        }
        jsonString += .squareClose
        FileWriter.write(jsonString, toFile: .brandColours)
    }

    private func coloursJSON(_ content: BrandContent, allColourNames: [String]) -> String {
        var brandColoursArray = [BrandColour]()
        let colours = cleanColours(content.colours)
        allColourNames.forEach { colourName in
            brandColoursArray.append(BrandColour(name: colourName, colour: colours[colourName] ?? ""))
        }
        let brandColours = BrandColours(brand: content.brand.rawValue, brandColours: brandColoursArray)
        let json = paletteHelper.createJSON(brandColours)
        return json
    }

    private func cleanColours(_ colours: [String: String]) -> [String: String] {
        var dict2 = colours
        for key in colours.keys {
            dict2[key] = clean(colours[key] ?? "")
        }
        return dict2
    }
    
    private func clean(_ colourString: String) -> String {
        guard !colourString.isEmpty else { return colourString }
        let returnString = colourString.replacingOccurrences(of: RegexPattern.oneLineComment, with: String.empty, options: .regularExpression)
        return returnString
    }
}

private extension String {
    static let fromHex = ".fromHex(0x"
}

// MARK: - Palette
extension ThemesColourExtractor {
    
    func generatePalette_old(_ colours: [String]) {
        for brand in BrandName.allCases {
            var elements = [PaletteElement]()
            let brandColourLines = colours.filter { $0.contains(brand.rawValue) }
            var linesWithSecondaryColours = [String]()
            for colourLine in brandColourLines {
                if let pal = PaletteElement(colourLine, brand: brand) {
                    elements.append(pal)
                } else {
                    linesWithSecondaryColours.append(colourLine)
                }
            }
            for secondaryColourLine in linesWithSecondaryColours {
                if let pal = PaletteElement(secondaryColourLine, brand: brand, elements: elements) {
                    elements.append(pal)
                } else {
                    print("Error: really can't get this colour: \(secondaryColourLine)")
                }
            }
            let brandPalette = BrandPalette(id: brand, palette: elements)
            paletteHelper.palettes[brand] = brandPalette
        }
        let json = paletteHelper.paletteJSON()
        FileWriter.write(json, toFile: .palettes)
    }
}
