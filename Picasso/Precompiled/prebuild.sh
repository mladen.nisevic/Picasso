#!/bin/zsh

#  prebuild.sh
#  BetVictor
#
#  Created by Francisco Trujillo on 28/06/2016.
#  Copyright © 2016 BetVictor Ltd. All rights reserved.

# uncomment bellow line to push debug details to a local log. Interesting to Troubleshoot ruby.
# exec > ${PROJECT_DIR}/prebuild.log 2>&1

# Note: When running from the command line uncomment the next two lines
 # PROJECT_DIR=.
 # SRCROOT=.

MODULE_NAME=AppearanceParser

log() {
    echo "$*"
}

die() {
    log "$*"
    exit
}

exec > ${PROJECT_DIR}/prebuild.log 2>&1
cd ${SRCROOT}

echo "$0 Project Dir: $PROJECT_DIR, src dir: $SRCROOT"

SWIFT_FILES=(
    Picasso/Model/BrandName
    Picasso/Model/Palette
    Picasso/Model/TargetFile
    Picasso/Precompiled/BrandStuff
    Picasso/Precompiled/Bundle+JSON
    Picasso/Precompiled/Constants
    Picasso/Precompiled/FileWriter
    Picasso/Precompiled/ThemesColourExtractor
    Picasso/Precompiled/ThemesExtractor
    Picasso/Precompiled/main
    Picasso/Utilities/DebugUtils
    Picasso/Utilities/String+Quotes
    Picasso/ViewModel/PaletteHelper
)

compilation_files() {
    for file in $SWIFT_FILES; do
        echo "${file}.swift"
    done
}

swiftc `compilation_files` -o $MODULE_NAME
[ $? -eq 0 ] || die "Compilation error"

echo "Executing: $MODULE_NAME"

./$MODULE_NAME

rm $MODULE_NAME
