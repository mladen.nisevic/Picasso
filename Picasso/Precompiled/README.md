#  Appearance Parser 


AppearanceParser is a standalone command line swift script.

It parses contents of Themes swift source code files and generates files with colour and theme structures that are used by the Picasso to facilitate display them.

Picasso/Precompiled/main.swift == AppearanceParser

(Note: main.swift should not be confused with the main of the Picasso app, they are two distinct apps)

### Generated Files

Four files are generated in the Picasso/ParsedColors directory

1. BVClasses.swift - contains one list of all the Theme classes for all brands
2. UIColor+Brands.swift - contains all the defined colours for all the brands
3. GeneratedClasses.swift - contains all Theme classes renamed corresponding to their target/brand
    * DefaultTheme.swift -> DefaultTheme_BV.swift, DefaultTheme_Parimatch.swift etc.
4. Palettes.json - Brands' colour palettes

These are later used by the Picasso app.

### Previous Name
The previous name of this file was SaveManager.swift. However, in order to be able to include other files it had to be renamed to main.swift. (a swiftc requirement)

Including other files was important so that they can be shared with the actual Picasso app without duplication.


## prebuild.sh

prebuild.sh creates an executable called AppearanceParser, which consists of the AppearanceParser class and some other swift classes, ~shared with the picasso app. (see the script for the list)

It then runs that executable.

Running prebuild.sh from command line works but it's necessary to uncomment two lines that define PROJECT_DIR and SRCROOT and to run it from the root directory (app)

## Debug

For any debug see prebuild.log (in the root dir (app)) 
