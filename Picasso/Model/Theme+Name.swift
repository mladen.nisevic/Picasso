//
//  Theme+Name.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 27/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import Foundation

private extension String {
    static let theme = "Theme"
    static let defaultTheme = "DefaultTheme"
}

extension Theme {

    var fullName: String {
        String(describing: Self.self)
    }

    var brand: String {
        let comps = fullName.components(separatedBy: String.underscore)
        return comps.count > 1 ? comps[1] : .empty
    }

    var themeName: String {
        let comps = fullName.components(separatedBy: String.underscore)
        var themeName = String.empty
        if comps.count > 0 {
            themeName = comps[0].replacingOccurrences(of: String.theme, with: String.empty)
        }
        return themeName
    }

    var justName: String {
        fullName.dethemed
    }
    
    var nameNoBrand: String {
        let comps = fullName.components(separatedBy: String.underscore)
        var themeName = String.empty
        if comps.count > 0 {
            themeName = comps[0]
        }
        return themeName
    }
}

extension Theme {
    var isDefault: Bool {
        let def = String(describing: Self.self)
        let comps = def.components(separatedBy: String.underscore)
        guard !comps.isEmpty else { return false }
        return comps[0] == .defaultTheme
    }
}

extension String {
    var dethemed: String {
        let comps = self.components(separatedBy: String.theme)
        var themeName = String.empty
        if comps.count > 0 {
            themeName = comps[0]
        }
        return themeName
    }
}
