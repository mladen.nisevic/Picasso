//
//  BrandName.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 14/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import Foundation

/**
 BrandName === Target
 */
public enum BrandName: String, CaseIterable, Identifiable {

    public var id: String { self.rawValue }

    case BV, BildBet, BWin, HeartBingo, Parimatch, WilliamHill, TalkSport

    var fullName: String {
        switch self {
        case .BV: return "BetVictor"
        case .BWin: return "BiYing"
        default: return self.rawValue
        }
    }

    var targetThemesClassName: String {
        switch self {
            case .BV: return "BetVictor"
            default: return self.rawValue
        }
    }
#if targetEnvironment(macCatalyst)
    var preference: String {
        self.fullName + "_preference"
    }
    
    var themes: [NSObject] {
        switch self {
        case .BV: return ThemesModel().betVictorThemes
        case .BildBet: return ThemesModel().bildBetThemes
        case .BWin: return ThemesModel().bWinThemes
        case .HeartBingo: return ThemesModel().heartBingoThemes
        case .Parimatch: return ThemesModel().parimatchThemes
        case .WilliamHill: return ThemesModel().williamHillThemes
        case .TalkSport: return ThemesModel().talkSportThemes
        }
    }
    
    var isEnabled: Bool {
        UserDefaults.standard.bool(forKey: preference)
    }
#endif
}
