//
//  DataManager+Alpha.swift
//  Picasso
//
//  Created by Mladen Nisevic on 10/02/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import Foundation
import CoreData

// MARK: - Alpha
extension DataManager {

    @discardableResult
    func addAlpha(_ value: Double) -> Alpha? {
        
        if let pkey = alpha(with: value) {
            return pkey
        }
        
        let newItem = Alpha(context: self.viewContext)
        newItem.id = UUID()
        newItem.value = value
        newItem.name = TintHex.suffix(value)
        save()
        return newItem
    }
    
    func alpha(with value: Double) -> Alpha? {
        let fetchRequest: NSFetchRequest<Alpha>
        fetchRequest = Alpha.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: .valueEquals, value.number)
        var objects: [Alpha]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects?.first
    }
    
    func allAlphas() -> [Alpha]? {
        let fetchRequest: NSFetchRequest<Alpha>
        fetchRequest = Alpha.fetchRequest()
        var objects: [Alpha]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }
}

// MARK: - ColourAlpha
extension DataManager {

    @discardableResult
    func addColourAlpha(colour: Colour, alpha: Alpha, brand: Brand, source: ColourSource) -> ColourAlpha? {
        
        if let col = colourAlpha(colour: colour, alpha: alpha, brand: brand) {
            return col
        }
        
        let newItem = ColourAlpha(context: self.viewContext)
        newItem.id = UUID()
        newItem.alpha = alpha
        newItem.colour = colour
        newItem.brand = brand
        newItem.name = (colour.name ?? "") + (alpha.name ?? "")
        newItem.source = source.rawValue
        save()
        return newItem
    }
    
    func colourAlpha(colour: Colour, alpha: Alpha, brand: Brand) -> ColourAlpha? {
        colourAlphas(NSPredicate(format: .colourAlphaBrand, colour, alpha, brand))?.first
    }
    
    func colourAlpha(with colourName: String, brand: Brand) -> ColourAlpha? {
        colourAlphas(NSPredicate(format: .colourColourNameNameBrand, colourName, brand))?.first
    }
    
    func colourAlpha(hexValue: String, brand: Brand) -> ColourAlpha? {
        colourAlphas(NSPredicate(format: .colourHexValueBrand, hexValue, brand))?.first
    }

    func colourAlphas(for brand: Brand) -> [ColourAlpha]? {
        let predicate = NSPredicate(format: .brandEquals, brand)
        return colourAlphas(predicate)
    }
    
    func colourAlphas(source: ColourSource) -> [ColourAlpha]? {
        let predicate = NSPredicate(format: .sourceEquals, source.rawValue)
        return colourAlphas(predicate)
    }
    
    func colourAlphas(colour: Colour, brand: Brand) -> [ColourAlpha]? {
        colourAlphas(NSPredicate(format: .colourBrand, colour, brand))
    }

    func colourAlphas(_ predicate: NSPredicate? = nil) -> [ColourAlpha]? {
        let fetchRequest: NSFetchRequest<ColourAlpha>
        fetchRequest = ColourAlpha.fetchRequest()
        if let pred = predicate {
            fetchRequest.predicate = pred
        }
        var objects: [ColourAlpha]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }
}

extension Double {
    var number: NSNumber {
        NSNumber(value: self)
    }
}
