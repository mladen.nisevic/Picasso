//
//  DataManager+BrandColours.swift
//  Picasso
//
//  Created by Mladen Nisevic on 21/12/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import UIKit
import CoreData

// MARK: - Colours
extension DataManager {

    @discardableResult
    /// adding from parsed colours, i.e. UIColor extension
    /// Alpha + Colour (hex) = ColourAlpha
    func addColour(name: String, inputString: String, brand: Brand) -> Colour? {
        
        var colour: Colour?
        if let col = fetchColour(for: name, brand: brand) {
            colour = col
        } else {
            let newItem = Colour(context: self.viewContext)
            newItem.id = UUID()
            newItem.name = name
            newItem.brand = brand
            newItem.rawLine = inputString
            newItem.colourName = colourName(with: name)
            newItem.source = ColourSource.colour.rawValue
            newItem.colourName?.source = ColourSource.colour.rawValue
            colour = newItem
        }
        
        var alpha = addAlpha(.defaultAlpha)
        if let cf = ColourParser().processLine(rawLine: inputString, brand: brand) {
            if let hex = cf.hex, UIColor.isValid(hexCode: hex) {
                colour?.hexValue = hex
                colour?.descriptiveName = UIColor.descriptiveName(for: hex)
            }
            colour?.parent = cf.name
            if let alphaValue = cf.alpha,
                alphaValue != .defaultAlpha,
               let alphaDB = addAlpha(alphaValue) {
                alpha = alphaDB
            }
        } else {
            let msg = "\(Self.self):\(#function):\(#line): \(brand.name ?? .empty) \(name): input line missing"
            log(message: msg, level: .info)
        }
        
        if let colour, let alpha {
            if !colour.isHexOrParentPresent && !inputString.isEmpty {
                let msg = "\(Self.self):\(#function):\(#line): hex nil for \(colour.name ?? .empty) <- \(inputString)"
                log(message: msg, level: .info)
            }
            addColourAlpha(colour: colour, alpha: alpha, brand: brand, source: .colour)
        }
        save()
        return colour
    }
    
    @discardableResult
    /// adding from themes
    /// Alpha + Colour (hex) = ColourAlpha
    func addColourAlphaAndColourAlpha(colourName: ColourName, colourInfo: ColourInfo?, brand: Brand) -> ColourAlpha? {
        
        var colour: Colour?
        if let col = fetchColour(colourName: colourName, brand: brand) {
            colour = col
        } else {
            let newItem = Colour(context: self.viewContext)
            newItem.id = UUID()
            newItem.name = colourName.name
            newItem.brand = brand
            newItem.rawLine = colourInfo?.inputString ?? .empty
            newItem.colourName = colourName
            newItem.source = ColourSource.theme.rawValue
            newItem.colourName?.source = ColourSource.theme.rawValue
            colour = newItem
        }
        
        var alpha = addAlpha(.defaultAlpha)
        if let cf = colourInfo {
            if let hex = cf.hex, UIColor.isValid(hexCode: hex) {
                colour?.hexValue = hex
                colour?.descriptiveName = UIColor.descriptiveName(for: hex)
            }
            colour?.parent = cf.name
            if let alphaValue = cf.alpha,
                alphaValue != .defaultAlpha,
               let alphaDB = addAlpha(alphaValue) {
                alpha = alphaDB
            }
        } else {
            let msg = "\(Self.self):\(#function):\(#line): \(brand.name ?? .empty) \(colourName.name ?? .empty): input line missing"
            log(message: msg, level: .info)
        }
        
        var colourAlpha: ColourAlpha?
        if let colour, let alpha {
            if !colour.isHexOrParentPresent && colour.rawLine?.isEmpty == false {
                let msg = "\(Self.self):\(#function):\(#line): hex nil for \(colour.name ?? .empty) <- \(colourInfo?.inputString ?? .empty))"
                log(message: msg, level: .info)
            }
            colourAlpha = addColourAlpha(colour: colour, alpha: alpha, brand: brand, source: .theme)
        }
        save()
        return colourAlpha
    }
    
    @discardableResult
    func addColour(name: String, hex: String, alpha: Double = 1, brand: Brand, source: ColourSource = .colour) -> Colour? {
        
        if let col = fetchColour(for: name, brand: brand) {
            return col
        }
        
        let newItem = Colour(context: self.viewContext)
        newItem.id = UUID()
        newItem.name = name
        newItem.hexValue = hex
        newItem.descriptiveName = UIColor.descriptiveName(for: hex)
        newItem.colourName = colourName(with: name)
        newItem.source = source.rawValue
        save()
        return newItem
    }
    
    func fetchColour(colourName: ColourName, brand: Brand) -> Colour? {
        colours(NSPredicate(format: .colourNameBrand, colourName, brand))?.first
    }
    
    func fetchColour(for name: String, brand: Brand) -> Colour? {
        colours(NSPredicate(format: .nameBrand, name, brand))?.first
    }

    func colours(for brand: Brand) -> [Colour]? {
        let predicate = NSPredicate(format: .brandEquals, brand)
        return colours(predicate)
    }
    
    func hexLess() -> [Colour]? {
        let predicate = NSPredicate(format: .hexValueNil)
        return colours(predicate)
    }
    
    func colours(_ predicate: NSPredicate? = nil) -> [Colour]? {
        let fetchRequest: NSFetchRequest<Colour>
        fetchRequest = Colour.fetchRequest()
        if let pred = predicate {
            fetchRequest.predicate = pred
        }
        var objects: [Colour]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }
}

// MARK: - ColourName aka ColourName
extension DataManager {

    @discardableResult
    func addColourName(_ name: String, isThemes: Bool = true) -> ColourName? {
        
        if let pkey = colourName(with: name) {
            return pkey
        }
        
        let newItem = ColourName(context: self.viewContext)
        newItem.id = UUID()
        newItem.name = name
        newItem.themes = isThemes
        save()
        return newItem
    }
    
    func colourName(with name: String) -> ColourName? {
        colourNames(NSPredicate(format: .nameEquals, name))?.first
    }
    
    func colourNames(_ predicate: NSPredicate? = nil) -> [ColourName]? {
        let fetchRequest: NSFetchRequest<ColourName>
        fetchRequest = ColourName.fetchRequest()
        if let pred = predicate {
            fetchRequest.predicate = pred
        }
        var objects: [ColourName]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }
}
