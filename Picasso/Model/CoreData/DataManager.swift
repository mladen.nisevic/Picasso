//
//  DataManager.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 13/07/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import SwiftUI
import CoreData

extension String {
    static var aliasNameColorBrand = "aliasName = %@ AND color.brand = %@"
    static var aliasNameNameIgnoreCaseColorBrand = "aliasName.name ==[cd] %@ AND color.brand = %@"
    static var brandEquals = "brand = %@"
    static var colourAlphaBrand = "colour = %@ AND alpha = %@ AND brand = %@"
    static var colourAlphaColourBrand = "colourAlpha.colour = %@ AND brand = %@"
    static var colourBrand = "colour = %@ AND brand = %@"
    static var colourColourNameNameBrand = "colour.colourName.name = %@ AND brand = %@"
    static var colourHexValueBrand = "colour.hexValue = %@ AND brand = %@"
    static var colourNameBrand = "colourName = %@ AND brand = %@"
    static var hexValueEquals = "hexValue = %@"
    static var hexValueNil = "hexValue == nil"
    static var hexValueNilWithRawLine = "hexValue == nil AND rawLine != nil"
    static var nameBrand = "name = %@ AND brand = %@"
    static var nameColorBrand = "name = %@ AND color.brand = %@"
    static var nameEquals = "name = %@"
    static var nameEqualsIgnoreCase = "name ==[cd] %@"
    static var sourceEquals = "source = %@"
    static var themeName2Brand = "themeName = %@ AND brand = %@"
    static var valueEquals = "value = %@"
}

final class DataManager: DebugLoggable {

    static let shared = DataManager()
    var viewContext: NSManagedObjectContext
    
    init() {
        self.viewContext = PersistenceController.shared.container.viewContext
    }
    
    func delete(objects: [NSManagedObject]) {
        for object in objects {
            delete(object)
        }
    }
    
    func delete(_ object: NSManagedObject) {
        viewContext.delete(object)
        save()
    }
    
    func save() {
        do {
            try self.viewContext.save()
        } catch {

            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}

// MARK: - Brand
extension DataManager {
    @discardableResult
    func add(from brandJSON: BrandJSON) -> Brand? {
        
        if let brando = brand(with: brandJSON.name) {
            return brando
        }
        
        let newItem = Brand(context: self.viewContext)
        newItem.id = UUID()
        newItem.name = brandJSON.name
        newItem.fullName = brandJSON.fullName
        save()
        return newItem
    }
    
    func brand(with name: String) -> Brand? {
        brands(NSPredicate(format: .nameEquals, name))?.first
    }
    
    func brands(_ predicate: NSPredicate? = nil) -> [Brand]? {
        let fetchRequest: NSFetchRequest<Brand>
        fetchRequest = Brand.fetchRequest()
        if let pred = predicate {
            fetchRequest.predicate = pred
        }
        var objects: [Brand]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {

            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }
}

// MARK: - History
extension DataManager {
    func addItem(_ search: String) {
        let newItem = HistoryItem(context: viewContext)
        newItem.timestamp = Date()
        newItem.searchString = search
        newItem.id = UUID()
        newItem.isExact = SettingsHelper.shared.isExactMatch
        newItem.brandSelection = SettingsHelper.shared.selectedBrand
        save()
    }
}
