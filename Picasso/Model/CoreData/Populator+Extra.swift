//
//  Populator+Extra.swift
//  Picasso
//
//  Created by Mladen Nisevic on 03/03/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import UIKit

private extension String {
    static let alphaColonSpace = "alpha: "
    static let colon = ":"
    static let dotFromHexPrefix = ".fromHex(0x"
    static let fromHexPrefix = "fromHex(0x"
    static let fullStop = "."
    static let parenthesis = "()"
    static let rightParenthesis = ")"
    static let leftParenthesis = "("
    static let theme = "Theme"
    static let withAlphaComponent = "withAlphaComponent"
}

struct HexAlpha {
    var hex: String
    var alpha: Double?
}

extension Populator: DebugLoggable {
    
    func populateMissingHexValues() {
        
        let hexLessWithInput = getNoHexlessWithInput()
        
        guard let hexLessWithInput, !hexLessWithInput.isEmpty else { return }
        
        for hexlessColour in hexLessWithInput {
            guard let rawLine = hexlessColour.rawLine, let brand = hexlessColour.brand else { continue }
            guard let colourInfo = ColourParser().processLine(rawLine: rawLine, brand: brand, colour: hexlessColour) else {
                printFailureInfo(hexlessColour, brand: brand)
                continue
            }
            update(hexlessColour, with: colourInfo, brand: brand)
        }
        dataManager.save()
        getNoHexlessWithInput()
    }
    
    private func printFailureInfo(_ hexlessColour: Colour, brand: Brand) {
        print("FAILED: \(hexlessColour.brand?.name ?? .empty): \(hexlessColour.name ?? .empty) ==> \(hexlessColour.rawLine ?? .empty)")
    }
    
    private func update(_ hexlessColour: Colour, with colourInfo: ColourInfo, brand: Brand) {
        
        hexlessColour.hexValue = colourInfo.hex
        if let alphaValue = colourInfo.alpha,
           let alpha = dataManager.addAlpha(alphaValue) {
            if let colourAlphas = dataManager.colourAlphas(colour: hexlessColour, brand: brand) {
                if colourAlphas.count == 1 {
                    colourAlphas.first?.alpha = alpha
                } else {
                    let msg = "\(Self.self):\(#function):\(#line): ERROR: multiple colour-alphas."
                    log(message: msg, level: .error)
                }
            }
        }
    }
}

// MARK: - Parser methods
extension Populator {

    @discardableResult
    private func getNoHexlessWithInput() -> [Colour]? {
        let colours = dataManager.colours()
        var msg = "\(Self.self):\(#function):\(#line): There are \(colours?.count ?? 0) colours"
        log(message: msg, level: .debug)
        let hexLess = colours?.filter { !$0.isHexValuePresent }
        msg = "\(Self.self):\(#function):\(#line): of which without a valid hex value: \(hexLess?.count ?? 0)"
        log(message: msg, level: .debug)
        let hexLessWithInput = hexLess?.filter { $0.rawLine != nil && $0.rawLine?.isEmpty == false }
        msg = "\(Self.self):\(#function):\(#line): of which hexLess with a valid input data: \(hexLessWithInput?.count ?? 0)"
        log(message: msg, level: .debug)
        return hexLessWithInput
    }
}
