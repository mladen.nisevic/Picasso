//
//  HistoryItem+Description.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 01/06/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import Foundation

private extension String {
    static let ddMMyyyHHmmss = "dd.MM.yyy HH:mm:ss"
    static let exact = "Exact"
}

extension DateFormatter {
    convenience init(_ format: String) {
        self.init()
        dateFormat = format
    }
    func string(_ format: String, date: Date) -> String {
        DateFormatter(format).string(from: date)
    }
}

extension HistoryItem {
    override public var description: String {
        var str: String = .empty
        if let ts = timestamp {
            str += "\(DateFormatter(.ddMMyyyHHmmss).string(from: ts)) "
        }
        str += searchString ?? .empty
        if let bs = brandSelection, let brand = BrandName(rawValue: bs) {
            str += " \(brand.rawValue)"
        }
        str += ", \(isExact ? .exact : String.empty)"
        return str
    }
}
