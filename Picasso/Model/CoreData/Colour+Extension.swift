//
//  Colour+Extension.swift
//  Picasso
//
//  Created by Mladen Nisevic on 13/02/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import Foundation
import CoreData

extension Colour {
    
    var isHexValuePresent: Bool {
        hexValue != nil && !hexValue!.isEmpty
    }
    
    var isParentPresent: Bool {
        parent != nil && !parent!.isEmpty
    }
    
    var isHexOrParentPresent: Bool {
        isParentPresent || isHexValuePresent
    }
}
