//
//  Populator+Theme.swift
//  Picasso
//
//  Created by Mladen Nisevic on 03/01/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import Foundation

private extension String {
    static let picasso = "Picasso."
    static let afterUnderscore = "_.*"
    static let themes = "Themes"
}

// MARK: - Themes
extension Populator {
    
    func populateThemes() {
        
        BrandName.allCases.forEach { brandName in
            guard let brand = dataManager.brand(with: brandName.rawValue) else { return } // continue
            let themes = readThemes(for: brandName)
            let baseThemes = themes.filter { $0.parentName == String.empty }
            for theme in baseThemes {
                dataManager.addTheme(from: theme, brand: brand)
            }
            
            let remaninderThemes = Set(themes).subtracting(Set(baseThemes))

            for theme in remaninderThemes {
                 dataManager.addTheme(from: theme, brand: brand)
            }

            // second pass for additional inherited
            for theme in remaninderThemes {
                dataManager.addTheme2ndPass(from: theme, brand: brand)
            }
        }
    }
    
    private func readThemes(for brandName: BrandName) -> [InputTheme] {
        let filename = String.themes + brandName.rawValue + String.dotJson
        let themes = Bundle.main.decode([InputTheme].self, from: filename)
        return themes
    }

    private func parentlessInfo(_ brand: Brand) -> (Int, [String])? {
        guard let total = dataManager.themes(for: brand) else { return nil }
        let parentless = total.filter { $0.parent == nil }
        let names = parentless.compactMap { $0.name }
        return (parentless.count, names)
    }
}
