//
//  Populator.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 13/07/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import Foundation
import SwiftUI

extension String {
    static let black = "black"
    static let blackFromHex = ".fromHex(0x000000)"
    static let blackHex = "000000"
    static let clear = "clear"
    static let clearFromHex = ".fromHex(0x000000)"
    static let darkGray = "darkGray"
    static let justFromHex = "fromHex"
    static let red = "red"
    static let white = "white"
    static let whiteFromHex = ".fromHex(0xFFFFFF)"
    static let whiteHex = "FFFFFF"
    static var appearance = "Appearance"
    static var appearanceAlias = "UICOLORALIAS"
    static var appearanceColor = "UICOLOR"
    static var brandColoursFileName = "BrandColours.json"
    static var brandFileName = "Brands.json"
    static var bvAppearance = "BVAppearance"
    static var colourNamesFileName = "ColourNames.json"
    static var jsonExtension = "json"
}

enum TintHex: String, CaseIterable {
    case _02,
    _10,
    _20,
    _30,
    _40,
    _50,
    _60,
    _70,
    _80,
    _90
    
    var hex: String {
        switch self {
        case ._02: return "05"
        case ._10: return "1A"
        case ._20: return "33"
        case ._30: return "4D"
        case ._40: return "66"
        case ._50: return "80"
        case ._60: return "99"
        case ._70: return "B3"
        case ._80: return "CC"
        case ._90: return "E6"
        }
    }
    
    var doubleValue: Double {
        switch self {
        case ._02: return 0.02
        case ._10: return 0.10
        case ._20: return 0.20
        case ._30: return 0.30
        case ._40: return 0.40
        case ._50: return 0.50
        case ._60: return 0.60
        case ._70: return 0.70
        case ._80: return 0.80
        case ._90: return 0.90
        }
    }
    
    var int: Int {
        switch self {
        case ._02: return 2
        case ._10: return 10
        case ._20: return 20
        case ._30: return 30
        case ._40: return 40
        case ._50: return 50
        case ._60: return 60
        case ._70: return 70
        case ._80: return 80
        case ._90: return 90
        }
    }
        
    static func suffix(_ double: Double) -> String {
        String(format: "_%02d", Int(double * 100))
    }
}

typealias ColourNames = [String]

final class Populator {
    static let shared = Populator()
    
    var decoder: JSONDecoder
    var dataManager: DataManager
    
    init(decoder: JSONDecoder = .init()) {
        self.decoder = decoder
        self.dataManager = .shared
    }
    
    func populate() {
        populateBrands()
        populateAppearance()
        detectDuplicates()
        populateColourNames()
        populateColours()
        populateThemes()
    }
    
    func depopulate() {
        removeAppearanceStuff()
        removeColourAlphas()
        removeAlphas()
        removeColours()
        removeColourNames()
        removeBrands()
        removeThemesAndNames()
    }
    
    private func populateBrands() {
        let brands = Bundle.main.decode([BrandJSON].self, from: .brandFileName)
        for brand in brands {
            dataManager.add(from: brand)
        }
    }
}

// MARK: - Colour i.e. Themes Colours
extension Populator {
    
    // brand - palette - colourname - colour 
    private func populateColourNames() {
        let colourNames = Bundle.main.decode([String].self, from: .colourNamesFileName)
        for colourName in colourNames {
            dataManager.addColourName(colourName)
        }
    }
    
    private func populateColours() {
        let brandsColours = Bundle.main.decode([BrandColours].self, from: .brandColoursFileName)
        for brandColours in brandsColours {
            guard let brand = dataManager.brand(with: brandColours.brand) else { continue }
            
            let fromHexDefined = brandColours.brandColours.filter { $0.colour.contains(String.justFromHex)}
            
            fromHexDefined.forEach { brandColour in
                self.dataManager.addColour(name: brandColour.name, inputString: brandColour.colour, brand: brand)
            }
            
            let secondaryDefinition = Set(brandColours.brandColours).subtracting(Set(fromHexDefined))
            secondaryDefinition.forEach { brandColour in
                self.dataManager.addColour(name: brandColour.name, inputString: brandColour.colour, brand: brand)
            }
            addWhiteAndBlack(for: brand)
            populateSecondary(for: brand)
        }
    }
    
    private func addWhiteAndBlack(for brand: Brand) {
        guard let alpha = dataManager.addAlpha(.defaultAlpha) else {
            print("\(date2()):\(Self.self):\(#function):\(#line): Unable to add/fetch alpha 1.0")
            return
        }
        
        if let white = dataManager.addColour(name: .white, inputString: .whiteFromHex, brand: brand) {
            dataManager.addColourAlpha(colour: white, alpha: alpha, brand: brand, source: .colour)
        }

        if let black = dataManager.addColour(name: .black, inputString: .blackFromHex, brand: brand) {
            dataManager.addColourAlpha(colour: black, alpha: alpha, brand: brand, source: .colour)
        }
        
        guard let clearAlpha = dataManager.addAlpha(.zero) else {
            print("\(date2()):\(Self.self):\(#function):\(#line): Unable to add/fetch alpha 0.0")
            return
        }
        
        if let clear = dataManager.addColour(name: .clear, inputString: .clearFromHex, brand: brand) {
            dataManager.addColourAlpha(colour: clear, alpha: clearAlpha, brand: brand, source: .colour)
        }
    }
    
    /// This should be able to resolve ancestor colour up to and including a grandparent, but not a great-grandparent
    /// so 'circularProgressSubtitle - unselectedChip - underline' will work
    /// why do you need this in the first place? just 'circularProgressSubtitle - underline' is the same, no?
    /// In fact, you could just use underline in any Theme, without adding any more colours
    private func populateSecondary(for brand: Brand) {
        let colours = dataManager.colours(for: brand)
        let noHex = colours?.filter { !$0.isHexValuePresent }
        let coloursWithParent = noHex?.filter { $0.isParentPresent }
        coloursWithParent?.forEach { colour in
            if !colour.isHexValuePresent {
                let parentColour = colours?.first(where: { $0.name == colour.parent })
                colour.hexValue = parentColour?.hexValue
                colour.descriptiveName = UIColor.descriptiveName(for: colour.hexValue)
                if colour.hexValue == nil {
                    let grandparentColour = colours?.first(where: { $0.name == parentColour?.parent })
                    colour.hexValue = grandparentColour?.hexValue
                    colour.descriptiveName = UIColor.descriptiveName(for: colour.hexValue)
                    if colour.hexValue == nil {
                        print("\(date2()):\(Self.self):\(#function):\(#line): no hex value: \(colour) >> parent: \(parentColour?.description ?? "")")
                    }
                }
            }
        }
    }
    
    private func removeBrands() {
        if let brands = dataManager.brands() {
            dataManager.delete(objects: brands)
        }
    }
    
    private func removeColourNames() {
        if let colourNames = dataManager.colourNames() {
            dataManager.delete(objects: colourNames)
        }
    }
    
    private func removeColours() {
        if let colourNames = dataManager.colours() {
            dataManager.delete(objects: colourNames)
        }
    }
    
    private func removeAlphas() {
        if let alphas = dataManager.allAlphas() {
            dataManager.delete(objects: alphas)
        }
    }
    
    private func removeColourAlphas() {
        if let colourAlphas = dataManager.colourAlphas() {
            dataManager.delete(objects: colourAlphas)
        }
    }

    private func removeThemesAndNames() {
        if let colourNames = dataManager.themeNames() {
            dataManager.delete(objects: colourNames)
        }
        if let colourNames = dataManager.themes() {
            dataManager.delete(objects: colourNames)
        }
    }
    
    private func removeAppearanceStuff() {
        if let aliases = dataManager.aliases() {
            dataManager.delete(objects: aliases)
        }
        if let aliaseNames = dataManager.aliasNames() {
            dataManager.delete(objects: aliaseNames)
        }
        if let colourNames = dataManager.appearanceColors() {
            dataManager.delete(objects: colourNames)
        }
    }
}

// MARK: - Appearance Colours
extension Populator {
    
    private func populateAppearance() {
        
        guard let brands = dataManager.brands() else {
            print("ERROR: no brands!")
            return
        }
        
        for brand in brands {
            guard let name = brand.name else { continue }
            let filename = name + .appearance
            if let appearance = readJSONSerial(from: filename) {
                dataManager.addAppearanceColour(appearance.colours, brand: brand)
                dataManager.add(aliases: appearance.aliases, brand: brand)
            }
        }
    }

    private func detectDuplicates() {
        guard let brands = dataManager.brands() else {
            print("ERROR: no brands!")
            return
        }
        
        var multibrandAliasesMap = [String: [String]]()
        var duplicates = Set<[String]>()
        
        for brand in brands {
            guard let name = brand.name else { continue }
            let filename = name + .appearance
            guard let appearance = readJSONSerial(from: filename)  else { continue }
            
            for alias in appearance.aliases {
                let aliasName = alias.key
                guard let aliasHexColor = appearance.colours.first(where: { $0.key == alias.value })?.value else { return }
                if let existing = multibrandAliasesMap.first(where: { $0.key == aliasName }) {
                    multibrandAliasesMap[aliasName] = existing.value + [aliasHexColor]
                } else {
                    multibrandAliasesMap[aliasName] = [aliasHexColor]
                }
            }
        }
        
        for elem in multibrandAliasesMap {
            let synonyms = multibrandAliasesMap.filter({ $0.value == elem.value})
            if synonyms.count > 1 {
                duplicates.insert(synonyms.map({ $0.key }) + [elem.key])
            }
        }
        
        if duplicates.isEmpty {
            print("✅ No duplicated aliases detecrted")
        } else {
            print("❌ Dupliacted aliases detected, check the following:")
            duplicates.forEach({ print("\($0) \n") })
        }
        
    }

    typealias ColoursAndAliases = (colours: [String: String], aliases: [String: String])

    /// Brand
    func readJSONSerial(from filename: String) -> ColoursAndAliases? {

        var coloursAndAliases: ColoursAndAliases = ([:], [:])

        if let fileURL = Bundle.main.url(forResource: filename, withExtension: .jsonExtension) {
            let jsonString = try! String(contentsOf: fileURL)
            let data = jsonString.data(using: .utf8) ?? Data()
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    if let colours = json[.appearanceColor] as? [String: String] {
                        coloursAndAliases.colours = colours
                    }
                    if let aliases = json[.appearanceAlias] as? [String: String] {
                        coloursAndAliases.aliases = aliases
                    }
                }
            } catch DecodingError.keyNotFound(let key, let context) {
                let msg = "could not find key \(key) in JSON: \(context.debugDescription)"
                print(msg)
            } catch DecodingError.valueNotFound(let type, let context) {
                let msg = "could not find type \(type) in JSON: \(context.debugDescription)"
                print(msg)
            } catch DecodingError.typeMismatch(let type, let context) {
                let msg = "type mismatch for type \(type) in JSON: \(context.debugDescription)"
                print(msg)
            } catch DecodingError.dataCorrupted(let context) {
                let msg = "data found to be corrupted in JSON: \(context.debugDescription)"
                print(msg)
            } catch let error as NSError {
                let msg = "Error in read(from:ofType:) domain= \(error.domain), description= \(error.localizedDescription)"
                print(msg)
            } catch {
                print("error: \(error.localizedDescription)")
            }
        }
        return coloursAndAliases
    }
}

struct BrandJSON: Codable {
    let name: String
    let fullName: String
}
