//
//  Brand+Utils.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 13/07/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import Foundation

typealias SortDescriptor<Value> = (Value, Value) -> Bool

let sortAppearanceColourByNameAsc: SortDescriptor<AppearanceColor> = { $0.name ?? .empty < $1.name ?? .empty }
let sortColourByNameAsc: SortDescriptor<Colour> = { $0.name ?? .empty < $1.name ?? .empty }
let sortThemesByNameAsc: SortDescriptor<Theme2> = { $0.name ?? .empty < $1.name ?? .empty }
let sortAliasByNameAsc: SortDescriptor<AppearanceAlias> = { ($0.name ?? .empty).localizedStandardCompare($1.name ?? .empty) == .orderedAscending }

extension Brand {
    var appearanceColoursArray: [AppearanceColor] {
        if let colSet = appearanceColors,
            let array = Array(colSet) as? [AppearanceColor] {
            return array.sorted(by: sortAppearanceColourByNameAsc)
        }
        return []
    }
    
    var aliasesArray: [AppearanceAlias] {
        var aliases = [AppearanceAlias]()
        appearanceColoursArray.forEach { aliases.append(contentsOf: $0.aliasesArray) }
        return aliases.sorted(by: sortAliasByNameAsc)
    }
    
    var coloursArray: [Colour] {
        if let colSet = colourAlphas,
            let array = Array(colSet) as? [Colour] {
            return array.sorted(by: sortColourByNameAsc)
        }
        return []
    }
    
    var themesArray: [Theme2] {
        if let colSet = themes,
            let array = Array(colSet) as? [Theme2] {
            return array.sorted(by: sortThemesByNameAsc)
        }
        return []
    }
    
    func theme(for themeName: String) -> Theme2? {
        let candidates = themesArray.filter { $0.name == themeName }
        return candidates.first
    }
}

extension AppearanceColor {
    var aliasesArray: [AppearanceAlias] {
        if let colSet = aliases,
            let array = Array(colSet) as? [AppearanceAlias] {
            return array
        }
        return []
    }
}
