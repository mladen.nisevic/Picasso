//
//  DataManager+Appearance.swift
//  Picasso
//
//  Created by Mladen Nisevic on 19/12/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import UIKit
import CoreData

// MARK: - AppearanceColor
extension DataManager {
    
    /// Import Appearance Colours from Dictionary into CoreData
    /// - Parameter colours: Dictionary name: String, hexValue: String
    func addAppearanceColour(_ colours: [String: String], brand: Brand) {
        for colour in colours {
            guard let colourKey = AppearanceColourKey(rawValue: colour.key) else { continue }
            if appearanceColor(with: colourKey, brand: brand) != nil {
                continue // exists already
            }
            add(colourKey, hexValue: colour.value, brand: brand)
        }
    }
    
    func appearanceColor(with colourKey: AppearanceColourKey, brand: Brand) -> AppearanceColor? {
        appearanceColors(NSPredicate(format: .nameBrand, colourKey.rawValue, brand))?.first
    }
    
    func appearanceColor(name: String, brand: Brand) -> AppearanceColor? {
        appearanceColors(NSPredicate(format: .nameBrand, name, brand))?.first
    }

    @discardableResult
    func add(_ colourKey: AppearanceColourKey, hexValue: String, brand: Brand) -> AppearanceColor? {
        let newItem = AppearanceColor(context: viewContext)
        newItem.id = UUID()
        newItem.name = colourKey.rawValue
        newItem.hexValue = hexValue
        newItem.brand = brand
        newItem.descriptiveName = UIColor.descriptiveName(for: hexValue)
        save()
        return newItem
    }
}

// MARK: - AppearanceColor fetch
extension DataManager {
    func appearanceColors(_ predicate: NSPredicate? = nil) -> [AppearanceColor]? {
        let fetchRequest: NSFetchRequest<AppearanceColor>
        fetchRequest = AppearanceColor.fetchRequest()
        if let pred = predicate {
            fetchRequest.predicate = pred
        }
        var objects: [AppearanceColor]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }

}

// MARK: - Appearance Alias
extension DataManager {
    
    /// Import Appearance Aliases from Dictionary into CoreData
    /// - Parameter colours: Dictionary name: String, hexValue: String
    func add(aliases: [String: String], brand: Brand) {
        
        for alias in aliases {
            
            guard let aliasName = addAliasName(alias.key) else {
                print("\(Date()):\(Self.self):\(#function):\(#line): no alias name for \(alias.key)")
                continue
            }
            
            if appearanceAlias(with: aliasName, brand: brand) != nil {
                continue // exists already
            }
            
            guard let colourKey = AppearanceColourKey(rawValue: alias.value) else { continue }
            if let colour = appearanceColor(with: colourKey, brand: brand) {
                add(aliasName, colour: colour)
            }
        }
    }
    
    func appearanceAlias(with aliasName: AppearanceAliasName, brand: Brand) -> AppearanceAlias? {
        aliases(NSPredicate(format: .aliasNameColorBrand, aliasName, brand))?.first
    }
    
    func appearanceAlias(name: String, brand: Brand) -> AppearanceAlias? {
        aliases(NSPredicate(format: .aliasNameNameIgnoreCaseColorBrand, name, brand))?.first
    }

    @discardableResult
    func add(_ aliasName: AppearanceAliasName, colour: AppearanceColor) -> AppearanceAlias? {
        let newItem = AppearanceAlias(context: viewContext)
        newItem.id = UUID()
        newItem.aliasName = aliasName
        newItem.name = aliasName.name
        newItem.color = colour
        save()
        return newItem
    }
}

// MARK: - AppearanceAlias fetch
extension DataManager {
    
    func aliases(with name: String) -> [AppearanceAlias]? {
        let predicate = NSPredicate(format: .nameEqualsIgnoreCase, name)
        return aliases(predicate)
    }
    
    func aliases(_ predicate: NSPredicate? = nil) -> [AppearanceAlias]? {
        let fetchRequest: NSFetchRequest<AppearanceAlias>
        fetchRequest = AppearanceAlias.fetchRequest()
        if let pred = predicate {
            fetchRequest.predicate = pred
        }
        var objects: [AppearanceAlias]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }
}

// MARK: - Appearance Alias Name
extension DataManager {
    
    private func addAliasName(_ aliasName: String) -> AppearanceAliasName? {
        
        if let dbAliasName = appearanceAliasName(with: aliasName) {
            return dbAliasName
        }
        
        let newItem = AppearanceAliasName(context: self.viewContext)
        newItem.id = UUID()
        newItem.name = aliasName
        save()
        return newItem
    }
    
    func appearanceAliasName(with name: String) -> AppearanceAliasName? {
        aliasNames(NSPredicate(format: .nameEqualsIgnoreCase, name))?.first
    }
    
    func aliasNames(_ predicate: NSPredicate? = nil) -> [AppearanceAliasName]? {
        let fetchRequest: NSFetchRequest<AppearanceAliasName>
        fetchRequest = AppearanceAliasName.fetchRequest()
        if let pred = predicate {
            fetchRequest.predicate = pred
        }
        var objects: [AppearanceAliasName]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }
}
