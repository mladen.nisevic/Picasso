//
//  Colour+Extension.swift
//  Picasso
//
//  Created by Mladen Nisevic on 22/12/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import UIKit
import CoreData

extension ColourAlpha {
    
    var hexValue: String? {
        colour?.hexValue
    }
    
    var alphaValue: Double? {
        alpha?.value
    }
    
    var brandName: String? {
        brand?.name
    }

    var uiColor: UIColor {
        var uiColor = UIColor.clear
        if let hex = colour?.hexValue, UIColor.isValid(hexCode: hex),
           let col = UIColor(hex: hex, alpha: CGFloat(alpha?.value ?? 1)) {
            uiColor = col
        }
        return uiColor
    }
    
    var hexOrPlaceholder: String {
        if let hv = colour?.hexValue, !hv.isEmpty {
            return hv
        }
        return "<No Hex Value>"
    }
    
    var nameOrPlaceholder: String {
        if let name = name, !name.isEmpty {
            return name
        }
        return "<colourName>"
    }
    
    var parentOrPlaceholder: String {
        if let parent = colour?.parent, !parent.isEmpty {
            return parent
        }
        return "<parent>"
    }
    
    var isParentPresent: Bool {
        if let colour {
            return colour.parent != nil && !colour.parent!.isEmpty
        }
        return false
    }
    
    var isHexValuePresent: Bool {
        if let colour {
            return colour.hexValue != nil && !colour.hexValue!.isEmpty
        }
        return false
    }
    
    func alphaToHex() {
        if let colour {
            let alpha = CGFloat(alpha?.value ?? 1)
            if let hex = colour.hexValue, let col = UIColor(hex: hex, alpha: alpha) {
                colour.hexValue = col.hexString ?? colour.hexValue
            }
        }
    }
}

extension Array where Element == ColourAlpha {
    func first(for inputColour: InputColour?) -> ColourAlpha? {
        first(where: { $0.colour?.colourName?.name == inputColour?.rawInput })
    }
}

extension ColourAlpha {
    public override var description: String {
        "\(brand?.name ?? .empty): \(name ?? .empty): \(colour as Colour?) , alpha: \(alpha as Alpha?)"
    }
}

extension Colour {
    public override var description: String {
        "\(brand?.name ?? .empty): \(name ?? .empty): \(hexValue ?? .empty) , rawLine: \(rawLine ?? .empty)"
    }
}

extension Alpha {
    public override var description: String {
        "\(name ?? .empty): \(value)"
    }
}
