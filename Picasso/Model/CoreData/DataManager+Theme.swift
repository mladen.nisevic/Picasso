//
//  DataManager+Theme.swift
//  Picasso
//
//  Created by Mladen Nisevic on 03/01/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import UIKit
import CoreData

private extension String {
    static let parentNil = "parent == nil"
}

// MARK: - Theme
extension DataManager {

    @discardableResult
    func addTheme(from inputTheme: InputTheme, brand: Brand) -> Theme2? {
        
        if let col = theme(name: inputTheme.name, brand: brand) {
            return col
        }
        
        let colours = colourAlphas(for: brand)
        
        let newItem = Theme2(context: self.viewContext)
        newItem.id = UUID()
        newItem.name = inputTheme.name
        if let parentTheme = theme(name: inputTheme.parentName, brand: brand) {
            newItem.parent = parentTheme
        }
        newItem.brand = brand
        populateColours(newItem, inputColours: inputTheme.colours, brandColours: colours)
        newItem.themeName = addThemeName(inputTheme.name)
        save()
        return newItem
    }
    
    @discardableResult
    func addTheme2ndPass(from inputTheme: InputTheme, brand: Brand) -> Theme2? {
        
        guard let dbTheme = theme(name: inputTheme.name, brand: brand) else {
            return addTheme2ndPass(from: inputTheme, brand: brand)
        }
        
        if dbTheme.parent != nil {
            return dbTheme
        }
        
        guard let parentTheme = theme(name: inputTheme.parentName, brand: brand) else {
            return dbTheme
        }

        // exists, but has no parent, let's retry
        let colours = colourAlphas(for: brand)
        dbTheme.parent = parentTheme
        populateColours(dbTheme, inputColours: inputTheme.colours, brandColours: colours)
        save()
        return dbTheme
    }
    
    func theme(_ themeName: ThemeName2, brand: Brand) -> Theme2? {
        themes(NSPredicate(format: .themeName2Brand, themeName, brand))?.first
    }
    
    func theme(name: String, brand: Brand) -> Theme2? {
        themes(NSPredicate(format: .nameBrand, name, brand))?.first
    }

    func themes(for brand: Brand) -> [Theme2]? {
        themes(NSPredicate(format: .brandEquals, brand))
    }

    func parentless() -> [Theme2]? {
        themes(NSPredicate(format: .parentNil))
    }
    
    func themes(_ predicate: NSPredicate? = nil) -> [Theme2]? {
        let fetchRequest: NSFetchRequest<Theme2>
        fetchRequest = Theme2.fetchRequest()
        if let pred = predicate {
            fetchRequest.predicate = pred
        }
        var objects: [Theme2]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }
}

// MARK: - Theme, private
extension DataManager {
    
    private func populateColours(_ newItem: Theme2, inputColours: [InputColour], brandColours: [ColourAlpha]?) {
        
        if let parent = newItem.parent {
            
            if newItem.accessorySymbol == nil {
                newItem.accessorySymbol = parent.accessorySymbol
            }
            
            if newItem.background == nil {
                newItem.background = parent.background
            }
            
            if newItem.body == nil {
                newItem.body = parent.body
            }

            if newItem.border == nil {
                newItem.border = parent.border
            }
            
            if newItem.cardBackground == nil {
                newItem.cardBackground = parent.cardBackground
            }
            
            if newItem.subtitle == nil {
                newItem.subtitle = parent.subtitle
            }
            
            if newItem.title == nil {
                newItem.title = parent.title
            }
            
            if newItem.underline == nil {
                newItem.underline = parent.underline
            }
        }

        newItem.accessorySymbol = populate(.accessorySymbol, inputColours, brandColours, newItem) ?? newItem.accessorySymbol
        newItem.background = populate(.background, inputColours, brandColours, newItem) ?? newItem.background
        newItem.body = populate(.body, inputColours, brandColours, newItem) ?? newItem.body
        newItem.border = populate(.border, inputColours, brandColours, newItem) ?? newItem.border
        newItem.cardBackground = populate(.cardBackground, inputColours, brandColours, newItem) ?? newItem.cardBackground
        newItem.subtitle = populate(.subtitle, inputColours, brandColours, newItem) ?? newItem.subtitle
        newItem.title = populate(.title, inputColours, brandColours, newItem) ?? newItem.title
        newItem.underline = populate(.underline, inputColours, brandColours, newItem) ?? newItem.underline
    }
    
    /// fetch a theme colour (title, background, etc) from the defined brand colours
    /// or if that fails, re-parse the raw input, and try to find a corresponding colour there
    /// these colours could have a different alpha or could have some inheritance related definition
    /// - Parameters:
    ///   - colourName: colour of theme
    ///   - inputColours: raw colour theme data, slightly processed
    ///   - brandColours: defined brand colours
    /// - Returns: an actual colour for the corresponding theme colour
    fileprivate func populate(_ colourName: ColourNameEnum, _ inputColours: [InputColour], _ brandColours: [ColourAlpha]?, _ theme: Theme2) -> ColourAlpha? {
        if let inputColour = inputColours.first(for: colourName),
           let colour = brandColours?.first(for: inputColour) {
            return colour
        }
        guard let colourLine = inputColours.first(where: { $0.name == colourName.rawValue } ) else { return nil }
        guard let brand = brandColours?.first?.brand else { return nil }
        guard let cf = ColourParser().processLine(rawLine: colourLine.rawInput, brand: brand, theme: theme) else { return nil }
        guard let name = cf.name else { return nil }
        
        if let col = getBrandColour(cf, name: name, brandColours: brandColours) {
            return col
        } else if let col = getColourFromTheme(theme, cf: cf, colourNameEnum: colourName, brandColours: brandColours) {
            return col
        }
        return nil
    }
    
    private func getBrandColour(_ cf: ColourInfo, name: String, brandColours: [ColourAlpha]?) -> ColourAlpha? {
        if let alpha = cf.alpha {
            let name1 = name + TintHex.suffix(alpha)
            if let col = brandColours?.first(where: { $0.colour?.colourName?.name == name1 }) {
                return col
            }
        } else {
            if let col = brandColours?.first(where: { $0.colour?.colourName?.name == name }) {
                return col
            }
        }
        return nil
    }
    
    /// // didn't find this colour in UIColor extension, add it as a new
    /// colourName = theme.name + colour purpose e.g. title
    /// colour =
    /// colourAlphaName = above + alpha suffix
    /// alpha = alpha
    private func getColourFromTheme(_ theme: Theme2, cf: ColourInfo, colourNameEnum: ColourNameEnum, brandColours: [ColourAlpha]?) -> ColourAlpha? {
        guard let brand = brandColours?.first?.brand else {
            let msg = "\(Self.self):\(#function):\(#line): unable to add colour name \(colourNameEnum.rawValue)"
            log(message: msg, level: .error)
            return nil
        }
        
        let newColourName = (theme.name ?? .empty) + String.underscore + colourNameEnum.rawValue
        guard let cn = addColourName(newColourName) else {
            let msg = "\(Self.self):\(#function):\(#line): unable to add colour name \(newColourName)"
            log(message: msg, level: .error)
            return nil
        }
        
        return addColourAlphaAndColourAlpha(colourName: cn, colourInfo: cf, brand: brand)
    }
 }

// MARK: - ThemeName
extension DataManager {

    @discardableResult
    func addThemeName(_ name: String) -> ThemeName2? {
        
        if let pkey = themeName(with: name) {
            return pkey
        }
        
        let newItem = ThemeName2(context: self.viewContext)
        newItem.id = UUID()
        newItem.name = name
        save()
        return newItem
    }
    
    func themeName(with name: String) -> ThemeName2? {
        themeNames(NSPredicate(format: .nameEquals, name))?.first
    }
    
    func themeNames(_ predicate: NSPredicate? = nil) -> [ThemeName2]? {
        let fetchRequest: NSFetchRequest<ThemeName2>
        fetchRequest = ThemeName2.fetchRequest()
        if let pred = predicate {
            fetchRequest.predicate = pred
        }
        var objects: [ThemeName2]?
        do {
             objects = try self.viewContext.fetch(fetchRequest)
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return objects
    }
}

