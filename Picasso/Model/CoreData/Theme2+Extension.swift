//
//  Theme2+Extension.swift
//  Picasso
//
//  Created by Mladen Nisevic on 03/01/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import Foundation
import CoreData

extension Theme2 {
    
    var coloursDescription: String {
        """
\(themeName?.name ?? "")
        "background: \(String(describing: background))
        "title: \(String(describing: title))
"""
    }
}
