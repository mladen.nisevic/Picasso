//
//  Theme2+Helpers.swift
//  Picasso
//
//  Created by Mladen Nisevic on 13/03/2023.
//  Copyright © 2023 BetVictor Ltd. All rights reserved.
//

import Foundation

extension Theme2 {
    
    func colour(for colourName: ColourNameEnum) -> ColourAlpha? {
        switch colourName {
        case .accessorySymbol: return accessorySymbol
        case .background: return background
        case .body: return body
        case .border: return border
        case .cardBackground: return cardBackground
        case .subtitle: return subtitle
        case .title: return title
        case .underline: return underline
        }
    }
    
    var colourSet: [ColourAlpha?] {
        [self.accessorySymbol,
         self.background,
         self.body,
         self.border,
         self.cardBackground,
         self.subtitle,
         self.title,
         self.underline
        ]
    }
    
    var hexValues: String {
        """
        accessorySymbol: \(accessorySymbol?.hexValue ?? .empty) \(accessorySymbol?.alphaValue ?? Double.defaultAlpha)
        background: \(background?.hexValue ?? .empty) \(background?.alphaValue ?? Double.defaultAlpha)
        body: \(body?.hexValue ?? .empty) \(body?.alphaValue ?? Double.defaultAlpha)
        border: \(border?.hexValue ?? .empty) \(border?.alphaValue ?? Double.defaultAlpha)
        cardBackground: \(cardBackground?.hexValue ?? .empty) \(cardBackground?.alphaValue ?? Double.defaultAlpha)
        subtitle: \(subtitle?.hexValue ?? .empty) \(subtitle?.alphaValue ?? Double.defaultAlpha)
        title: \(title?.hexValue ?? .empty) \(title?.alphaValue ?? Double.defaultAlpha)
        underline: \(underline?.hexValue ?? .empty) \(underline?.alphaValue ?? Double.defaultAlpha)
        """
    }
    
    var definitionStrings: String {
        """
        accessorySymbol: \(accessorySymbol?.colour?.rawLine ?? .empty)
        background: \(background?.colour?.rawLine ?? .empty)
        body: \(body?.colour?.rawLine ?? .empty)
        border: \(border?.colour?.rawLine ?? .empty)
        cardBackground: \(cardBackground?.colour?.rawLine ?? .empty)
        subtitle: \(subtitle?.colour?.rawLine ?? .empty)
        title: \(title?.colour?.rawLine ?? .empty)
        underline: \(underline?.colour?.rawLine ?? .empty)
        """
    }
    
    var themeGenesis: String {
        """
        accessorySymbol: \(accessorySymbol?.hexValue ?? .empty) \(accessorySymbol?.alphaValue ?? Double.defaultAlpha) < \(accessorySymbol?.colour?.rawLine ?? .empty)
        background: \(background?.hexValue ?? .empty) \(background?.alphaValue ?? Double.defaultAlpha) < \(background?.colour?.rawLine ?? .empty)
        body: \(body?.hexValue ?? .empty) \(body?.alphaValue ?? Double.defaultAlpha) < \(body?.colour?.rawLine ?? .empty)
        border: \(border?.hexValue ?? .empty) \(border?.alphaValue ?? Double.defaultAlpha) < \(border?.colour?.rawLine ?? .empty)
        cardBackground: \(cardBackground?.hexValue ?? .empty) \(cardBackground?.alphaValue ?? Double.defaultAlpha) < \(cardBackground?.colour?.rawLine ?? .empty)
        subtitle: \(subtitle?.hexValue ?? .empty) \(subtitle?.alphaValue ?? Double.defaultAlpha) < \(subtitle?.colour?.rawLine ?? .empty)
        title: \(title?.hexValue ?? .empty) \(title?.alphaValue ?? Double.defaultAlpha) < \(title?.colour?.rawLine ?? .empty)
        underline: \(underline?.hexValue ?? .empty) \(underline?.alphaValue ?? Double.defaultAlpha) < \(underline?.colour?.rawLine ?? .empty)
        """
    }
}

extension Theme2 {
    func keyExists(_ key: String) -> Bool {
        entity.propertiesByName.contains(where: { $0.key == key })
    }
}
