//
//  ThemesModel.swift
//  Picasso
//
//  Created by Mantas Jakimavicius on 01/09/2021.
//

import Foundation

final class ThemesModel {
    let betVictorThemes = BetVictorThemes().BVClases
    let bildBetThemes = BildBetThemes().BildBetClases
    let bWinThemes = BWinThemes().BWinClases
    let heartBingoThemes = HeartBingoThemes().HeartBingoClases
    let parimatchThemes = ParimatchThemes().ParimatchClases
    let williamHillThemes = WilliamHillThemes().WilliamHillClases
    let talkSportThemes = TalkSportThemes().TalkSportClases
}
