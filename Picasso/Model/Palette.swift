//
//  Palette.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 15/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//
// Note: This should stay foundation because the parsing script is compiled for UIKit
import Foundation

struct BrandPalette: Identifiable {
    var id: BrandName
    var palette: [PaletteElement]
    var desc: String {
        "\(id.rawValue) \(palette)"
    }

    var standardElements: [PaletteElement] {
        palette.filter { PaletteKeyStruct.standardKeys.contains($0.key) }
    }
    var additionalElements: [PaletteElement] {
        palette.filter { PaletteKeyStruct.additionalKeys.contains($0.key) }
    }
}

extension BrandPalette {
    init?(_ json: JSONPalette) {
        guard let brand = BrandName(rawValue: json.brand) else { return nil }
        self.id = brand
        var palette = [PaletteElement]()
        for jsonEl in json.palette {
            if let el = PaletteElement(jsonEl) {
                palette.append(el)
            }
        }
        self.palette = palette
    }
}

struct PaletteElement: Hashable {
    var key: PaletteKeyStruct
    var colourHex: String
}

extension PaletteElement {
    init?(_ colourLine: String, brand: BrandName, elements: [PaletteElement] = []) {
        guard let pal = PaletteKeyStruct(colourLine) else {
            print("Error: \(colourLine) not producing PaletteKeyStruct")
            return nil
        }

        if let hexValue = PaletteKeyStruct.hexValue(from: colourLine, brand: brand, elements: elements) {
            self = PaletteElement(key: pal, colourHex: hexValue)
        } else {
            // this may not be an issue bacause there will be a second pass
            return nil
        }
    }

    init?(_ json: JSONPaletteElement) {
        guard let pal = PaletteKeyStruct(rawValue: json.key) else { return nil }
        key = pal
        colourHex = json.hexColour
    }
}

extension Array where Element == PaletteElement {
    var desc: String {
        var str = "\(type(of: self)), elements: \(count)\n"
        for pe in self {
            str += "\(pe)\n"
        }
        return str
    }

    func value(forKey key: PaletteKeyStruct) -> PaletteElement? {
        filter { $0.key == key }.first
    }
}

enum PaletteKeyStruct: String, CaseIterable, Identifiable {

    var id: String { self.rawValue }

    // standard colour palette
    case text,
         background,
         card,
         brand,
         underline,
         primary,
         warning,
         error,
         payment,
         navigation,
         // additional colours for palettes
         selectedChip,
         unselectedChip,
         boost,
         input,
         teamHome,
         teamAway,
         selected,
         outcome,
         menuSelected,
         menuUnselected,
         filterSelected,
         filterUnselected,
    // HeartBingo
    errorView, new, popular, subtitleText, unselected,
    // white, black
    white, black, shelf

    static var standardKeys: [PaletteKeyStruct] = [
        text,
        background,
        card,
        brand,
        underline,
        primary,
        warning,
        error,
        payment,
        navigation
    ]

    static var additionalKeys: [PaletteKeyStruct] = [
        boost,
        input,
        teamHome,
        teamAway,
        selected,
        outcome,
        menuSelected,
        menuUnselected,
        filterSelected,
        filterUnselected,
        selectedChip,
        unselectedChip,
        shelf
    ]
}

extension PaletteKeyStruct {

    struct Const {
        static let hexPrefix = ".fromHex(0x"
        static let hexSuffix = ")"
        static let teamPrefix = "("
    }

    enum EndColour: String {
        case white = ".white"
        case black = ".black"
        // these have been referenced by other base colours of the palette
        case warning = ".warning"
        case text = ".text"
        case brand = ".brand"
        case background = ".background"
        case payment = ".payment"
    }

    static func hexValue(from colourLine: String, brand: BrandName, elements: [PaletteElement] = []) -> String? {

        guard let colourName = PaletteKeyStruct.endColourName(from: colourLine) else {
            print("Can't get the colour name: \(colourLine)")
            return nil
        }

        if colourName.hasPrefix(Const.hexPrefix) {
            return colourName.replacingOccurrences(of: Const.hexPrefix, with: String.empty).replacingOccurrences(of: Const.hexSuffix, with: String.empty)
        }

        var colourName2 = colourName
        if colourName.contains(String.underscore) {
            let components = colourName.components(separatedBy: String.underscore)
            if components.count > 1 {
                colourName2 = components[0]
            }
        }

        if colourName2.hasPrefix(Const.teamPrefix) {
            colourName2 = colourName2.replacingOccurrences(of: Const.teamPrefix, with: String.empty)
        }
        colourName2 = colourName2.trimmingCharacters(in: .whitespaces)

        guard let col = EndColour(rawValue: colourName2) else {
            print("Unknown end colour: \(colourName2)")
            return nil
        }

        switch col {
        case .white: return "FFFFFF"
        case .black: return "000000"
        case .warning: return elements.value(forKey: .warning)?.colourHex
        case .text: return elements.value(forKey: .text)?.colourHex
        case .brand: return elements.value(forKey: .brand)?.colourHex
        case .background: return elements.value(forKey: .background)?.colourHex
        case .payment: return elements.value(forKey: .payment)?.colourHex
        }
    }

    static func endColourName(from colourLine: String) -> String? {
        colourLine.replacingOccurrences(of: "^.*UIColor = ", with: String.empty, options: .regularExpression)
    }
}

extension PaletteKeyStruct {
    init?(_ colourLine: String) {
        var line = colourLine.replacingOccurrences(of: "static var ", with: String.empty)
        line = line.replacingOccurrences(of: ": .*", with: String.empty, options: .regularExpression)
        let comps = line.components(separatedBy: String.underscore)
        guard comps.count == 2 else { return nil }
        if let pal = PaletteKeyStruct(rawValue: comps[0]) {
            self = pal
        } else {
            return nil
        }
    }
}
