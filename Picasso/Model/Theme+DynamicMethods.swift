//
//  Theme+DynamicMethods.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 02/11/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import UIKit

extension Theme {

    func colourMethodNames() -> [String] {
        /// Not supporting excluded names yet as they don't return UIColor
        let excludeList = ["gradient", "init", "shape"]
        let meths = methodNames().filter { !excludeList.contains($0) }
        return meths
    }

    func colourComponentNames() -> [ThemeComponent] {
        let excludeList = ["gradient", "init", "shape"]
        let meths = methodNames().filter { !excludeList.contains($0) }
        return ThemeComponent.comps(meths)
    }

    func methodNames() -> [String] {
        let myClass = type(of: self)
        var methodCount: UInt32 = 0
        let methodList = class_copyMethodList(myClass, &methodCount)
        var methodNames = [String]()
        for i in 0..<Int(methodCount) {
            let selName = sel_getName(method_getName(methodList![i]))
            let methodName = String(cString: selName, encoding: .utf8)!
            methodNames.append(methodName)
        }
        return methodNames.sorted()
    }
}
