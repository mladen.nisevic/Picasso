//
//  ColourParser.swift
//  Picasso
//
//  Created by Mladen Nisevic on 21/12/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//
/**
 
 A class to try to interprete all the different definitions of colours and produce a RGBA hex value
 
 */
import UIKit

extension String {
    static let outsideBrackets = "\\((.*)\\)"
}

private extension String {
    static let `self` = "self"
    static let `super` = "super"
    static let alphaColonSpace = "alpha: "
    static let colon = ":"
    static let color = "Color"
    static let dotFromHexPrefix = ".fromHex(0x"
    static let fromHexPrefix = "fromHex(0x"
    static let fullStop = "."
    static let leftParenthesis = "("
    static let outsideFullStops = "\\.(.*)\\."
    static let parenthesis = "()"
    static let reference1 = "$1"
    static let rightBracketAlpha = "(alpha"
    static let rightParenthesis = ")"
    static let selfDot = "self."
    static let systemBlue = "systemBlue"
    static let theme = "Theme"
    static let uiColor = "UIColor"
    static let withAlphaComponent = "withAlphaComponent"
}

struct ColourInfo {
    var name: String?
    var hex: String?
    var alpha: Double?
    var inputString: String?
}

final class ColourParser: DebugLoggable {
    
    func processLine(rawLine: String, brand: Brand, colour: Colour? = nil, theme: Theme2? = nil) -> ColourInfo? {
        
        let rawLine = BaseParser.clean(rawLine)
        var parser: Parser?
        
        if BaseParser.isItUIColor(rawLine) {
            parser = UIColorParser(inputLine: rawLine, brand: brand)
        } else if rawLine.contains(String.fromHexPrefix) {
            parser = FromHexParser(inputLine: rawLine, brand: brand)
        } else if rawLine.contains(String.theme) {
            parser = ThemeParser(inputLine: rawLine, brand: brand)
        } else if rawLine.hasPrefix(String.appearance) {
            parser = AppearanceParser(inputLine: rawLine, brand: brand)
        } else if let colourName = DataManager.shared.colourName(with: rawLine) {
            parser = ColourNameParser(inputLine: rawLine, brand: brand, miscData: colourName)
        } else {
            parser = BrandColourParser(inputLine: rawLine, brand: brand, miscData: colour ?? theme)
        }
        
        return parser?.parseColour()
    }

}

protocol Parser {
    var inputLine: String { get }
    var brand: Brand { get }
    var miscData: Any? { get }
    func parseColour() -> ColourInfo?
    init(inputLine: String, brand: Brand, miscData: Any?)
}

// used in theme colour definitions
fileprivate let uiColors: [String] = [.clear, .white, .black, .darkGray, .red, .systemBlue]

class BaseParser: Parser, DebugLoggable {
    
    var inputLine: String
    var brand: Brand
    var miscData: Any?
    
    required init(inputLine: String, brand: Brand, miscData: Any? = nil) {
        self.inputLine = inputLine
        self.brand = brand
        self.miscData = miscData
    }
    
    func parseColour() -> ColourInfo? {
        nil
    }
}
 
// MARK: - Utils
extension BaseParser {
    static func isItUIColor(_ inputLine: String) -> Bool {
        let tokens = inputLine.split(String.fullStop)
        let colsSet = Set(uiColors)
        let tokensSet = Set(tokens)
        return !colsSet.intersection(tokensSet).isEmpty
    }
    
    static func clean(_ inputLine: String) -> String {
        var outputLine = inputLine.trimmingCharacters(in: .whitespacesAndNewlines)
        if outputLine.hasPrefix(.leftParenthesis) {
            outputLine = outputLine.apply(regex: .outsideBrackets)
        }
        outputLine = outputLine.trimmingCharacters(in: .whitespacesAndNewlines)
        if outputLine.hasPrefix(.fullStop) {
            outputLine = String(outputLine.dropFirst())
        }
        return outputLine
    }
}

/// things like white, black, clear, darkGray etc.
///  accepts UIColor.black, .black or black
///  and withAlphaComponent(0.5)
final class UIColorParser: BaseParser {

    override func parseColour() -> ColourInfo? {
        var line = inputLine.remove(.uiColor)
        line = line.trimmingCharacters(in: .punctuationCharacters)
        let comps = line.split(String.withAlphaComponent)
        var colourName: String? = line
        var alpha: Double?
        if comps.count > 1 {
            colourName = comps[0].remove(.fullStop)
            let alphaString = comps[1].remove(.leftParenthesis).remove(.rightParenthesis)
            alpha = Double(alphaString)
        }
        
        guard let colourName else { return nil }
        let sel = NSSelectorFromString(colourName + .color)
        if UIColor.responds(to: sel) {
            if let colour = UIColor.perform(sel).takeRetainedValue() as? UIColor,
                let hex = colour.hexString {
                if colour == UIColor.clear {
                    alpha = 0
                }
                return ColourInfo(name: colourName, hex: String(hex.prefix(6)), alpha: alpha, inputString: inputLine)
            }
        }
        return nil
    }
}

final class FromHexParser: BaseParser {

    override func parseColour() -> ColourInfo? {
        var line = inputLine.remove(.uiColor)
        line = line.trimmingCharacters(in: .punctuationCharacters)
        line = line.remove(.fromHexPrefix).remove(.rightParenthesis)
        var hexValue: String? = line
        let comps = line.split(String.comma)
        var alpha: Double?
        if comps.count > 1 {
            hexValue = comps[0]
            let alphaRaw = comps[1].remove(.alphaColonSpace).trimmingCharacters(in: .whitespacesAndNewlines)
            alpha = Double(alphaRaw)
        }
        
        let cf = ColourInfo(hex: hexValue, alpha: alpha)
        return cf
    }
}

final class ThemeParser: BaseParser {
    
    override func parseColour() -> ColourInfo? {
        var cf: ColourInfo?
        let dataManager = DataManager.shared
        let comps = inputLine.split(String.fullStop)
        if comps.count == 2 {
            let colourName = comps[1].remove(.parenthesis)
            let themeName = comps[0].remove(.parenthesis)
            if let themeName = dataManager.themeName(with: themeName),
               let theme = dataManager.theme(themeName, brand: brand) {
                let colourAlpha = theme.value(forKey: colourName)
                if let hex = (colourAlpha as? ColourAlpha)?.colour?.hexValue {
                    cf = ColourInfo(hex: hex)
                }
            }
        }
        return cf
    }
}

final class AppearanceParser: BaseParser {
    
    override func parseColour() -> ColourInfo? {
        var cf: ColourInfo?
        let dataManager = DataManager.shared
        
        var alpha: Double?
        var appearancePart: String? = inputLine
        let components1 = inputLine.split(String.rightBracketAlpha)
        if components1.count > 1 {
            appearancePart = components1[0]
            let alphaRaw = components1[1].remove(.colon).trimmingCharacters(in: .whitespacesAndNewlines)
            alpha = Double(alphaRaw)
        }
        
        let comps = appearancePart?.split(String.fullStop) ?? []
        guard comps.count == 3 else { return nil }
        let name = comps[2].remove(.parenthesis)
        if let alias = dataManager.appearanceAlias(name: name, brand: brand),
           let hex = alias.color?.hexValue {
            cf = ColourInfo(hex: hex, alpha: alpha)
        } else if let appearanceColor = dataManager.appearanceColor(name: name, brand: brand),
                  let hex = appearanceColor.hexValue {
            cf = ColourInfo(hex: hex, alpha: alpha)
        }
        return cf
    }
}

final class ColourNameParser: BaseParser {
    
    override func parseColour() -> ColourInfo? {
        guard let colourName = miscData as? ColourName else { return nil }
        let definedColour = DataManager.shared.fetchColour(colourName: colourName, brand: brand)
        if let hex = definedColour?.hexValue {
            return ColourInfo(name: colourName.name, hex: hex)
        }
        return nil
    }
}

final class BrandColourParser: BaseParser {
    
    override func parseColour() -> ColourInfo? {
        
        if inputLine.contains(String.parenthesis) || inputLine.hasPrefix(String.super) {
            return parseThemeMethod()
        } else {
            return parseColourExtension()
        }
    }
    
    private func parseColourExtension() -> ColourInfo? {
        let line = inputLine.remove(.super).remove(.`self`).remove(.uiColor)
        let comps = line.split(String.withAlphaComponent)
        var colourName: String? = line
        var alpha: Double?
        if comps.count > 1 {
            colourName = comps[0]
            let alphaString = comps[1].remove(.leftParenthesis).remove(.rightParenthesis)
            alpha = Double(alphaString)
        }
        colourName = colourName?.remove(.fullStop)
        
        if let colourName, let colour = DataManager.shared.fetchColour(for: colourName, brand: brand) {
            return ColourInfo(hex: colour.hexValue, alpha: alpha)
        }
        return nil
    }
    
    private func parseThemeMethod() -> ColourInfo? {
        guard let miscData else {
            let msg = "\(Self.self):\(#function):\(#line): no misc data, colour or theme, \(inputLine)"
            log(message: msg, level: .error)
            return nil
        }
        let theme: Theme2? = (miscData as? Theme2) ?? theme(from: miscData as? Colour)
        return getFromTheme(theme: theme, brand: brand)
    }
    
    private func getFromTheme(theme: Theme2?, brand: Brand) -> ColourInfo? {
        guard let theme else { return nil }
        let line = inputLine.remove(.super).remove(.`self`).remove(.uiColor)
        
        let comps = line.split(String.withAlphaComponent)
        var colourName: String? = line.remove(.fullStop).remove(.parenthesis)
        var alpha: Double?
        if comps.count > 1 {
            colourName = comps[0].remove(.fullStop).remove(.parenthesis)
            let alphaString = comps[1].remove(.leftParenthesis).remove(.rightParenthesis)
            alpha = Double(alphaString)
        }

        if let colourName,
           theme.keyExists(colourName),
           let themeColourAlpha = theme.value(forKey: colourName) as? ColourAlpha {
            if let hex = themeColourAlpha.hexValue {
                return ColourInfo(name: colourName, hex: hex, alpha: alpha)
            }
        }

        if let parentTheme = theme.parent {
            return getFromTheme(theme: parentTheme, brand: brand)
        }
        
        return nil
    }
    
    private func getThemeName(from colourName: String) -> String {
        let comps = colourName.split(String.underscore)
        guard comps.count == 2 else { return .empty }
        let themeName = comps[0]
        return themeName
    }
    
    private func theme(from colour: Colour?) -> Theme2? {
        guard let colour else { return nil }
        let themeName = getThemeName(from: colour.name ?? .empty)
        return DataManager.shared.theme(name: themeName, brand: brand)
    }
}

extension String {
    
    func split(_ string: String) -> [String] {
        components(separatedBy: string)
    }
    
    func remove(_ string: String) -> String {
        replacingOccurrences(of: string, with: String.empty)
    }

    func apply(regex: String) -> String {
        replacingOccurrences(of: regex, with: String.reference1, options: .regularExpression).trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
