//
//  Palette+UIKit.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 18/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import UIKit

extension BrandPalette {
    func colour(_ key: PaletteKeyStruct) -> UIColor {
        let cols = palette.filter { $0.key == key }
        return cols.first?.colour ?? UIColor.clear
    }
    func colourHex(_ key: PaletteKeyStruct) -> String {
        let cols = palette.filter { $0.key == key }
        return cols.first?.colourHex ?? "Undefined"
    }
}

extension UIColor {
    var rgba: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)

        return (red, green, blue, alpha)
    }
}

extension BrandPalette {
    static var test: BrandPalette {
        BrandPalette(id: BrandName.BV, palette: PaletteElement.test)
    }
    static var test2: BrandPalette {
        BrandPalette(id: BrandName.Parimatch, palette: PaletteElement.test2)
    }
}

extension PaletteElement {
    var colour: UIColor {
        UIColor(hex: colourHex) ?? UIColor()
    }
}

extension PaletteElement {
    static var test: [PaletteElement] {
        let testCols: [UIColor] = [.systemYellow, .systemPurple, .systemGray6, .systemGreen, .systemOrange]
        return testBase(testCols)
    }
    static var test2: [PaletteElement] {
        let testCols: [UIColor] = [.systemRed, .systemBlue, .systemGray, .systemPink, .systemTeal]
        return testBase(testCols)
    }
    static func testBase(_ colours: [UIColor]) -> [PaletteElement] {
        var arr = [PaletteElement]()
        for pal in PaletteKeyStruct.allCases {
            let idx = Int.random(in: 0..<colours.count)
            arr.append(PaletteElement(key: pal, colourHex: colours[idx].toHexString()))
        }
        return arr
    }
}
