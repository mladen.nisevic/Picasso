//
//  AppearanceKey.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 13/07/2022.
//  Copyright © 2022 BetVictor Ltd. All rights reserved.
//

import Foundation

enum PlistKey: String, CaseIterable {
    case UICOLOR, UICOLORALIAS, UIFONT
}

enum AppearanceColourKey: String, CaseIterable {
    case  Background,
          Black,
          Button,
          Error,
          Secondary,
          Selected,
          SelectedChip,
          SportTeamAway,
          UnselectedChip,
          Warning,
          White
}
