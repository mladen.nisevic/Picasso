//
//  TargetFile
//  Picasso
//
//  Created by Mladen.Nisevic on 15/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import Foundation

/** Themes Target Files */
enum TargetFile: String, CaseIterable {
    case
    BadgeThemes,
    ButtonThemes,
    CTCThemes,
    CasinoThemes,
    CollectionTileThemes,
    HistoryThemes,
    InplayThemes,
    ModalThemes,
    MyAccountThemes,
    MyBetsThemes,
    SearchThemes,
    SearchViewThemes,
    SportThemes,
    TextFieldThemes,
    Themes
}

