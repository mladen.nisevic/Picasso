//
//  Helpers.swift
//  Test
//
//  Created by Alexei Jovmir on 26/02/2021.
//
import Foundation
import UIKit
import SwiftUI

extension String {
    static let hexPrefix = "#"
    static let defaultAlphaHex = "FF"
}

extension CGFloat {
    static let defaultAlpha: CGFloat = 1.0
}

extension Double {
    static let defaultAlpha: Double = 1.0
}

@objc public protocol Theme {
    func background() -> UIColor
    func cardBackground() -> UIColor
    func title() -> UIColor
    func subtitle() -> UIColor
    func border() -> UIColor
    func underline() -> UIColor
    func accessorySymbol() -> UIColor
    func shape() -> ViewShape
    init()
}

@objc public class Gradient: NSObject {
    @objc public var colors: [UIColor]
    @objc public var locations: [NSNumber]?
    @objc public var direction: GradientDirection

    @objc public init(colors: [UIColor], locations: [NSNumber]? = nil, direction: GradientDirection = .vertical) {
        self.colors = colors
        self.locations = locations
        self.direction = direction
        super.init()
    }
}

@objc public enum GradientDirection: Int {
    case vertical, horizontal
}

@objc public protocol BackgroundTheme: Theme {
    func gradient() -> Gradient
    func shadowColor() -> UIColor
}

@objc public enum ViewShape: Int {
    case square
    case rounded
    case carded
    case oval
    case invisible
}

@objc public enum ViewStatus: Int {
    case active
    case inactive
    case selected
    case selectedInactive
    case error
    case warning
    case disabled
}

@objc public protocol Chamaleon: Theme {
    func mutate(status: ViewStatus) -> Theme.Type
}

public extension UIColor {
    class func fromHex(_ hexValue: Int, alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: CGFloat(((Float)((hexValue & 0xFF0000) >> 16))/255.0),
                       green: CGFloat(((Float)((hexValue & 0xFF00) >> 8))/255.0),
                       blue: CGFloat(((Float)(hexValue & 0xFF))/255.0), alpha: alpha)

    }
}

extension UIColor {
    
    /// there are two ways to enter the value for alpha
    /// parameter or last two digits of an 8-digit hex value
    /// parameter has priority
    /// - Parameters:
    ///   - hex: 6 or 8 digit hexadecimal value, optionally prefixed with '#'
    ///   - alpha: optional value for alpha (defaults to 1/FF/255)
    convenience init?(hex: String, alpha: CGFloat = .defaultAlpha) {
        var hexFormatted: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if hexFormatted.hasPrefix(String.hexPrefix) {
            hexFormatted = String(hexFormatted.dropFirst())
        }

        guard hexFormatted.count == 6 || hexFormatted.count == 8 else { return nil }
        guard hexFormatted.isHexNumber else { return nil }
        
        if hexFormatted.count == 6 {
            hexFormatted.append(.defaultAlphaHex)
        }

        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)

        let alphaFinal = alpha == .defaultAlpha ? CGFloat(rgbValue & 0x000000FF) / 255.0 : alpha
        self.init(red: CGFloat((rgbValue & 0xFF000000) >> 24) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF0000) >> 16) / 255.0,
                  blue: CGFloat((rgbValue & 0x0000FF00) >> 8) / 255.0,
                  alpha: alphaFinal)
    }

    func toHexString() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var alpha: CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &alpha)
        let rgba: Int = (Int)(r*255)<<24 | (Int)(g*255)<<16 | (Int)(b*255)<<8 | (Int)(alpha*255)<<0
        if alpha == 0 {
            return .empty
        }
        return String(format: "#%06x", rgba)
    }
    
    static func isValid(hexCode: String) -> Bool {
        var hexFormatted: String = hexCode.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if hexFormatted.hasPrefix(String.hexPrefix) {
            hexFormatted = String(hexFormatted.dropFirst())
        }

        guard hexFormatted.count == 6 || hexFormatted.count == 8 else { return false }
        guard hexFormatted.isHexNumber else { return false }

        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)

        _ = UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: 1)
        return true
    }
    
    static func descriptiveName(for hexValue: String?) -> String? {
        guard let hexValue = hexValue else { return nil }
        if UIColor.isValid(hexCode: hexValue) {
            return UIColor(hex: hexValue)?.accessibilityName
        }
        return nil
    }
    
    var inverted: UIColor {
        var a: CGFloat = 0.0, r: CGFloat = 0.0, g: CGFloat = 0.0, b: CGFloat = 0.0
        return getRed(&r, green: &g, blue: &b, alpha: &a) ? UIColor(red: 1.0-r, green: 1.0-g, blue: 1.0-b, alpha: a) : .black
    }
    
    var contrasty: UIColor {
        isLight ? .black : .white
    }
    
    var isLight: Bool {
        guard let components = cgColor.components, components.count > 2 else { return false }
        let brightness = ((components[0] * 299) + (components[1] * 587) + (components[2] * 114)) / 1000
        return brightness > 0.5
    }
}

extension UIColor {
    var hexString: String? {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0

        let multiplier = CGFloat(255.999999)

        guard self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) else {
            return nil
        }

        if alpha == 1.0 {
            return String(
                format: "%02lX%02lX%02lX",
                Int(red * multiplier),
                Int(green * multiplier),
                Int(blue * multiplier)
            )
        }
        else {
            return String(
                format: "%02lX%02lX%02lX%02lX",
                Int(red * multiplier),
                Int(green * multiplier),
                Int(blue * multiplier),
                Int(alpha * multiplier)
            )
        }
    }
}

extension Color {
  static let defaultBlue = Color(red: 0, green: 97 / 255.0, blue: 205 / 255.0)
  static let paleBlue = Color(red: 188 / 255.0, green: 224 / 255.0, blue: 253 / 255.0)
  static let paleWhite = Color(white: 1, opacity: 179 / 255.0)
}
