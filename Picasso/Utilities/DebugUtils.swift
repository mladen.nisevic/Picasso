//
//  MiscUtils.swift
//  ExploratoryProject1
//
//  Created by Mladen Nisevic on 30/07/2021.
//

import Foundation

private extension String {
    static let timestampFormat = "yyyy-MM-dd HH:mm:ss.SSS"
}

func date2() -> String {
    let fd = DateFormatter()
    fd.dateFormat = .timestampFormat
    return fd.string(from: Date())
}

/// Helpful for logging into the swift script log
/// the name is not correct but
func pr(_ message: String = .empty, function: String = #function, file: String = #file, line: Int = #line, thread: String = "\(Thread.current.desc)") {
    print("\(date2()) \(thread) \(file.components(separatedBy: "/").last!) \(function):\(line): \(message)")
}

func prm(_ message: String = .empty, function: String = #function, file: String = #file, line: Int = #line, thread: String = "\(Thread.current.desc)") -> String {
    "\(date2()) \(thread) \(file.components(separatedBy: "/").last!) \(function):\(line): \(message)"
}

extension Thread {

    var desc: String {
        var extendedDetails = String.empty
        if Thread.isMainThread {
            extendedDetails += "main"
        } else {
            if let threadName = Thread.current.name, !threadName.isEmpty {
                extendedDetails += "\(threadName)"
            } else if let queueName = OperationQueue.current?.underlyingQueue?.label, !queueName.isEmpty {
                extendedDetails += "\(queueName)"
            } else {
                let firs = String(format: "%p", Thread.current)
                let scnd = firs.replacingOccurrences(of: "0x60000", with: String.empty)
                extendedDetails += scnd
            }
        }
        return extendedDetails
    }
}
