//
//  ThemeNamesHelper.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 14/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import Foundation

class ThemeNameHelper: ObservableObject {

    static let shared = ThemeNameHelper()

    @Published var themeNames: [ThemeName] = []

    private init() {
        _ = themeList()
    }

    func themeList() -> [ThemeName] {

        guard themeNames.isEmpty else { return themeNames }

        for themeClass in BetVictorThemes().BVClases {
            print("theme: \(themeClass.themeName), \(themeClass.brand), \(themeClass), \(themeClass.fullName)")
            themeNames.append(ThemeName(id: themeClass.themeName))
        }

        return themeNames
    }
}
