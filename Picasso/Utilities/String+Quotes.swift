//
//  String+SmartQuote.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 27/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

#if os(iOS)
import UIKit
#else
import Cocoa
#endif

public extension String {
    static let closeCurly = "}"
    static let comment = "//"
    static let comma = ","
    static let ddMMMyyyy = "dd MMM yyyy"
    static let doubleQuote = "\""
    static let ed = "ed"
    static var empty = ""
    static var dotJson = ".json"
    static let newLine = "\n"
    static var space = " "
    static var squareOpen = "["
    static var squareClose = "]"
    static let dotSwift = ".swift"
    static let tab = "\t"
    static let underscore = "_"
    static let yyyy = "yyyy"

    static func newLines(_ count: Int = 2) -> String {
        String(repeating: .newLine, count: count)
    }
}

public extension String {
    static var importUIKit = "import UIKit"
    static var importFoundation = "import Foundation"
    static var importUIFactory = "import UIFactory"
}

public extension String {

    static var quotes: (String, String) {
        guard
            let bQuote = Locale.current.quotationBeginDelimiter,
            let eQuote = Locale.current.quotationEndDelimiter
            else { return (.doubleQuote, .doubleQuote) }

        return (bQuote, eQuote)
    }

    var quoted: String {
        let (bQuote, eQuote) = String.quotes
        return bQuote + self + eQuote
    }

    var isQuoted: Bool {
        isQuotedStandard || isQuotedLocal || isQuotedLocalAlternate
    }

    var isQuotedStandard: Bool {
        hasPrefix(.doubleQuote) && hasSuffix(.doubleQuote)
    }

    var isQuotedLocal: Bool {
        hasPrefix(Locale.current.quotationBeginDelimiter ?? .doubleQuote) && hasSuffix(Locale.current.quotationEndDelimiter ?? .doubleQuote)
    }

    var isQuotedLocalAlternate: Bool {
        return hasPrefix(Locale.current.alternateQuotationBeginDelimiter ?? .doubleQuote) && hasSuffix(Locale.current.alternateQuotationEndDelimiter ?? .doubleQuote)
    }

    var isStartsQuoted: Bool {
        return hasPrefix(.doubleQuote) ||
            hasPrefix(Locale.current.quotationBeginDelimiter ?? .doubleQuote) ||
            hasPrefix(Locale.current.alternateQuotationBeginDelimiter ?? .doubleQuote)
    }
}

extension String {
    var isHexNumber: Bool {
        filter(\.isHexDigit).count == count
    }
}
