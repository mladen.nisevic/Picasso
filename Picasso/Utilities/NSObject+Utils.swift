//
//  NSObject+Utils.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 24/12/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import Foundation

func isType(of nsObject: NSObject, in anyObjectTypes: [AnyObject.Type]) -> Bool {
    for anyObjectType in anyObjectTypes {
        if type(of: nsObject) == anyObjectType {
            return true
        }
    }
    return false
}

/// Return an array of types of all ancestor themes
/// - Parameter anyClass: a class of a theme whose ancestor types we want to find
/// - Returns: an array of ancestor types
func superClasses(for anyClass: Any) -> [AnyClass] {
    var classes = [AnyClass]()
    /// If the parent type is NSObject - return, otherwise get the superclass of the class
    /// - Parameter anyClass: Any Theme class
    /// - Returns: parent class
    func getSuper(_ anyClass: AnyClass) -> AnyClass? {
        guard let next = anyClass.superclass() as? NSObject.Type else {
            return NSObject.self
        }
        classes.append(anyClass)
        return getSuper(next.self)
    }
    if let objClass = anyClass.self as? NSObject.Type {
        _ = getSuper(objClass)
    }
    return classes
}
