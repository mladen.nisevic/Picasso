//
//  SettingsHelper.swift
//  Picasso
//
//  Created by Mladen.Nisevic on 27/10/2021.
//  Copyright © 2021 BetVictor Ltd. All rights reserved.
//

import Foundation

class SettingsHelper: ObservableObject {

    static let shared = SettingsHelper()

    @Published var lastSearch: String {
        didSet {
            UserDefaults.standard.set(lastSearch, forKey: Key.lastSearch)
        }
    }

    @Published var isExactMatch: Bool {
        didSet {
            UserDefaults.standard.set(isExactMatch, forKey: Key.isExact)
        }
    }

    @Published var selectedBrand: String? {
        didSet {
            UserDefaults.standard.set(selectedBrand, forKey: Key.selectedBrand)
        }
    }

    @Published var noMatches: Int = 0

    init() {
        lastSearch = UserDefaults.standard.string(forKey: Key.lastSearch) ?? .empty
        isExactMatch = UserDefaults.standard.bool(forKey: Key.isExact)
        if let brand1 = UserDefaults.standard.string(forKey: Key.selectedBrand),
            let brand = BrandName(rawValue: brand1) {
            selectedBrand = brand.rawValue
        }
    }
}
