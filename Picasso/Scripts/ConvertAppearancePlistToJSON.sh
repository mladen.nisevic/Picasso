#!/bin/sh


for i in 'WilliamHill' 'BV' 'Parimatch' 'BWin' 'HeartBingo' 'BildBet' 'TalkSport'; do
    echo "i: $i"
    echo "./Resources/$i/Common/Style/BVAppearance.plist"
    echo "Picasso/ParsedColors/${i}Appearance.json"
    plutil -convert json ./Resources/$i/Common/Style/BVAppearance.plist -o Picasso/ParsedColors/${i}Appearance.json
done

